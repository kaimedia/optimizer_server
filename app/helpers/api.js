'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const hash = require('object-hash');
const cache = require('../lib/redis');
const Tracker = require('../models/tracker');

function keyCache(name, body, params = null, queries = null) {
    body._name = name;

    if (params)
        body = _.assign(body, params);

    if (queries)
        body = _.assign(body, queries);

    return hash(_.omit(body, '_cache'));
}

function setCache(client, name, body, params = null, queries = null) {
    let key = keyCache(name, body, params, queries);

    if (!_.isUndefined(body._cache) && body._cache == 0) {
        cache._del(client, key);
    }

    return key;
}

function removeKey(body) {
    return _.omit(body, ['_id', 'accessAPI']);
}

function calculatorSumFields(obj, fields) {
    const data = _.transform(fields, function (result, n) {
        result[n] = sumByObject(obj, n);
    }, {});

    return data;
}

function sumByObject(obj, key) {
    return round(_.sumBy(obj, key), 2);
}

function floor(n, length) {
    return _.floor(_z(n), length);
}

function round(n, length) {
    return _.round(_z(n), length);
}

function ceil(n, length) {
    return _.ceil(_z(n), length);
}

function divide(a, b) {
    if (b === 0 || isNaN(b))
        return 0;
    else
        return _.divide(a, b);
}

function defaultKey(obj, fields) {
    const data = _.transform(fields, function (result, n) {
        result[n] = _z(obj[n]);
    }, {});
    return data;
}

function _z(number) {
    return number = number || 0;
}

function takeObjectHasField(data, fields) {
    _.forEach(fields, field => {
        data = _.filter(data, o => !_.isUndefined(o[field]));
    });
    return data;
}

function changeKeys(obj, newKeys) {
    return _.mapKeys(obj, (value, key) => newKeys[key]);
}

function addCampaignColumns(campaign) {
    const columns = ['revenue', 'cost', 'trk_clicks', 'lp_clicks', 'conversions', 'epc'];
    campaign = _.defaults(campaign, defaultKey(campaign, columns));

    const net = _.subtract(campaign['revenue'], campaign['cost']);
    const roi = divide(net, campaign['cost']) * 100;
    const epv = divide(campaign['revenue'], campaign['trk_clicks']);
    const cpv = divide(campaign['cost'], campaign['trk_clicks']);
    const cpa = divide(campaign['cost'], campaign['conversions']);

    return _.assign(campaign, { net, roi, epv, cpv, cpa });
}

function addSiteColumns(site) {
    const columns = ['revenue', 'cost', 'trk_clicks', 'total_bot', 'block_bot', 'conversions', 'cpc_boost', 'configured_cpc'];
    site = _.defaults(site, defaultKey(site, columns));

    const net = _.subtract(site['revenue'], site['cost']);
    const roi = divide(net, site['cost']) * 100;
    const bot = divide(site['block_bot'], site['total_bot']) * 100;
    const epv = divide(site['revenue'], site['trk_clicks']);
    const cpv = divide(site['cost'], site['trk_clicks']);
    const cpa = divide(site['cost'], site['conversions']);

    return _.assign(site, { net, roi, bot, epv, cpv, cpa });
}

function addContentColumns(content) {
    const columns = ['revenue', 'cost', 'trk_clicks', 'lp_clicks', 'conversions', 'epc'];
    content = _.defaults(content, defaultKey(content, columns));

    const net = _.subtract(content['revenue'], content['cost']);
    const roi = divide(net, content['cost']) * 100;
    const epv = divide(content['revenue'], content['trk_clicks']);
    const cpv = divide(content['cost'], content['trk_clicks']);
    const cpa = divide(content['cost'], content['conversions']);

    return _.assign(content, { net, roi, epv, cpv, cpa });
}

function getTrackerKeys(req) {
    return new Promise(function (resolve, reject) {
        let keys = req.accessAPI ? req.accessAPI.tracker : {};
        if (!req.query.tracker_id) {
            resolve(keys);
        } else {
            Tracker.findById(req.query.tracker_id, (err, tracker) => {
                if (err) return reject(err);
                keys = {
                    access_id: tracker.access_id,
                    access_key: tracker.access_key
                };
                resolve(keys);
            });
        }
    });
}

function calcCpm(cost, impressions) {
    return divide(cost, divide(impressions, 1000));
}

exports.keyCache = keyCache;
exports.setCache = setCache;
exports.removeKey = removeKey;
exports.sumByObject = sumByObject;
exports.calculatorSumFields = calculatorSumFields;
exports.defaultKey = defaultKey;
exports.changeKeys = changeKeys;
exports.addSiteColumns = addSiteColumns;
exports.addCampaignColumns = addCampaignColumns;
exports.addContentColumns = addContentColumns;
exports.takeObjectHasField = takeObjectHasField;
exports.getTrackerKeys = getTrackerKeys;
exports.calcCpm = calcCpm;
exports.floor = floor;
exports.round = round;
exports.ceil = ceil;
exports.divide = divide;
exports._z = _z;