'use strict';

const _ = require('lodash');
const config = require('config');
const logger = require('../lib/logger');
const Promise = require('bluebird');
const CronJob = require('cron').CronJob;
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const dateFormat = config.get('dateFormat');
const Traffic = require('../models/traffic');
const Tracker = require('../models/tracker');
const Campaign = require('../models/campaign');
const campaignService = require('../shared/services/campaign');
const siteService = require('../shared/services/site');
const contentService = require('../shared/services/content');
const taboolaAPI = require('../shared/services/api-connector/traffic-sources/taboola');
const outbrainAPI = require('../shared/services/api-connector/traffic-sources/outbrain');
const taboolaCampaignService = require('../shared/services/taboola/campaign');
const taboolaContentService = require('../shared/services/taboola/content');
const outbrainCampaignService = require('../shared/services/outbrain/campaign');
const outbrainContentService = require('../shared/services/outbrain/content');

/**
 * Update `id` field of all campaigns from Traffic Source
 */
const updateCampaignFromTrafficSource = new CronJob({
    cronTime: '*/13 * * * *',
    onTick: function () {
        Campaign.find({ archived: 0 }, (err, results) => {
            if (err) return;

            const campaigns = _.filter(results, o => !o.id && o.id_fraud && o.trafficSourceAccount);
            if (!campaigns || _.size(campaigns) === 0) return;

            const promises = [];
            _.forEach(campaigns, campaign => {
                promises.push(addTrafficSourceCampaign(campaign));
            });
            Promise.all(promises).then(response => {
                logger.info("DONE: Updated Traffic Source Campaigns!");
            }).catch(e => logger.error(e));
        });
    },
    start: false,
    timeZone: config.get('timezone'),
});
updateCampaignFromTrafficSource.start();
logger.info(`[CRON_JOB]: Update Traffic Source Campaigns = ${updateCampaignFromTrafficSource.running}`);

function addTrafficSourceCampaign(campaign) {
    return new Promise(function (resolve, reject) {
        Traffic.findById(campaign.trafficSourceAccount, (err, trafficSource) => {
            if (err) return err;
            let accessKeys;
            switch (trafficSource.type) {
                case 'Taboola':
                    accessKeys = _.pick(trafficSource, ['type', 'account_id', 'client_id', 'client_secret']);
                    taboolaCampaigns(accessKeys, campaign).then(res => {
                        Campaign.findByIdAndUpdate(campaign._id, {
                            id: res.campaign.id,
                            cpc: res.campaign.cpc,
                            spending_limit: res.campaign.spending_limit,
                            daily_cap: res.campaign.daily_cap,
                            daily_ad_delivery_model: res.campaign.daily_ad_delivery_model,
                            start_date: res.campaign.start_date,
                            publisher_targeting: res.campaign.publisher_targeting,
                            publisher_bid_modifier: res.campaign.publisher_bid_modifier,
                            is_active: res.campaign.is_active,
                            status: res.campaign.status,
                            updated_at: Date.now(),
                        }, (err, response) => {
                            logger.info('Updated Taboola campaign [%s] success.', campaign._id);

                            if (!campaign.blockSites || _.isEmpty(campaign.blockSites)) {
                                resolve({ success: true });
                            } else {
                                const sites = _.map(campaign.blockSites, site_id => {
                                    return { site_id: site_id };
                                });
                                taboolaCampaignService.blockSitesCampaign(accessKeys, res.campaign.id, sites).then(result => {
                                    resolve({ success: true });
                                }, err => logger.error(err));
                            }
                        });
                    });
                    break;

                case 'Outbrain':
                    accessKeys = _.pick(trafficSource, ['type', 'account_id', 'email', 'password']);
                    outbrainCampaigns(accessKeys, campaign).then(res => {
                        Campaign.findByIdAndUpdate(campaign._id, {
                            id: res.campaign.id,
                            cpc: res.campaign.cpc,
                            minimumCpc: res.campaign.minimumCpc,
                            spending_limit: res.campaign.budget.amount,
                            daily_ad_delivery_model: res.campaign.budget.type,
                            budget: res.campaign.budget,
                            amountSpent: res.campaign.liveStatus.amountSpent,
                            status: outbrainCampaignService.changeStatusText(res.campaign.liveStatus.onAirReason),
                            updated_at: Date.now(),
                        }, (err, response) => {
                            logger.info('Updated Outbrain campaign [%s] success.', campaign._id);

                            if (!campaign.blockSites || _.isEmpty(campaign.blockSites)) {
                                resolve({ success: true });
                            } else {
                                const sites = _.map(campaign.blockSites, site_id => {
                                    return { site_id: site_id };
                                });
                                outbrainCampaignService.blockSitesCampaign(accessKeys, res.campaign.id, sites).then(result => {
                                    resolve({ success: true });
                                }, err => logger.error(err));
                            }
                        });
                    });
                    break;
            }
        });
    });
}

function taboolaCampaigns(accessKeys, campDb) {
    return new Promise(function (resolve, reject) {
        taboolaCampaignService.allTaboolaCampaigns(accessKeys).then(data => {
            if (_.isEmpty(data.results)) return;
            taboolaAPI.auth(accessKeys).then(auth => {
                let promises = [];
                _.forEach(data.results, campTaboola => {
                    promises.push(taboolaContent(accessKeys, auth.access_token, campTaboola));
                });
                Promise.all(promises).then(response => {
                    response = _.find(response, o => o.contentUrl.indexOf(campDb.id_fraud) != -1);
                    resolve(response);
                }).catch(err => logger.error(err));
            });
        }, err => logger.error(err));
    });
}

function taboolaContent(accessKeys, token, campTaboola) {
    return new Promise(function (resolve, reject) {
        taboolaContentService.taboolaContentList(token, accessKeys.account_id, campTaboola.id).then(data => {
            if (_.isEmpty(data.results)) return;
            resolve({ campaign: campTaboola, contentUrl: data.results[0]['url'] })
        }, err => logger.error(err));
    });
}

function outbrainCampaigns(accessKeys, campDb) {
    return new Promise(function (resolve, reject) {
        outbrainCampaignService.outbrainCampaignList(accessKeys).then(data => {
            if (_.isEmpty(data.results)) return;
            outbrainAPI.auth(accessKeys).then(token => {
                let promises = [];
                _.forEach(data.results, campOutbrain => {
                    promises.push(outbrainContent(accessKeys, token, campOutbrain));
                });
                Promise.all(promises).then(response => {
                    response = _.find(response, o => o.contentUrl.indexOf(campDb.id_fraud) != -1);
                    resolve(response);
                }).catch(err => logger.error(err));
            });
        }, err => logger.error(err));
    });
}

function outbrainContent(accessKeys, token, campOutbrain) {
    return new Promise(function (resolve, reject) {
        outbrainContentService.outbrainContentList(token, campOutbrain.id).then(data => {
            if (_.isEmpty(data.results)) return;
            resolve({ campaign: campOutbrain, contentUrl: data.results[0]['baseUrl'] })
        }, err => logger.error(err));
    });
}

/**
 * Update all campaigns status
 */
const updateCampaignStatus = new CronJob({
    cronTime: '*/17 * * * *',
    onTick: function () {
        Traffic.find({ status: true }, (err, res) => {
            if (err || !res) return;
            if (res) {
                const promises = [];
                _.forEach(res, traffic_source => {
                    promises.push(addCampaignStatus(traffic_source));
                });
                Promise.all(promises).then(response => {
                    logger.info("DONE: Updated Campaigns Status!");
                }).catch(e => logger.error(e));
            }
        });
    },
    start: false,
    timeZone: config.get('timezone'),
});
updateCampaignStatus.start();
logger.info(`[CRON_JOB]: Update Campaign Status = ${updateCampaignStatus.running}`);

function addCampaignStatus(traffic_source) {
    return new Promise(function (resolve, reject) {
        let accessKeys;
        switch (traffic_source.type) {
            case 'Taboola':
                accessKeys = _.pick(traffic_source, ['type', 'account_id', 'client_id', 'client_secret']);
                updateTaboolaCampaignStatus(accessKeys).then(res => {
                    resolve(res);
                }, err => logger.error(err));
                break;

            case 'Outbrain':
                accessKeys = _.pick(traffic_source, ['type', 'account_id', 'email', 'password']);
                updateOutbrainCampaignStatus(accessKeys).then(res => {
                    resolve(res);
                }, err => logger.error(err));
                break;
        }
    });
}

function updateTaboolaCampaignStatus(accessKeys) {
    return new Promise(function (resolve, reject) {
        Campaign.find({ type: 'Taboola', archived: 0, status: { $nin: ['RUNNING', 'DAILY CAPPED'] } }, (err, res) => {
            if (err) reject(err);
            if (!res) return;
            if (res) {
                const campaignDb = _.filter(res, o => o.id);
                if (_.isEmpty(campaignDb)) return;

                taboolaCampaignService.allTaboolaCampaigns(accessKeys).then(data => {
                    if (_.isEmpty(data.results)) return;

                    let promises = [];
                    _.forEach(campaignDb, camp => {
                        promises.push(pipeUpdateCampaignStatus(camp, data.results))
                    });
                    Promise.all(promises).then(response => {
                        resolve(response);
                    });
                }, err => reject(err));
            }
        });
    });
}

function updateOutbrainCampaignStatus(accessKeys) {
    return new Promise(function (resolve, reject) {
        Campaign.find({ type: 'Outbrain', archived: 0, status: { $nin: ['RUNNING', 'DAILY CAPPED'] } }, (err, res) => {
            if (err) reject(err);
            if (!res) return;
            if (res) {
                const campaignDb = _.filter(res, o => o.id);
                if (_.isEmpty(campaignDb)) return;

                outbrainCampaignService.outbrainCampaignList(accessKeys).then(data => {
                    if (_.isEmpty(data.results)) return;

                    let promises = [];
                    _.forEach(campaignDb, camp => {
                        promises.push(pipeUpdateCampaignStatus(camp, data.results))
                    });
                    Promise.all(promises).then(response => {
                        resolve(response);
                    });
                }, err => reject(err));
            }
        });
    });
}

function pipeUpdateCampaignStatus(camp, tsCamp) {
    return new Promise(function (resolve, reject) {
        tsCamp = _.find(tsCamp, ['id', camp.id]);
        if (!tsCamp) return;

        Campaign.findOneAndUpdate({ id: camp.id }, {
            cpc: tsCamp.cpc,
            minimumCpc: tsCamp.minimumCpc ? tsCamp.minimumCpc : 0,
            spending_limit: tsCamp.budget ? tsCamp.budget.amount : 0,
            daily_ad_delivery_model: tsCamp.budget ? tsCamp.budget.type : "",
            budget: tsCamp.budget ? tsCamp.budget : {},
            amountSpent: tsCamp.liveStatus ? tsCamp.liveStatus.amountSpent : 0,
            status: tsCamp.status,
            updated_at: Date.now(),
        }, (err, res) => {
            if (err) return;
            resolve({ success: true });
        });
    });
}

/**
 * Auto update stats campaigns from APIs
 */
const updateStatsCampaign = new CronJob({
    cronTime: '*/4 * * * *',
    onTick: function () {
        Campaign.find({ status: { $in: ['RUNNING', 'DAILY CAPPED'] }, archived: 0 }, (err, campaigns) => {
            if (err || !campaigns) return;

            const campaignFiltered = _.filter(campaigns, e => e.id && e.type && e.trafficSourceAccount && e.trackerAccount);
            if (_.isEmpty(campaignFiltered)) return;

            if (campaignFiltered) {
                let timeout = 0, promises = [];
                const start_date = moment().subtract(1, 'days').format(config.get('dateFormat'));
                const end_date = moment().format(config.get('dateFormat'));
                _.forEach(campaignFiltered, campaign => {
                    promises.push(campaignService.multipleUpdateStats(campaign, start_date, end_date, timeout));
                    promises.push(contentService.multipleUpdateStats(campaign, start_date, end_date, timeout));
                    promises.push(siteService.multipleUpdateStats(campaign, start_date, end_date, timeout));
                    timeout = timeout + 5000;
                });
                Promise.all(promises).then(res => {
                    logger.info("DONE: Updated Campaigns Stats!");
                }).catch(err => logger.error(err));
            }
        });
    },
    start: false,
    timeZone: config.get('timezone'),
});

updateStatsCampaign.start();
logger.info(`[CRON_JOB]: Update Stats Campaigns = ${updateStatsCampaign.running}`);