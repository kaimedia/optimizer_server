'use strict';

const _ = require('lodash');
const config = require('config');
const logger = require('../lib/logger');
const Promise = require('bluebird');
const dateFormat = require('dateformat');
const CronJob = require('cron').CronJob;
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const Campaign = require('../models/campaign');
const contentService = require('../shared/services/content');