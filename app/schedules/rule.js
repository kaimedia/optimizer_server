'use strict';

const _ = require('lodash');
const logger = require('../lib/logger');
const config = require('config');
const CronJob = require('cron').CronJob;
const Rule = require('../models/rule');
const ruleHelper = require('../rules/index');

Rule.find({ status: true }, (err, rules) => {
    if (err) return;

    if (_.isEmpty(rules)) return;

    _.forEach(rules, rule => {
        startRule(rule);
    });
});

function startRule(rule, start = true) {
    if (!rule) return;

    let rotation = ruleHelper.getCronTime(rule.rotation);
    let job = new CronJob({
        cronTime: rotation,
        onTick: function () {
            ruleHelper.ruleAction(rule);
        },
        start: false,
        timeZone: config.get('timezone'),
    });

    job.stop();

    if (start && rule.status)
        job.start();

    logger.info(`*RULE* ${rule.name} = ${job.running}`);
}

exports.startRule = startRule;