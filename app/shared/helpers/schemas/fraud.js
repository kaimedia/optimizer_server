'use strict';

const fraudStatsColumns = [
    'name',
    'total',
    'block',
    'allow',
];

exports.fraudStatsColumns = fraudStatsColumns;