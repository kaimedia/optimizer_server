'use strict';

const config = require('config');
const variablesTaboola = config.get('TABOOLA').variables;
const variablesOutbrain = config.get('OUTBRAIN').variables;

const taboolaDetailColumns = [
    'id',
    'name',
    'cpc',
    'spending_limit',
    'daily_cap',
    'daily_ad_delivery_model',
    'start_date',
    'is_active',
    'status',
    'publisher_targeting',
    'publisher_bid_modifier',
];

const outbrainDetailColumns = [
    'id',
    'name',
    'cpc',
    'minimumCpc',
    'budget',
    'liveStatus',
    'status',
    'blockedSites',
    'bids',
];

const taboolaCampaignColumns = [
    'id',
    'name',
    'spending_limit',
    'daily_cap',
    'daily_ad_delivery_model',
    'clicks',
    'spent',
    'ctr',
    'status',
    'impressions',
    'cpc',
    'cpm',
];

const voluumCampaignTaboolaColumns = [
    variablesTaboola.campaign_id,
    'conversions',
    'revenue',
    'visits',
    'clicks',
    'ctr',
    'epc',
    'cv',
    'ap',
];

const voluumCampaignOutbrainColumns = [
    variablesOutbrain.campaign_id,
    'conversions',
    'revenue',
    'visits',
    'clicks',
    'ctr',
    'epc',
    'cv',
    'ap',
];

const keyMapping = {
    id: "id",
    name: "name",
    status: "status",
    spending_limit: "spending_limit",
    daily_cap: "daily_cap",
    daily_ad_delivery_model: "daily_ad_delivery_model",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    cpc: "cpc",
    cpm: "cpm",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    visits_VL: "trk_clicks",
    clicks_VL: "lp_clicks",
    ctr_VL: "lp_ctr",
    epc_VL: "epc",
    cv_VL: "cv",
    ap_VL: "ap",
    impressions_AD: "rmk_impressions",
    clicks_AD: "rmk_clicks",
    conversions_AD: "rmk_conversions",
    spent_AD: "rmk_cost",
};

const keyMappingOutbrain = {
    id: "id",
    name: "name",
    status: "status",
    cpc: "cpc",
    cpm: "cpm",
    spending_limit: "spending_limit",
    daily_ad_delivery_model: "daily_ad_delivery_model",
    budget: "budget",
    amountSpent: "amountSpent",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    visits_VL: "trk_clicks",
    clicks_VL: "lp_clicks",
    ctr_VL: "lp_ctr",
    epc_VL: "epc",
    cv_VL: "cv",
    ap_VL: "ap",
    impressions_AD: "rmk_impressions",
    clicks_AD: "rmk_clicks",
    conversions_AD: "rmk_conversions",
    spent_AD: "rmk_cost",
};

const defaultFields = {
    impressions: 0,
    clicks: 0,
    ctr: 0,
    cpm: 0,
    conversions: 0, //VL
    revenue: 0, //VL
    trk_clicks: 0, //VL
    lp_clicks: 0, //VL
    lp_ctr: 0, //VL
    epc: 0, //VL
    cv: 0, //VL
    ap: 0,
    net: 0,
    roi: 0,
    epv: 0,
    cpv: 0,
    cpa: 0,
    rmk_impressions: 0,
    rmk_clicks: 0,
    rmk_conversions: 0,
    rmk_cost: 0,
};

exports.taboolaDetailColumns = taboolaDetailColumns;
exports.taboolaCampaignColumns = taboolaCampaignColumns;
exports.voluumCampaignTaboolaColumns = voluumCampaignTaboolaColumns;
exports.keyMapping = keyMapping;
exports.defaultFields = defaultFields;
exports.outbrainDetailColumns = outbrainDetailColumns;
exports.voluumCampaignOutbrainColumns = voluumCampaignOutbrainColumns;
exports.keyMappingOutbrain = keyMappingOutbrain;