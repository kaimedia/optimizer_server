'use strict';

const config = require('config');
const variablesTaboola = config.get('TABOOLA').variables;
const variablesOutbrain = config.get('OUTBRAIN').variables;

const taboolaSiteColumns = [
    'camp_name',
    'camp_id',
    'site',
    'site_name',
    'blocking_level',
    'impressions',
    'clicks',
    'spent',
    'ctr',
    'cpc',
    'cpm',
    'configured_cpc',
    'cpc_modification',
    'status',
];

const voluumSiteColumns = [
    variablesTaboola.campaign_id,
    variablesTaboola.site,
    'clicks',
    'visits',
    'conversions',
    'revenue',
    'epc',
    'ctr',
    'cv',
    'ap',
];

const voluumSectionOutbrainColumns = [
    variablesOutbrain.campaign_id,
    variablesOutbrain.section_id,
    'clicks',
    'visits',
    'conversions',
    'revenue',
    'epc',
    'ctr',
    'cv',
    'ap',
    'auto_update_cpc',
];

const keyMapping = {
    camp_name: "camp_name",
    camp_id: "camp_id",
    site: "site_id",
    site_name: "site_name",
    blocking_level: "blocking_level",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    cpc: "cpc",
    cpm: "cpm",
    status: "status",
    configured_cpc: "configured_cpc",
    cpc_modification: "cpc_boost",
    cpc_boost_percent: "cpc_boost_percent",
    boosted_cpc: "boosted_cpc",
    clicks_VL: "lp_clicks",
    visits_VL: "trk_clicks",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    epc_VL: "epc",
    ctr_VL: "lp_ctr",
    cv_VL: "cv",
    ap_VL: "ap",
    total_PT: "total_bot",
    block_PT: "block_bot",
    allow_PT: "allow_bot",
    auto_update_cpc: "auto_update_cpc",
};

const keyMappingOutbrain = {
    camp_name: "camp_name",
    camp_id: "camp_id",
    site_id: "site_id",
    site_name: "site_name",
    site_url: "site_url",
    publisher_id: "publisher_id",
    publisher_name: "publisher_name",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    cpm: "cpm",
    cpc: "cpc",
    status: "status",
    configured_cpc: "configured_cpc",
    cpc_boost: "cpc_boost",
    cpc_boost_percent: "cpc_boost_percent",
    boosted_cpc: "boosted_cpc",
    clicks_VL: "lp_clicks",
    visits_VL: "trk_clicks",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    epc_VL: "epc",
    ctr_VL: "lp_ctr",
    cv_VL: "cv",
    ap_VL: "ap",
    total_PT: "total_bot",
    block_PT: "block_bot",
    allow_PT: "allow_bot",
    auto_update_cpc: "auto_update_cpc",
};

const defaultFields = {
    cpm: 0,
    cpc_boost: "",
    lp_clicks: 0, //VL
    trk_clicks: 0, //VL
    conversions: 0, //VL
    revenue: 0, //VL
    epc: 0, //VL
    lp_ctr: 0, //VL
    cv: 0, //VL
    ap: 0,
    total_bot: 0,
    block_bot: 0,
    allow_bot: 0,
    net: 0,
    roi: 0,
    bot: 0,
    epv: 0,
    cpv: 0,
    cpa: 0,
    auto_update_cpc: true,
};

exports.taboolaSiteColumns = taboolaSiteColumns;
exports.voluumSiteColumns = voluumSiteColumns;
exports.keyMapping = keyMapping;
exports.defaultFields = defaultFields;
exports.voluumSectionOutbrainColumns = voluumSectionOutbrainColumns;
exports.keyMappingOutbrain = keyMappingOutbrain;