'use strict';


const config = require('config');
const variablesTaboola = config.get('TABOOLA').variables;
const variablesOutbrain = config.get('OUTBRAIN').variables;

const taboolaContentColumns = [
    'id',
    'campaign_id',
    'title',
    'thumbnail_url',
    'url',
    'status',
    'is_active',
    'impressions',
    'clicks',
    'ctr',
    'spent',
    'cpc',
    'cpm',
];

const voluumContentColumns = [
    variablesTaboola.campaign_item_id,
    'visits',
    'clicks',
    'epc',
    'ctr',
    'conversions',
    'revenue',
    'cv',
    'ap',
];

const voluumContentOutbrainColumns = [
    variablesOutbrain.ad_id,
    'visits',
    'clicks',
    'epc',
    'ctr',
    'conversions',
    'revenue',
    'cv',
    'ap',
];

const keyMapping = {
    id: "id",
    campaign_id: "campaign_id",
    title: "title",
    thumbnail_url: "thumbnail_url",
    url: "url",
    status: "status",
    is_active: "is_active",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    cpc: "cpc",
    cpm: "cpm",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    visits_VL: "trk_clicks",
    clicks_VL: "lp_clicks",
    ctr_VL: "lp_ctr",
    epc_VL: "epc",
    cv_VL: "cv",
    ap_VL: "ap",
};

const keyMappingOutbrain = {
    id: "id",
    campaign_id: "campaign_id",
    title: "title",
    thumbnail_url: "thumbnail_url",
    url: "url",
    status: "status",
    is_active: "is_active",
    impressions: "impressions",
    clicks: "clicks",
    spent: "cost",
    ctr: "ctr",
    cpc: "cpc",
    cpm: "cpm",
    conversions_VL: "conversions",
    revenue_VL: "revenue",
    visits_VL: "trk_clicks",
    clicks_VL: "lp_clicks",
    ctr_VL: "lp_ctr",
    epc_VL: "epc",
    cv_VL: "cv",
    ap_VL: "ap",
};

const defaultFields = {
    impressions: 0,
    clicks: 0,
    ctr: 0,
    cpm: 0,
    conversions: 0, //VL
    revenue: 0, //VL
    trk_clicks: 0, //VL
    lp_clicks: 0, //VL
    lp_ctr: 0, //VL
    epc: 0, //VL
    cv: 0, //VL
    ap: 0,
    net: 0,
    roi: 0,
    epv: 0,
    cpv: 0,
    cpa: 0,
};

exports.taboolaContentColumns = taboolaContentColumns;
exports.voluumContentColumns = voluumContentColumns;
exports.keyMapping = keyMapping;
exports.defaultFields = defaultFields;
exports.voluumContentOutbrainColumns = voluumContentOutbrainColumns;
exports.keyMappingOutbrain = keyMappingOutbrain;