'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const __ = require('../../helpers/api');

function convertSummary(data, field = null) {
    return new Promise(function (resolve, reject) {
        if (field) {
            data = _.map(data, e => e[field]);
        }
        let result = [], i,
            totalClicks = 0,
            totalCost = 0,
            totalCPC = 0,
            totalImpressions = 0,
            totalCTR = 0,
            totalCPM = 0,
            totalAP = 0,
            totalLPClicks = 0,
            totalTrkClicks = 0,
            totalConv = 0,
            totalRev = 0,
            totalEPC = 0,
            totalLPCtr = 0,
            totalCV = 0,
            totalNET = 0,
            totalROI = 0,
            totalEPV = 0,
            totalCPV = 0,
            totalCPA = 0,
            totalRMKImpressions = 0,
            totalRMKClicks = 0,
            totalRMKConversions = 0,
            totalRMKCost = 0;
        for (i = 0; i < data.length; i++) {
            totalClicks += data[i].clicks;
            totalCost += data[i].cost;
            totalCPC = __.divide(totalCost, totalClicks);
            totalImpressions += data[i].impressions;
            totalCTR = __.divide(totalClicks, totalImpressions) * 100;
            totalCPM = __.divide(totalCost, totalImpressions / 1000);
            totalAP = data[i].ap === 0 ? totalAP : data[i].ap;
            totalLPClicks += data[i].lp_clicks;
            totalTrkClicks += data[i].trk_clicks;
            totalLPCtr = __.divide(totalLPClicks, totalTrkClicks) * 100;
            totalConv += data[i].conversions;
            totalRev += data[i].revenue;
            totalEPC = __.divide(totalRev, totalLPClicks);
            totalCV = __.divide(totalConv, totalTrkClicks) * 100;
            totalNET = totalRev - totalCost;
            totalROI = __.divide(totalNET, totalCost) * 100;
            totalEPV = __.divide(totalRev, totalTrkClicks);
            totalCPV = __.divide(totalCost, totalTrkClicks);
            totalCPA = __.divide(totalCost, totalConv);
            if (data[i].rmk_impressions) {
                totalRMKImpressions += data[i].rmk_impressions;
            }
            if (data[i].rmk_impressions) {
                totalRMKClicks += data[i].rmk_clicks;
            }
            if (data[i].rmk_impressions) {
                totalRMKConversions += data[i].rmk_conversions;
            }
            if (data[i].rmk_impressions) {
                totalRMKCost += data[i].rmk_cost;
            }
        }
        result.push({
            clicks: totalClicks,
            cost: _.round(totalCost, 2),
            cpc: _.round(totalCPC, 2),
            impressions: totalImpressions,
            ctr: _.round(totalCTR, 2),
            cpm: _.round(totalCPM, 2),
            ap: totalAP,
            lp_clicks: totalLPClicks,
            trk_clicks: totalTrkClicks,
            lp_ctr: _.round(totalLPCtr, 2),
            conversions: totalConv,
            revenue: _.round(totalRev, 2),
            epc: _.round(totalEPC, 2),
            cv: _.round(totalCV, 2),
            net: _.round(totalNET, 2),
            roi: _.round(totalROI, 2),
            epv: _.round(totalEPV, 4),
            cpv: _.round(totalCPV, 4),
            cpa: _.round(totalCPA, 3),
            rmk_impressions: totalRMKImpressions,
            rmk_clicks: totalRMKClicks,
            rmk_conversions: totalRMKConversions,
            rmk_cost: _.round(totalRMKCost, 2),
        });
        resolve(result);
    });
}

function convertMetricStep1(data) {
    return new Promise(function (resolve, reject) {
        let tmp, result = [], i, j;
        for (i = 0; i < data.length; i++) {
            tmp = data[i].rows;
            for (j = 0; j < tmp.length; j++) {
                result.push(tmp[j]);
            }
        }
        resolve(result);
    });
}

function convertMetricStep2(data, field) {
    return new Promise(function (resolve, reject) {
        let result = {}, i, tmp;
        for (i = 0; i < data.length; i++) {
            tmp = data[i];
            if (!result[tmp[field]]) {
                result[tmp[field]] = [];
            }
            result[tmp[field]].push(tmp);
        }
        resolve(result);
    });
}

exports.convertSummary = convertSummary;
exports.convertMetricStep1 = convertMetricStep1;
exports.convertMetricStep2 = convertMetricStep2;