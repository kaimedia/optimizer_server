'use strict';

const _ = require('lodash');
const __ = require('../../helpers/api');
const logger = require('../../lib/logger');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const helperService = require('../helpers/service');
const Traffic = require('../../models/traffic');
const Tracker = require('../../models/tracker');
const Campaign = require('../../models/campaign');
const Content = require('../../models/content');
const ContentMetrics = require('../../models/content.metrics');
const taboolaContentService = require('./taboola/content');
const outbrainContentService = require('./outbrain/content');

// Begin raw content metrics data
function updateStats(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findById(campaign_id, (err, campaign) => {
            if (err) return reject(err);

            if (!campaign || !campaign.id || !campaign.type || !campaign.trafficSourceAccount || !campaign.trackerAccount)
                return reject('Invalid campaign');

            multipleUpdateStats(campaign, start_date, end_date).then(res => {
                resolve(res);
            }).catch(err => reject(err));
        });
    });
}

function multipleUpdateStats(campaign, start_date, end_date, timeout = 0) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            const promises = [
                new Promise((resolve, reject) => {
                    Traffic.findById(campaign.trafficSourceAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                }),
                new Promise((resolve, reject) => {
                    Tracker.findById(campaign.trackerAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                })
            ];
            Promise.all(promises).then(accessAccounts => {
                let timeout = 0, daysOfYear = [];

                for (let day = moment(moment(start_date)); day <= moment(end_date); day.add(1, 'days')) {
                    const thisDate = day.format(config.get('dateFormat'));
                    daysOfYear.push(updateStatsByDates(accessAccounts, campaign, thisDate, thisDate, timeout));
                    timeout = timeout + 2000;
                }

                Promise.all(daysOfYear).then(res => {
                    resolve(res);
                }).catch(err => logger.error(err));
            }).catch(err => logger.error(err));
        }, timeout);
    });
}

function updateStatsByDates(accounts, campaign, start_date, end_date, timeout) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            let accessTraffic;
            let accessTracker = _.pick(accounts[1], ['access_id', 'access_key']);
            switch (campaign.type) {
                case 'Taboola':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'client_id', 'client_secret']);
                    taboolaContentService.taboolaContentMetrics(accessTraffic, accessTracker, campaign, start_date, end_date).then(res => {
                        resolve(res);
                    }, err => logger.error(err));
                    break;
                case 'Outbrain':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'email', 'password']);
                    outbrainContentService.contentSummary(accessTraffic, accessTracker, campaign.id, start_date, end_date, 0).then(results => {
                        if (results.totalRows === 0)
                            return;

                        saveContentDatas(campaign, results, results.summary).then(res => {
                            resolve(res);
                        }, err => logger.error(err));
                    }, err => logger.error(err));
            }
        }, timeout);
    });
}

function saveContentDatas(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        let promises = [
            saveContentMetrics(campaign, result, totals),
            saveContent(campaign, result)
        ];
        Promise.all(promises).then(res => {
            resolve(res);
        }).catch(err => logger.error(err));
    });
}

function saveContentMetrics(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        ContentMetrics.findOne({
            start_date: result.start_date,
            end_date: result.end_date,
            campaign_id: campaign.id,
            trafficSource: campaign.type
        }, (err, res) => {
            if (err) throw err;
            const contentList = _.map(result.contents, e => _.omit(e, ['campaign_id', 'thumbnail_url', 'url', 'is_active', 'status']));
            if (!res) {
                //add
                const metrics = {
                    last_rawdata_update_time: result.timestamp,
                    last_rawdata_update_timestamp: moment().unix(),
                    start_date: result.start_date,
                    end_date: result.end_date,
                    campaign_id: campaign.id,
                    campaign_name: campaign.name,
                    trafficSource: campaign.type,
                    totalRows: result.totalRows,
                    totals: totals,
                    rows: contentList
                };
                ContentMetrics(metrics).save((err) => {
                    if (err) throw err;
                    resolve('Content Metrics created!');
                });
            } else {
                //update
                res.updated_at = Date.now();
                res.last_rawdata_update_time = result.timestamp;
                res.last_rawdata_update_timestamp = moment().unix();
                res.totalRows = result.totalRows;
                res.totals = totals;
                res.rows = contentList;
                res.save((err) => {
                    if (err) throw err;
                    resolve('Content Metrics updated!');
                });
            }
        });
    });
}

function saveContent(campaign, result) {
    return new Promise(function (resolve, reject) {
        _.forEach(result.contents, e => {
            Content.findOne({ id: e.id }, (err, res) => {
                if (err) throw err;
                if (!res) {
                    //add
                    const rowItem = {
                        id: e.id,
                        name: e.title,
                        thumbnail_url: e.thumbnail_url,
                        url: e.url,
                        campaign_id: e.campaign_id,
                        trafficSource: campaign.type,
                        is_active: e.is_active,
                        status: e.status
                    };

                    Content(rowItem).save((err) => {
                        if (err) throw err;
                        resolve('Content Metadata created!');
                    });
                } else {
                    //update
                    res.updated_at = Date.now();
                    res.name = e.title;
                    res.thumbnail_url = e.thumbnail_url;
                    res.url = e.url;
                    res.is_active = e.is_active;
                    res.status = e.status;
                    res.save((err) => {
                        if (err) throw err;
                        resolve('Content Metadata updated!');
                    });
                }
            });
        });
    });
}
// End raw content metrics data

// Begin content site metrics
function getContentMetrics(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, camp) => {
            if (err) reject(err);
            ContentMetrics.find({
                trafficSource: camp.type,
                campaign_id: campaign_id,
                start_date: { $gte: start_date },
                end_date: { $lte: end_date }
            }, (err, response) => {
                if (err) reject(err);
                let promises = [
                    helperService.convertSummary(response, 'totals'),
                    convertMetrics(response),
                ];
                Promise.all(promises).then(results => {
                    let data = {
                        start_date: start_date,
                        end_date: end_date,
                        summary: results[0][0],
                    };
                    let contentList = results[1];
                    let contentIds = _.map(contentList, e => e.id);
                    contentMetadatas(contentIds, camp.type, campaign_id).then(metas => {
                        contentList = _.map(contentList, e => _.assign(e, _.find(metas, ['id', e.id])));
                        data.totalRows = _.size(contentList);
                        data.contents = contentList;
                        resolve(data);
                    }, err => reject(err));
                });
            });
        });
    });
}

function convertMetrics(data) {
    return new Promise(function (resolve, reject) {
        helperService.convertMetricStep1(data).then(step1 => {
            helperService.convertMetricStep2(step1, 'id').then(step2 => {
                convertMetricFinal(step2).then(final => {
                    resolve(final);
                }, err => reject(err));
            }, err => reject(err));
        }, err => reject(err));
    });
}

function convertMetricFinal(data) {
    return new Promise(function (resolve, reject) {
        let result = [], i, j, tmp,
            ID, NAME, totalCPC, totalClicks, totalCost, totalImpressions, totalCTR, totalCPM, totalAP, totalLPClicks, totalTrkClicks, totalConv, totalRev,
            totalEPC, totalLPCtr, totalCV, totalNET, totalROI, totalEPV, totalCPV, totalCPA;

        for (i in data) {
            tmp = data[i];
            totalClicks = 0;
            totalCost = 0;
            totalCPC = 0;
            totalImpressions = 0;
            totalCTR = 0;
            totalCPM = 0;
            totalAP = 0;
            totalLPClicks = 0;
            totalTrkClicks = 0;
            totalConv = 0;
            totalRev = 0;
            totalEPC = 0;
            totalLPCtr = 0;
            totalCV = 0;
            totalNET = 0;
            totalROI = 0;
            totalEPV = 0;
            totalCPV = 0;
            totalCPA = 0;
            for (j = 0; j < tmp.length; j++) {
                ID = tmp[j].id;
                NAME = tmp[j].title;
                totalClicks += tmp[j].clicks;
                totalCost += tmp[j].cost;
                totalCPC = __.divide(totalCost, totalClicks);
                totalImpressions += tmp[j].impressions;
                totalCTR = __.divide(totalClicks, totalImpressions) * 100;
                totalCPM = __.divide(totalCost, totalImpressions / 1000);
                totalAP = tmp[j].ap === 0 ? totalAP : tmp[j].ap;
                totalLPClicks += tmp[j].lp_clicks;
                totalTrkClicks += tmp[j].trk_clicks;
                totalLPCtr = __.divide(totalLPClicks, totalTrkClicks) * 100;
                totalConv += tmp[j].conversions;
                totalRev += tmp[j].revenue;
                totalEPC = __.divide(totalRev, totalLPClicks);
                totalCV = __.divide(totalConv, totalTrkClicks) * 100;
                totalNET = totalRev - totalCost;
                totalROI = __.divide(totalNET, totalCost) * 100;
                totalEPV = __.divide(totalRev, totalTrkClicks);
                totalCPV = __.divide(totalCost, totalTrkClicks);
                totalCPA = __.divide(totalCost, totalConv);
            }
            result.push({
                id: ID,
                title: NAME,
                clicks: totalClicks,
                cost: _.round(totalCost, 2),
                cpc: _.round(totalCPC, 2),
                impressions: totalImpressions,
                ctr: _.round(totalCTR, 2),
                cpm: _.round(totalCPM, 2),
                ap: totalAP,
                lp_clicks: totalLPClicks,
                trk_clicks: totalTrkClicks,
                lp_ctr: _.round(totalLPCtr, 2),
                conversions: totalConv,
                revenue: _.round(totalRev, 2),
                epc: _.round(totalEPC, 2),
                cv: _.round(totalCV, 2),
                net: _.round(totalNET, 2),
                roi: _.round(totalROI, 2),
                epv: _.round(totalEPV, 4),
                cpv: _.round(totalCPV, 4),
                cpa: _.round(totalCPA, 3),
            });
        }
        resolve(result);
    });
}

function contentMetadatas(siteIds, trafficSource, campaign_id) {
    return new Promise(function (resolve, reject) {
        Content.find({
            id: { $in: siteIds },
            campaign_id: campaign_id,
            trafficSource: trafficSource,
        }, (err, results) => {
            if (err) reject(err);
            resolve(_.map(results, e => {
                return {
                    id: e.id,
                    title: e.name,
                    thumbnail_url: e.thumbnail_url,
                    url: e.url,
                    campaign_id: e.campaign_id,
                    is_active: e.is_active,
                    status: e.status,
                };
            }));
        });
    });
}
// End get content metrics

exports.updateStats = updateStats;
exports.multipleUpdateStats = multipleUpdateStats;
exports.saveContentDatas = saveContentDatas;
exports.getContentMetrics = getContentMetrics;