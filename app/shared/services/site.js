'use strict';

const _ = require('lodash');
const __ = require('../../helpers/api');
const config = require('config');
const logger = require('../../lib/logger');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const helperService = require('../helpers/service');
const Traffic = require('../../models/traffic');
const Tracker = require('../../models/tracker');
const Campaign = require('../../models/campaign');
const User = require('../../models/user');
const Site = require('../../models/site');
const SiteMetrics = require('../../models/site.metrics');
const SiteMetadata = require('../../models/site.metadata');
const taboolaSiteService = require('./taboola/site');
const outbrainSiteService = require('./outbrain/site');

function getList(trafficSource, status) {
    return new Promise(function (resolve, reject) {
        let where = {};

        if (trafficSource !== 'all')
            where.trafficSource = _.upperFirst(trafficSource);

        if (status !== 'all')
            where.status = _.toUpper(status);

        Site.find(where, (err, data) => {
            if (err) return reject(err);
            resolve(data);
        });
    });
}

function saveSiteStatus(item, status, trafficSource, userId = null) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            status = _.toUpper(status);
            let metrics = _.omit(item, ['site_id', 'site_name', 'site_url', 'status', 'selected']);
            metrics.time = Date.now();

            checkUserExist(userId).then(user => {
                if (user !== false) {
                    metrics.user_id = userId;
                    metrics.user_name = `${user.firstName} ${user.lastName}`;
                }
                Site.findOne({ site_id: item.site_id }, {}, (err, site) => {
                    if (err) return logger.error(err);
                    if (!site) {
                        const newSite = Site({
                            site_id: item.site_id,
                            site_name: item.site_name,
                            trafficSource: trafficSource,
                            status: status,
                            metrics: [
                                metrics
                            ],
                        });
                        newSite.save(err => {
                            if (err) return logger.error(err);
                            resolve({ success: true, message: "Saved site status." });
                        });
                    } else {
                        site.status = status;
                        site.updated_at = Date.now();

                        const checkSite = _.find(site.metrics, ['camp_id', item.camp_id]);
                        if (_.isUndefined(checkSite) || _.isEmpty(checkSite)) {
                            site.metrics.push(metrics);
                        }
                        site.save(err => {
                            if (err) return logger.error(err);
                            resolve({ success: true, message: "Updated site status." });
                        });
                    }
                });
            }, err => logger.error(err));
        }, 500);
    });
}

function checkUserExist(userId = null) {
    return new Promise(function (resolve, reject) {
        if (!userId) resolve(false);
        User.findById(userId, (err, user) => {
            if (err) return logger.error(err);
            if (user) resolve(user);
        });
    });
}

function removeBlockedSites(sites, trafficSource, campaign_id) {
    return new Promise(function (resolve, reject) {
        let promises = [];
        _.forEach(sites, site => {
            promises.push(removeBlockedSiteItem(site, trafficSource));
            promises.push(saveSiteMetadataStatus(site, 'active', trafficSource, campaign_id));
        });
        Promise.all(promises).then(response => {
            resolve(response);
        }).catch(err => reject(err));
    });
}

function removeBlockedSiteItem(item, trafficSource) {
    return new Promise(function (resolve, reject) {
        const where = {
            site_id: item.site_id,
            trafficSource: trafficSource
        };
        Site.findOne(where, (err, site) => {
            if (err || !site) return;

            let metrics = site.metrics ? site.metrics : [];
            _.remove(metrics, { camp_id: item.camp_id });

            site.metrics = metrics;
            site.updated_at = Date.now();

            if (_.isEmpty(metrics)) {
                Site.findOneAndRemove(where, (err, res) => {
                    if (err) return;
                    resolve({ success: true, message: 'Success.' });
                });
            } else {
                Site.findOneAndUpdate(where, site, (err, res) => {
                    if (err) return;
                    resolve({ success: true, message: 'Success.' });
                });
            }
        });
    });
}

function blockedSites(trafficSource) {
    return new Promise(function (resolve, reject) {
        Site.find({ trafficSource: trafficSource, status: ['BLOCKED', 'PAUSE'] }, (err, data) => {
            if (err) return reject(err);
            resolve(data);
        });
    });
}

function saveSiteMetadataStatus(item, status, trafficSource) {
    return new Promise(function (resolve, reject) {
        const where = {
            site_id: item.site_id,
            trafficSource: trafficSource
        };
        SiteMetadata.findOne(where, (err, site) => {
            if (err || !site)
                return;
            if (site) {
                const metadata = _.map(site.metadata, e => {
                    if (e.camp_id === item.camp_id) {
                        e.status = _.toUpper(status);
                    }
                    return e;
                });
                SiteMetadata.findOneAndUpdate(where, {
                    updated_at: Date.now(),
                    metadata: metadata
                }, (err, res) => {
                    if (err) return logger.error(err);
                    resolve({ success: true, message: "Updated site metadata status." });
                })
            }
        });
    });
}

function saveSiteMetadataCpc(item, trafficSource) {
    return new Promise(function (resolve, reject) {
        const where = {
            site_id: item.site_id,
            trafficSource: trafficSource
        };
        SiteMetadata.findOne(where, (err, site) => {
            if (err || !site)
                return;
            if (site) {
                const metadata = _.map(site.metadata, e => {
                    if (e.camp_id === item.camp_id) {
                        e.configured_cpc = item.configured_cpc;
                        e.cpc_boost = item.cpc_boost;
                        e.cpc_boost_percent = item.cpc_boost_percent;
                        e.boosted_cpc = item.boosted_cpc;
                    }
                    return e;
                });
                SiteMetadata.findOneAndUpdate(where, {
                    updated_at: Date.now(),
                    metadata: metadata
                }, (err, res) => {
                    if (err) return logger.error(err);
                    resolve({ success: true, message: "Updated site metadata status." });
                })
            }
        });
    });
}

function updateAutoChangeCpcStatus(trafficSource, item, status) {
    return new Promise(function (resolve, reject) {
        const where = {
            site_id: item.site_id,
            trafficSource: trafficSource
        };
        SiteMetadata.findOne(where, (err, site) => {
            if (err || !site)
                return;
            if (site) {
                const metadata = _.map(site.metadata, e => {
                    if (e.camp_id === item.camp_id) {
                        e.auto_update_cpc = (status === 'true') ? false : true;
                    }
                    return e;
                });
                SiteMetadata.findOneAndUpdate(where, {
                    updated_at: Date.now(),
                    metadata: metadata
                }, (err, res) => {
                    if (err) return logger.error(err);
                    resolve({ success: true, message: "Updated site metadata status." });
                })
            }
        });
    });
}

function updateSitesConfiguredCpc(campaign_id, cpc) {
    return new Promise(function (resolve, reject) {
        SiteMetadata.find({ 'metadata.camp_id': campaign_id }, (err, sites) => {
            if (err || !sites) return;
            if (sites) {
                let promises = [];
                _.forEach(sites, site => {
                    promises.push(
                        new Promise((resolve, reject) => {
                            SiteMetadata.findById(site._id, (err, res) => {
                                if (err || !res) return;
                                if (res) {
                                    const metadata = _.map(res.metadata, e => {
                                        if (e.camp_id === campaign_id) {
                                            e.configured_cpc = cpc;
                                            e.boosted_cpc = _.round(_.add(_.multiply(cpc, e.cpc_boost), cpc), 3);
                                        }
                                        return e;
                                    });
                                    SiteMetadata.findByIdAndUpdate(site._id, {
                                        updated_at: Date.now(),
                                        metadata: metadata
                                    }, (err, res) => {
                                        if (err) return;
                                        resolve({ success: true });
                                    });
                                }
                            });
                        })
                    );
                });
                Promise.all(promises).then(res => {
                    resolve(res);
                }).catch(err => logger.error(err));
            }
        });
    });
}

// Begin raw site metrics data
function updateStats(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findById(campaign_id, (err, campaign) => {
            if (err) return reject(err);

            if (!campaign || !campaign.id || !campaign.type || !campaign.trafficSourceAccount || !campaign.trackerAccount)
                return reject('Invalid campaign');

            multipleUpdateStats(campaign, start_date, end_date).then(res => {
                resolve(res);
            }).catch(err => reject(err));
        });
    });
}

function multipleUpdateStats(campaign, start_date, end_date, timeout = 0) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            const promises = [
                new Promise((resolve, reject) => {
                    Traffic.findById(campaign.trafficSourceAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                }),
                new Promise((resolve, reject) => {
                    Tracker.findById(campaign.trackerAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                })
            ];
            Promise.all(promises).then(accessAccounts => {
                let timeout = 0, daysOfYear = [];

                for (let day = moment(moment(start_date)); day <= moment(end_date); day.add(1, 'days')) {
                    const thisDate = day.format(config.get('dateFormat'));
                    daysOfYear.push(updateStatsByDates(accessAccounts, campaign, thisDate, thisDate, timeout));
                    timeout = timeout + 2000;
                }

                Promise.all(daysOfYear).then(res => {
                    resolve(res);
                }).catch(err => logger.error(err));
            }).catch(err => logger.error(err));
        }, timeout);
    });
}

function updateStatsByDates(accounts, campaign, start_date, end_date, timeout) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            let accessTraffic;
            let accessTracker = _.pick(accounts[1], ['access_id', 'access_key']);
            switch (campaign.type) {
                case 'Taboola':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'client_id', 'client_secret']);
                    taboolaSiteService.taboolaSiteMetrics(accessTraffic, accessTracker, campaign, start_date, end_date).then(res => {
                        resolve(res);
                    }, err => logger.error(err));
                    break;
                case 'Outbrain':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'email', 'password']);
                    outbrainSiteService.sectionSummary(accessTraffic, accessTracker, campaign.id, start_date, end_date, false).then(results => {
                        if (results.totalRows === 0)
                            return;

                        saveSiteDatas(campaign, results, results.summary).then(res => {
                            resolve(res);
                        }, err => logger.error(err));
                    }, err => logger.error(err));
            }
        }, timeout);
    });
}

function saveSiteDatas(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        let promises = [
            saveSiteMetrics(campaign, result, totals),
            saveSiteMetadata(campaign, result)
        ];
        Promise.all(promises).then(res => {
            resolve(res);
        }).catch(err => logger.error(err));
    });
}

function saveSiteMetrics(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        SiteMetrics.findOne({
            start_date: result.start_date,
            end_date: result.end_date,
            campaign_id: campaign.id,
            trafficSource: campaign.type
        }, (err, res) => {
            if (err) throw err;
            const siteList = _.map(result.sites, e => _.omit(e, [
                'camp_id', 'camp_name', 'site_url', 'publisher_id', 'publisher_name', 'status',
                'configured_cpc', 'cpc_boost', 'cpc_boost_percent', 'boosted_cpc', 'auto_update_cpc'
            ]));
            if (!res) {
                //add
                const metrics = {
                    last_rawdata_update_time: result.timestamp,
                    last_rawdata_update_timestamp: moment().unix(),
                    start_date: result.start_date,
                    end_date: result.end_date,
                    campaign_id: campaign.id,
                    campaign_name: campaign.name,
                    trafficSource: campaign.type,
                    totalRows: result.totalRows,
                    totals: totals,
                    rows: siteList
                };
                SiteMetrics(metrics).save((err) => {
                    if (err) throw err;
                    resolve('Site Metrics created!');
                });
            } else {
                //update
                res.updated_at = Date.now();
                res.last_rawdata_update_time = result.timestamp;
                res.last_rawdata_update_timestamp = moment().unix();
                res.totalRows = result.totalRows;
                res.totals = totals;
                res.rows = siteList;
                res.save((err) => {
                    if (err) throw err;
                    resolve('Site Metrics updated!');
                });
            }
        });
    });
}

function saveSiteMetadata(campaign, result) {
    return new Promise(function (resolve, reject) {
        _.forEach(result.sites, e => {
            const metadata = [
                {
                    camp_id: e.camp_id,
                    camp_name: e.camp_name,
                    status: e.status,
                    configured_cpc: e.configured_cpc,
                    cpc_boost: e.cpc_boost,
                    cpc_boost_percent: e.cpc_boost_percent,
                    boosted_cpc: e.boosted_cpc,
                    auto_update_cpc: e.auto_update_cpc
                }
            ];

            SiteMetadata.findOne({ site_id: e.site_id }, (err, res) => {
                if (err) throw err;
                if (!res) {
                    //add
                    const rowItem = {
                        site_id: e.site_id,
                        site_name: e.site_name,
                        site_url: e.site_url ? e.site_url : "",
                        publisher_id: e.publisher_id ? e.publisher_id : "",
                        publisher_name: e.publisher_name ? e.publisher_name : "",
                        trafficSource: campaign.type,
                        metadata: metadata
                    };

                    SiteMetadata(rowItem).save((err) => {
                        if (err) throw err;
                        resolve('Site Metadata created!');
                    });
                } else {
                    //update
                    res.updated_at = Date.now();
                    res.metadata = _.unionBy(metadata, res.metadata, 'camp_id');
                    res.save((err) => {
                        if (err) throw err;
                        resolve('Site Metadata updated!');
                    });
                }
            });
        });
    });
}
// End raw site metrics data

// Begin get site metrics
function getSiteMetrics(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, camp) => {
            if (err) reject(err);
            SiteMetrics.find({
                trafficSource: camp.type,
                campaign_id: campaign_id,
                start_date: { $gte: start_date },
                end_date: { $lte: end_date }
            }, (err, response) => {
                if (err) reject(err);
                let promises = [
                    helperService.convertSummary(response, 'totals'),
                    convertMetrics(response),
                ];
                Promise.all(promises).then(results => {
                    let data = {
                        start_date: start_date,
                        end_date: end_date,
                        summary: results[0][0],
                    };
                    let siteList = results[1];
                    let siteIds = _.map(siteList, e => e.site_id);
                    siteMetadatas(siteIds, camp.type, campaign_id).then(metas => {
                        siteList = _.map(siteList, e => _.assign(e, _.find(metas, ['site_id', e.site_id])));
                        data.totalRows = _.size(siteList);
                        data.sites = siteList;
                        resolve(data);
                    }, err => reject(err));
                });
            });
        });
    });
}

function convertMetrics(data) {
    return new Promise(function (resolve, reject) {
        helperService.convertMetricStep1(data).then(step1 => {
            helperService.convertMetricStep2(step1, 'site_id').then(step2 => {
                convertMetricFinal(step2).then(final => {
                    resolve(final);
                }, err => reject(err));
            }, err => reject(err));
        }, err => reject(err));
    });
}

function convertMetricFinal(data) {
    return new Promise(function (resolve, reject) {
        let result = [], i, j, tmp,
            ID, NAME, totalCPC, totalClicks, totalCost, totalImpressions, totalCTR, totalCPM, totalAP, totalLPClicks, totalTrkClicks, totalConv, totalRev,
            totalEPC, totalLPCtr, totalCV, totalNumBot, totalBlockBot, totalAllowBot, totalNET, totalROI, totalBot, totalEPV, totalCPV, totalCPA;

        for (i in data) {
            tmp = data[i];
            totalClicks = 0;
            totalCost = 0;
            totalCPC = 0;
            totalImpressions = 0;
            totalCTR = 0;
            totalCPM = 0;
            totalAP = 0;
            totalLPClicks = 0;
            totalTrkClicks = 0;
            totalConv = 0;
            totalRev = 0;
            totalEPC = 0;
            totalLPCtr = 0;
            totalCV = 0;
            totalNumBot = 0;
            totalBlockBot = 0;
            totalAllowBot = 0;
            totalBot = 0;
            totalNET = 0;
            totalROI = 0;
            totalEPV = 0;
            totalCPV = 0;
            totalCPA = 0;
            for (j = 0; j < tmp.length; j++) {
                ID = tmp[j].site_id;
                NAME = tmp[j].site_name;
                totalClicks += tmp[j].clicks;
                totalCost += tmp[j].cost;
                totalCPC = __.divide(totalCost, totalClicks);
                totalImpressions += tmp[j].impressions;
                totalCTR = __.divide(totalClicks, totalImpressions) * 100;
                totalCPM = __.divide(totalCost, totalImpressions / 1000);
                totalAP = tmp[j].ap === 0 ? totalAP : tmp[j].ap;
                totalLPClicks += tmp[j].lp_clicks;
                totalTrkClicks += tmp[j].trk_clicks;
                totalLPCtr = __.divide(totalLPClicks, totalTrkClicks) * 100;
                totalConv += tmp[j].conversions;
                totalRev += tmp[j].revenue;
                totalEPC = __.divide(totalRev, totalLPClicks);
                totalCV = __.divide(totalConv, totalTrkClicks) * 100;
                totalNET = totalRev - totalCost;
                totalROI = __.divide(totalNET, totalCost) * 100;
                totalEPV = __.divide(totalRev, totalTrkClicks);
                totalCPV = __.divide(totalCost, totalTrkClicks);
                totalCPA = __.divide(totalCost, totalConv);
                totalNumBot += tmp[j].total_bot;
                totalBlockBot += tmp[j].block_bot;
                totalAllowBot += tmp[j].allow_bot;
                totalBot = __.divide(totalBlockBot, totalNumBot) * 100;
            }
            result.push({
                site_id: ID,
                site_name: NAME,
                clicks: totalClicks,
                cost: _.round(totalCost, 2),
                cpc: _.round(totalCPC, 2),
                impressions: totalImpressions,
                ctr: _.round(totalCTR, 2),
                cpm: _.round(totalCPM, 2),
                ap: totalAP,
                lp_clicks: totalLPClicks,
                trk_clicks: totalTrkClicks,
                lp_ctr: _.round(totalLPCtr, 2),
                conversions: totalConv,
                revenue: _.round(totalRev, 2),
                epc: _.round(totalEPC, 2),
                cv: _.round(totalCV, 2),
                net: _.round(totalNET, 2),
                roi: _.round(totalROI, 2),
                epv: _.round(totalEPV, 4),
                cpv: _.round(totalCPV, 4),
                cpa: _.round(totalCPA, 3),
                total_bot: totalNumBot,
                block_bot: totalBlockBot,
                allow_bot: totalAllowBot,
                bot: _.round(totalBot, 2),
            });
        }
        resolve(result);
    });
}

function siteMetadatas(siteIds, trafficSource, campaign_id) {
    return new Promise(function (resolve, reject) {
        SiteMetadata.find({
            site_id: { $in: siteIds },
            trafficSource: trafficSource,
        }, (err, results) => {
            if (err) reject(err);
            resolve(_.map(results, e => {
                return _.assign({
                    site_id: e.site_id,
                    site_url: e.site_url,
                    publisher_id: e.publisher_id,
                    publisher_name: e.publisher_name
                }, _.find(e.metadata, ['camp_id', campaign_id]));
            }));
        });
    });
}
// End get site metrics

exports.getList = getList;
exports.saveSiteStatus = saveSiteStatus;
exports.removeBlockedSites = removeBlockedSites;
exports.blockedSites = blockedSites;
exports.updateSitesConfiguredCpc = updateSitesConfiguredCpc;
exports.updateStats = updateStats;
exports.multipleUpdateStats = multipleUpdateStats;
exports.saveSiteDatas = saveSiteDatas;
exports.saveSiteMetrics = saveSiteMetrics;
exports.saveSiteMetadata = saveSiteMetadata;
exports.getSiteMetrics = getSiteMetrics;
exports.saveSiteMetadataStatus = saveSiteMetadataStatus;
exports.saveSiteMetadataCpc = saveSiteMetadataCpc;
exports.updateAutoChangeCpcStatus = updateAutoChangeCpcStatus;