'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const logger = require('../../lib/logger');
const admediaAPI = require('./api-connector/remarketing/admedia');

function admediaCampaignSummary(start_date, end_date) {
    return new Promise(function (resolve, reject) {
        admediaAPI.campaignBreakdown(start_date, end_date).then(data => {
            let response = _.map(data, e => {
                return {
                    id_AD: Number(e.id),
                    impressions_AD: Number(e.impressions),
                    clicks_AD: Number(e.clicks),
                    conversions_AD: Number(e.conversions),
                    spent_AD: Number(e.spent),
                }
            });
            resolve({ results: response });
        }, err => reject(err));
    });
}

exports.admediaCampaignSummary = admediaCampaignSummary;