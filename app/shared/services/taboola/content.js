'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const taboolaAPI = require('../api-connector/traffic-sources/taboola');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/taboola');
const campaignService = require('./campaign');
const contentService = require('../content');
const trackerService = require('../voluum');
const api_helper = require('../../../helpers/api');
const cache = require('../../../lib/redis');
const client = cache._createClient();
const expire = config.get('expireCache');
const schemaContent = require('../../helpers/schemas/content');
const variables = config.get('TABOOLA').variables;
const _VL = config.get('VOLUUM').mapKey;

function contentSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            taboolaContentSummary(accessTraffic, campaign_id, start_date, end_date),
            voluumContentSummary(accessTracker, campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const content_taboola = data[0].results;
            const content_voluum = data[1].results;
            const contentList = filterContentResults(mergeContents(content_taboola, content_voluum));

            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(contentList),
                contents: _.reverse(contentList),
            });
        }).catch(err => reject(err));
    });
}

function taboolaContentSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            const token = auth.access_token;
            const account_id = accessKeys.account_id;
            const promises = [
                taboolaContentList(token, account_id, campaign_id),
                taboolaAPI.contentBreakdown(token, account_id, campaign_id, start_date, end_date)
            ];
            Promise.all(promises).then(data => {
                const contentList = data[0].results;
                const contentBreakdown = data[1].results;
                const response = _.map(contentList, o => _.pick(
                    _.assign(o, _.find(contentBreakdown, ['item', o.id])),
                    schemaContent.taboolaContentColumns
                ));

                // if (_.size(response) > 0)
                //     cache._set(client, key, { results: response }, expire);

                resolve({ results: response });
            }).catch(err => reject(err));
        });

        // const key = api_helper.setCache(client, 'taboola_content_summary', {
        //     accessKeys: accessKeys, campaign_id: campaign_id, start_date: start_date, end_date: end_date
        // });

        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function taboolaContentList(token, account_id, campaign_id) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.contentList(token, account_id, campaign_id).then(data => {
            resolve(data);
        }, err => reject(err));
    });
}

function voluumContentSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.contentBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                var response = _.map(data.rows, o => _.mapKeys(
                    _.pick(o, schemaContent.voluumContentColumns), (val, key) => key + _VL)
                );
                resolve({ results: response });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateContent(accessKeys, campaign_id, content_id, data) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaAPI.updateContent(auth.access_token, accessKeys.account_id, campaign_id, content_id, data).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function removeContent(accessKeys, campaign_id, content_id) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaAPI.deleteContent(auth.access_token, accessKeys.account_id, campaign_id, content_id).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function filterContentResults(data) {
    const removeFields = [variables.campaign_item_id + _VL];
    if (!_.isArray(data)) {
        data = _.omit(data, removeFields);
        return _.defaults(
            api_helper.addContentColumns(api_helper.changeKeys(data, schemaContent.keyMapping)),
            schemaContent.defaultFields
        );
    } else {
        data = _.map(data, obj => {
            obj = _.omit(obj, removeFields);
            return api_helper.addContentColumns(api_helper.changeKeys(obj, schemaContent.keyMapping));
        });
        return _.map(data, o => _.defaults(o, schemaContent.defaultFields));
    }
}

function mergeContents(taboolaContents, voluumContents) {
    return _.map(taboolaContents, taboola => _.assign(taboola, _.find(voluumContents, voluum => {
        if (voluum[variables.campaign_item_id + _VL] === taboola.id)
            return true;
        return false;
    })));
}

function taboolaContentMetrics(accessTraffic, accessTracker, campaign, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        let promises = [
            contentSummary(accessTraffic, accessTracker, campaign.id, start_date, end_date),
            campaignService.campaignItemTotals(accessTraffic, accessTracker, campaign.id, start_date, end_date)
        ];
        Promise.all(promises).then(results => {
            if (results[0].totalRows === 0)
                return;

            contentService.saveContentDatas(campaign, results[0], results[1]).then(res => {
                resolve(res);
            }, err => logger.error(err));
        }).catch(err => logger.error(err));
    });
}

exports.contentSummary = contentSummary;
exports.taboolaContentSummary = taboolaContentSummary;
exports.taboolaContentList = taboolaContentList;
exports.voluumContentSummary = voluumContentSummary;
exports.updateContent = updateContent;
exports.removeContent = removeContent;
exports.filterContentResults = filterContentResults;
exports.mergeContents = mergeContents;
exports.taboolaContentMetrics = taboolaContentMetrics;