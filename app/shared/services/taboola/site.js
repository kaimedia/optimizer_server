'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const Campaign = require('../../../models/campaign');
const Site = require('../../../models/site');
const SiteMetadata = require('../../../models/site.metadata');
const taboolaAPI = require('../api-connector/traffic-sources/taboola');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/taboola');
const fraudAPI = require('../api-connector/fraud/noidfraud');
const siteService = require('../site');
const campaignService = require('./campaign');
const fraudService = require('../fraud');
const trackerService = require('../voluum');
const api_helper = require('../../../helpers/api');
const schemaSite = require('../../helpers/schemas/site');
const variables = config.get('TABOOLA').variables;
const _VL = config.get('VOLUUM').mapKey;
const cache = require('../../../lib/redis');
const client = cache._createClient();
const expire = config.get('expireCache');

function siteSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            taboolaSiteList(accessTraffic, campaign_id, start_date, end_date),
            voluumSiteList(accessTracker, campaign_id, start_date, end_date),
            fraudService.fraudTrackingList('site', campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const sites_taboola = data[0].sites;
            const sites_voluum = data[1].sites;
            const sites_fraud = data[2];
            const siteList = filterSiteResults(mergeSites(sites_taboola, sites_voluum, sites_fraud));
            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(siteList),
                sites: siteList,
            });
        }).catch(err => reject(err));
    });
}

function taboolaSiteList(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Promise.all([
            new Promise((resolve, reject) => {
                Campaign.findOne({ id: campaign_id }, (err, res) => {
                    if (err) return reject(err);
                    resolve(res);
                });
            }),
            new Promise((resolve, reject) => {
                SiteMetadata.find({}, (err, res) => {
                    if (err) return reject(err);
                    resolve(res);
                });
            }),
            campaignService.campaignDetail(accessKeys, campaign_id),
            taboolaSiteBreakdown(accessKeys, campaign_id, start_date, end_date),
            sitesInDatabase(),
        ]).then(data => {
            const campaignDatabase = data[0];
            const siteMetadata = data[1];
            const campaignDetail = data[2];
            const siteBreakdown = data[3].results;
            const siteDatabases = data[4].results;

            const cpc_boost = campaignDetail.publisher_bid_modifier !== null
                ? campaignDetail.publisher_bid_modifier.values
                : [];

            let sites = _.map(siteBreakdown, item => {
                item.camp_id = campaignDetail.id;
                item.camp_name = campaignDetail.name;
                item.cpc_modification = '';
                item.configured_cpc = campaignDetail.cpc;
                item.status = changeStatusText(item, siteDatabases);
                const row = _.pick(_.assign(item, _.find(cpc_boost, ['target', item.site])), schemaSite.taboolaSiteColumns);

                const cpc_boost_percent = row.cpc_modification !== "" ? _.round((row.cpc_modification - 1) * 100) : 0;
                row.boosted_cpc = _.round(row.configured_cpc + row.configured_cpc * (cpc_boost_percent / 100), 3);
                row.cpc_boost_percent = cpc_boost_percent;
                row.ap_VL = campaignDatabase.payout ? campaignDatabase.payout : 0;
                row.auto_update_cpc = true;

                if (siteMetadata) {
                    const site = _.find(siteMetadata, ['site_id', item.metadata.id]);
                    if (site && site.metadata) {
                        const meta = _.find(site.metadata, ['camp_id', campaign_id]);
                        if (meta && !meta.auto_update_cpc) {
                            row.auto_update_cpc = meta.auto_update_cpc;
                        }
                    }
                }

                return row;
            });

            const response = {
                campaign_id: campaign_id,
                recordCount: data[1].recordCount,
                sites: sites
            };

            resolve(response);

        }).catch(err => reject(err));
    });
}

function voluumSiteList(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.siteBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                var sites = _.map(data['rows'], o => _.mapKeys(
                    _.pick(o, schemaSite.voluumSiteColumns), (val, key) => key + _VL)
                );
                resolve({
                    campaign_id: campaign_id,
                    recordCount: data.totalRows,
                    totals: data.totals,
                    sites: sites
                });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function taboolaSiteBreakdown(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaAPI.siteBreakdown(auth.access_token, accessKeys.account_id, campaign_id, start_date, end_date).then(data => {

                // if (_.size(data) > 0)
                //     cache._set(client, key, data, expire);

                resolve(data);
            }).catch(err => reject(err));
        }, err => reject(err));

        // const key = api_helper.setCache(client, 'site_breakdown', { accessKeys: accessKeys, campaign_id: campaign_id, start_date: start_date, end_date: end_date });
        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function sitesInDatabase() {
    return new Promise(function (resolve, reject) {
        Site.find({ trafficSource: 'Taboola' }, (err, data) => {
            if (err) return reject(err);
            resolve({ results: data });
        });
    });
}

function changeStatusText(item, siteDatabases) {
    let newStatus;

    if (!_.isEmpty(siteDatabases)) {
        const siteDb = _.find(siteDatabases, ['site_id', item.site]);
        if (!_.isEmpty(siteDb)) {
            if (siteDb.status === 'PAUSED' || siteDb.status === 'BLACKLIST');
            return 'BLOCKED';
        }
    }

    switch (item.blocking_level) {
        case 'NONE':
            newStatus = 'ACTIVE';
            break;

        default:
            newStatus = 'BLOCKED';
            break;
    };

    return newStatus;
}

function filterSites(siteList, sitesStatus) {

    switch (status) {
        case 'active':
            siteList = _.filter(siteList, o => o.blocking_level === 'NONE');
            siteList = _.map(siteList, o => _.assign(o, { status: 'ACTIVE' }));

            break;

        case 'blocked':
            siteList = _.filter(siteList, o => o.blocking_level !== 'NONE');
            siteList = _.map(siteList, o => _.assign(o, { status: 'BLOCKED' }));

            break;

        case 'pause':
        case 'blacklist':
            const siteIds = _.map(sitesStatus, o => o.site_id);
            siteList = _.filter(siteList, o => _.includes(siteIds, o.site));
            break;
    }

    return siteList;
}

function filterSiteResults(data) {
    data = _.map(data, obj => {
        obj = _.omit(obj, [variables.campaign_id + _VL, variables.site + _VL, 'name_PT']);
        return _.defaults(api_helper.addSiteColumns(api_helper.changeKeys(obj, schemaSite.keyMapping)), schemaSite.defaultFields);
    });
    return _.map(data, o => _.defaults(o, schemaSite.defaultFields));
}

function mergeSites(siteTaboola, siteVoluum, sitesFraud) {
    let traffic_tracker = _.map(siteTaboola, o => _.defaults(o, _.find(siteVoluum, [variables.site + _VL, o.site])));

    if (!_.isEmpty(sitesFraud))
        traffic_tracker = _.map(traffic_tracker, o => _.defaults(o, _.find(sitesFraud, ['name_PT', o.site])));

    return traffic_tracker;
}

function changeSiteStatus(accessKeys, siteList, status, userId = null) {
    return new Promise(function (resolve, reject) {
        let promises = [];
        _.forEach(siteList, item => {
            promises.push(siteService.saveSiteStatus(item, status, accessKeys.type, userId));
            if (status === 'active' || status === 'blocked') {
                promises.push(siteService.saveSiteMetadataStatus(item, status, accessKeys.type));
            }
        });
        Promise.all(promises).then(data => {
            resolve({ success: true, message: 'Success.' });
        }).catch(err => reject(err));
    });
}

function taboolaSiteMetrics(accessTraffic, accessTracker, campaign, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        let promises = [
            siteSummary(accessTraffic, accessTracker, campaign.id, start_date, end_date),
            campaignService.campaignItemTotals(accessTraffic, accessTracker, campaign.id, start_date, end_date)
        ];
        Promise.all(promises).then(results => {
            if (results[0].totalRows === 0)
                return;
            siteService.saveSiteDatas(campaign, results[0], results[1]).then(res => {
                resolve(res);
            }, err => logger.error(err));
        }).catch(err => logger.error(err));
    });
}

exports.siteSummary = siteSummary;
exports.taboolaSiteList = taboolaSiteList;
exports.voluumSiteList = voluumSiteList;
exports.changeSiteStatus = changeSiteStatus;
exports.sitesInDatabase = sitesInDatabase;
exports.filterSiteResults = filterSiteResults;
exports.mergeSites = mergeSites;
exports.taboolaSiteMetrics = taboolaSiteMetrics;