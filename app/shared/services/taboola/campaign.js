'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const Campaign = require('../../../models/campaign');
const taboolaAPI = require('../api-connector/traffic-sources/taboola');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/taboola');
const trackerService = require('../voluum');
const siteService = require('../site');
const admediaService = require('../admedia');
const taboolaSiteService = require('../taboola/site');
const api_helper = require('../../../helpers/api');
const schema = require('../../helpers/schemas/campaign');
const variables = config.get('TABOOLA').variables;
const _VL = config.get('VOLUUM').mapKey;
const cache = require('../../../lib/redis');
const client = cache._createClient();
const expire = config.get('expireCache');

function finalCampaignSummary(accessTraffic, accessTracker, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            taboolaCampaignSummary(accessTraffic, start_date, end_date),
            voluumCampaignSummary(accessTracker, start_date, end_date),
            admediaService.admediaCampaignSummary(start_date, end_date),
        ];
        Promise.all(promises).then(data => {
            const campaigns_taboola = data[0].results;
            const campaigns_voluum = data[1].results;
            const campaigns_admedia = data[2].results;
            const campaignsFiltered = filterCampaignResults(mergeCampaigns(campaigns_taboola, campaigns_voluum, campaigns_admedia));
            const campaignSummary = calcCampaignTotals(campaignsFiltered);
            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(campaignsFiltered),
                campaigns: _.reverse(campaignsFiltered),
                summary: campaignSummary,
            });
        }).catch(err => reject(err));
    });
}

function campaignTotals(accessTraffic, accessTracker, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            taboolaCampaignSummary(accessTraffic, start_date, end_date),
            voluumCampaignSummary(accessTracker, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const campaigns_taboola = data[0].results;
            const campaigns_voluum = data[1].results;
            const campaignsFiltered = filterCampaignResults(
                mergeCampaigns(campaigns_taboola, campaigns_voluum)
            );
            const campaignTotalsData = calcCampaignTotals(campaignsFiltered);
            resolve(campaignTotalsData);
        }).catch(err => reject(err));
    });
}

function campaignItemTotals(accessTraffic, accessTracker, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            taboolaCampaignItemSummary(accessTraffic, campaign_id, start_date, end_date),
            voluumCampaignItemSummary(accessTracker, campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(results => {
            const taboolaTotals = results[0];
            const voluumTotals = results[1].totals;
            const totals = calcCampaignItemTotals(taboolaTotals, voluumTotals);
            resolve(totals);
        }).catch(err => reject(err));
    });
}

function taboolaCampaignSummary(accessKeys, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            const token = auth.access_token;
            const account_id = accessKeys.account_id;
            const promises = [
                taboolaCampaignList(token, account_id),
                taboolaAPI.campaignBreakdown(token, account_id, start_date, end_date)
            ];
            Promise.all(promises).then(data => {
                const campaignList = data[0].results;
                const campaignBreakdown = data[1].results;
                const response = _.map(campaignList, o => _.pick(
                    _.assign(o, _.omit(_.find(campaignBreakdown, ['campaign', o.id]), ['cpc'])),
                    schema.taboolaCampaignColumns
                ));
                resolve({ results: response });
            }).catch(err => reject(err));
        });

        // const key = api_helper.setCache(client, 'taboola_campaign_summary', {
        //     accessKeys: accessKeys, start_date: start_date, end_date: end_date
        // });
        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function taboolaCampaignList(token, account_id, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = ['id', 'name', 'cpc', 'spending_limit', 'daily_cap', 'daily_ad_delivery_model', 'status'];

        taboolaAPI.campaignList(token, account_id).then(data => {
            let results = _.map(data.results, o => _.pick(o, columns));
            resolve({ results: results });
        }, err => reject(err));
    });
}

function taboolaCampaignDetail(accessKeys, campaign_id, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = schema.taboolaDetailColumns;

        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaAPI.specificCampaign(auth.access_token, accessKeys.account_id, campaign_id).then(data => {
                resolve(_.pick(data, columns));
            }, err => reject(err));
        }, err => reject(err));
    });
}

function taboolaCampaignDetailWithToken(token, account_id, campaign_id, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = schema.taboolaDetailColumns;

        taboolaAPI.specificCampaign(token, account_id, campaign_id).then(data => {
            resolve(_.pick(data, columns));
        }, err => reject(err));
    });
}

function taboolaCampaignItemSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const key = api_helper.setCache(client, 'taboola_campaign_item_summary', {
            accessKeys: accessKeys, campaign_id: campaign_id, start_date: start_date, end_date: end_date
        });

        client.get(key, (err, data) => {
            if (err) throw err;
            if (data) {
                resolve(JSON.parse(data));
            } else {
                taboolaAPI.auth(accessKeys).then(auth => {
                    const token = auth.access_token;
                    const account_id = accessKeys.account_id;
                    const promises = [
                        taboolaCampaignDetailWithToken(token, account_id, campaign_id),
                        taboolaAPI.campaignBreakdown(token, account_id, start_date, end_date)
                    ];
                    Promise.all(promises).then(data => {
                        const campaignDetail = data[0];
                        const campaignBreakdown = data[1].results;
                        const response = _.pick(
                            _.assign(campaignDetail, _.find(campaignBreakdown, ['campaign', campaignDetail.id])),
                            schema.taboolaCampaignColumns
                        );

                        if (_.size(response) > 0)
                            cache._set(client, key, response, expire);

                        resolve(response);
                    });
                }, err => reject(err));
            }
        });
    });
}

function allTaboolaCampaigns(accessKeys, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = schema.taboolaDetailColumns;

        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaCampaignList(auth.access_token, accessKeys.account_id, columns).then(results => {
                resolve(results);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateCampaignTaboola(accessKeys, campaign_id, data) {
    return new Promise(function (resolve, reject) {
        taboolaAPI.auth(accessKeys).then(auth => {
            taboolaAPI.updateCampaign(auth.access_token, accessKeys.account_id, campaign_id, data).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function voluumCampaignSummary(accessKeys, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.campaignBreakdown(auth.token, start_date, end_date).then(data => {
                var response = _.map(data.rows, o => _.mapKeys(
                    _.pick(o, schema.voluumCampaignTaboolaColumns), (val, key) => key + _VL)
                );
                resolve({ results: response });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function voluumCampaignDetailSummary(accessKeys, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.campaignDetailBreakdown(auth.token, start_date, end_date).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function voluumCampaignItemSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.campaignItemBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function detailAllCampaigns(accessTraffic, accessTracker, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            allTaboolaCampaigns(accessTraffic),
            voluumCampaignDetailSummary(accessTracker, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const taboolaCampaigns = data[0].results;
            const voluumCampaigns = data[1].rows;
            let response = _.map(taboolaCampaigns, o => _.assign(o, _.find(voluumCampaigns, [`${variables.campaign_id}`, o.id])));
            response = api_helper.takeObjectHasField(response, ['campaignUrl', 'offerUrl']);
            resolve(response);
        }).catch(err => reject(err));
    });
}

function campaignDetail(accessKeys, campaign_id) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, campaign) => {
            if (err) return reject(err);
            if (!_.isEmpty(campaign)) {
                resolve(campaign);
            } else {
                taboolaCampaignDetail(accessKeys, campaign_id).then(result => {
                    resolve(result);
                }, err => reject(err));
            }
        });
    });
}

function calcCampaignTotals(campaignData) {
    const sumResults = api_helper.calculatorSumFields(campaignData, ['cost', 'revenue']);
    const net = api_helper.round(_.subtract(sumResults['revenue'], sumResults['cost']), 2);
    const roi = api_helper.round(api_helper.divide(net, sumResults['cost']) * 100, 2);
    return _.assign(sumResults, { net, roi });
}

function calcCampaignItemTotals(taboolaCampaign, voluumCampaign) {
    voluumCampaign = _.mapKeys(_.pick(voluumCampaign, schema.voluumCampaignTaboolaColumns), (val, key) => key + _VL);
    const totals = filterCampaignResults(_.assign(taboolaCampaign, voluumCampaign));
    return totals;
}

function filterCampaignResults(data) {
    if (!_.isArray(data)) {
        data = _.omit(data, [variables.campaign_id + _VL, 'remarketing_id', 'id_AD']);
        return _.defaults(
            api_helper.addCampaignColumns(api_helper.changeKeys(data, schema.keyMapping)),
            schema.defaultFields
        );
    } else {
        data = _.map(data, obj => {
            obj = _.omit(obj, [variables.campaign_id + _VL]);
            return api_helper.addCampaignColumns(api_helper.changeKeys(obj, schema.keyMapping));
        });
        return _.map(data, o => _.defaults(o, schema.defaultFields));
    }
}

function mergeCampaigns(taboolaCampaigns, voluumCampaigns, admediaCampaigns) {
    let combinedCampaigns = _.map(taboolaCampaigns, e => {
        e = _.assign(e, _.find(voluumCampaigns, [variables.campaign_id + _VL, e.id]));
        e = _.assign(e, _.find(admediaCampaigns, ['id_AD', e.remarketing_id]));
        return e;
    });
    return combinedCampaigns;
}

function updateCampaignDatabase(where, data) {
    return new Promise(function (resolve, reject) {
        Campaign.findOneAndUpdate(where, data, { new: true }, (err, response) => {
            if (err) return reject(err);
            resolve(response);
        });
    });
}

function blockSitesCampaign(accessKeys, campaign_id, sites, userId = null) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, campaign) => {
            if (err) return reject(err);

            if (!_.isEmpty(campaign)) {
                blockSitesService(accessKeys, campaign, sites, userId).then(response => {
                    resolve(response);
                }, err => reject(err));
            } else {
                taboolaCampaignDetail(accessKeys, campaign_id, ['id', 'name', 'publisher_targeting']).then(result => {
                    blockSitesService(accessKeys, result, sites, userId).then(response => {
                        resolve(response);
                    }, err => reject(err));
                }, err => reject(err));
            }
        });
    });
}

function blockSitesService(accessKeys, campaign, sites, userId = null) {
    return new Promise(function (resolve, reject) {
        const siteIds = _.map(sites, o => o.site_id);
        const blockedSites = campaign.publisher_targeting !== null ? campaign.publisher_targeting.value : [];
        const sitesBlocking = _.union(blockedSites, siteIds);
        const payload = {
            publisher_targeting: {
                type: "EXCLUDE",
                value: sitesBlocking
            }
        };
        updateCampaignTaboola(accessKeys, campaign.id, payload).then(result => {
            Campaign.findOneAndUpdate({ id: campaign.id }, payload, (err, res) => {
                if (err) return reject(err);
                taboolaSiteService.changeSiteStatus(accessKeys, sites, 'blocked', userId).then(res => {
                    resolve({ success: true, message: 'Success.' });
                }, err => reject(err));
            });
        }, err => reject(err));
    });
}

function unBlockSitesCampaign(accessKeys, campaign_id, sites) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, campaign) => {
            if (err) return reject(err);

            if (!_.isEmpty(campaign)) {
                unBlockSitesService(accessKeys, campaign, sites).then(response => {
                    resolve(response);
                }, err => reject(err));
            } else {
                taboolaCampaignDetail(accessKeys, campaign_id, ['id', 'name', 'publisher_targeting']).then(result => {
                    unBlockSitesService(accessKeys, result, sites).then(response => {
                        resolve(response);
                    }, err => reject(err));
                }, err => reject(err));
            }
        });
    });
}

function unBlockSitesService(accessKeys, campaign, sites) {
    return new Promise(function (resolve, reject) {
        const siteIds = _.map(sites, o => o.site_id);

        let payload = {
            publisher_targeting: null
        };

        const blockedSites = campaign.publisher_targeting !== null ? campaign.publisher_targeting.value : [];

        _.remove(blockedSites, site_id => {
            return (siteIds.indexOf(site_id) != -1);
        });

        if (!_.isEmpty(blockedSites)) {
            payload.publisher_targeting = {
                type: "EXCLUDE",
                value: blockedSites
            }
        }

        updateCampaignTaboola(accessKeys, campaign.id, payload).then(result => {
            Campaign.findOneAndUpdate({ id: campaign.id }, payload, (err, response) => {
                if (err) return reject(err);
                siteService.removeBlockedSites(sites, accessKeys.type, campaign.id).then(result => {
                    resolve({ success: true, message: 'Success.' });
                }, err => reject(err));
            });
        }, err => reject(err));

    });
}

function changeSiteCpc(accessKeys, site) {
    return new Promise(function (resolve, reject) {

        if (site.cpc_boost_percent > 50)
            return reject({ success: false, message: 'Please enter a value no larger than 50.' });

        if (site.cpc_boost_percent < -50)
            return reject({ success: false, message: 'Please enter a value of at least -50.' });

        taboolaCampaignDetail(accessKeys, site.camp_id, ['publisher_bid_modifier']).then(campaign => {
            let modified_sites = campaign.publisher_bid_modifier !== null ? campaign.publisher_bid_modifier.values : [];
            site.cpc_boost = _.round(1 + site.cpc_boost, 2);

            if (site.cpc_boost_percent === 0) {
                if (isSiteExists(site, modified_sites)) {
                    _.remove(modified_sites, e => site.site_id.indexOf(e.target) != -1);
                } else {
                    return resolve({ success: false, message: 'Success.' });
                }
            } else {
                if (!isSiteExists(site, modified_sites)) {
                    modified_sites.push({
                        target: site.site_id,
                        cpc_modification: site.cpc_boost
                    });
                } else {
                    modified_sites = _.unionBy([{
                        target: site.site_id,
                        cpc_modification: site.cpc_boost
                    }], modified_sites, 'target');
                }
            }

            const payload = {
                publisher_bid_modifier: {
                    values: modified_sites
                }
            };

            updateCampaignTaboola(accessKeys, site.camp_id, payload).then(response => {
                Campaign.findOneAndUpdate({ id: site.camp_id }, payload, (err, result) => {
                    if (err) return reject(err);
                    siteService.saveSiteMetadataCpc(site, accessKeys.type).then(res => {
                        resolve({ success: true, message: 'Success.' });
                    }, err => reject(err));
                });
            }, err => reject(err));

        }, err => reject(err));
    });
}

function isSiteExists(site, siteList) {
    const sites = _.filter(siteList, o => o.target === site.site_id);
    if (_.size(sites) > 0)
        return true;
    return false;
}

exports.finalCampaignSummary = finalCampaignSummary;
exports.campaignTotals = campaignTotals;
exports.campaignItemTotals = campaignItemTotals;
exports.allTaboolaCampaigns = allTaboolaCampaigns;
exports.detailAllCampaigns = detailAllCampaigns;
exports.taboolaCampaignSummary = taboolaCampaignSummary;
exports.taboolaCampaignList = taboolaCampaignList;
exports.taboolaCampaignDetail = taboolaCampaignDetail;
exports.taboolaCampaignDetailWithToken = taboolaCampaignDetailWithToken;
exports.taboolaCampaignItemSummary = taboolaCampaignItemSummary;
exports.updateCampaignTaboola = updateCampaignTaboola;
exports.voluumCampaignSummary = voluumCampaignSummary;
exports.voluumCampaignDetailSummary = voluumCampaignDetailSummary;
exports.voluumCampaignItemSummary = voluumCampaignItemSummary;
exports.campaignDetail = campaignDetail;
exports.filterCampaignResults = filterCampaignResults;
exports.calcCampaignTotals = calcCampaignTotals;
exports.calcCampaignItemTotals = calcCampaignItemTotals;
exports.mergeCampaigns = mergeCampaigns;
exports.updateCampaignDatabase = updateCampaignDatabase;
exports.blockSitesCampaign = blockSitesCampaign;
exports.unBlockSitesCampaign = unBlockSitesCampaign;
exports.changeSiteCpc = changeSiteCpc;