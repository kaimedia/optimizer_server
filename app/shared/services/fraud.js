'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const logger = require('../../lib/logger');
const Campaign = require('../../models/campaign');
const fraudAPI = require('./api-connector/fraud/noidfraud');
const trackerService = require('./voluum');
const schemaFraud = require('../helpers/schemas/fraud');

function campaignFraudInit(trafficType, accessKeys, tracker) {
    return new Promise(function (resolve, reject) {
        const offerId = tracker.redirectTarget.inlineFlow.defaultPaths[0].offers[0].offer.id;
        const promises = [
            trackerService.trafficSourcesVoluum(accessKeys),
            trackerService.getOfferVoluum(accessKeys, offerId)
        ];
        Promise.all(promises).then(data => {
            const variablesVoluum = _.filter(data[0].trafficSources, traffic => traffic.name === trafficType);
            const offer = data[1];

            if (_.isEmpty(variablesVoluum))
                return reject(`No traffic source ${trafficType}.`);

            const customVariables = variablesVoluum[0].customVariables;
            if (_.isEmpty(customVariables))
                return reject("No custom variables.");

            const campaignUrl = _.split(tracker.url, "?", 2)[0];

            let dynamicVariables = _.map(customVariables, variable => {
                return {
                    name: variable.parameter,
                    value: variable.placeholder,
                    track: variable.trackedInReports
                };
            });
            dynamicVariables = _.filter(dynamicVariables, o => !_.isEmpty(o));

            let dynvarStr = "";
            _.forEach(dynamicVariables, obj => {
                if (dynvarStr != "") {
                    dynvarStr += "&";
                }
                dynvarStr += `${obj.name}=${obj.value}`;
            });

            const fakeUrl = offer.url;

            const campaignFields = {
                info: tracker.name,
                fakeurl: fakeUrl,
                realurl: [
                    {
                        url: campaignUrl,
                        perc: 100,
                        desc: "LP1"
                    }
                ],
                dynvar: dynamicVariables,
                active: 1,
                traffic: "59e9a040bb2bca37758b4569",
                rules: {},
                urlfilter: [],
                filters: [],
                lptrack: true,
                dynautopt: true,
                pagelock: {
                    enabled: false,
                    action: "blank",
                    url: "",
                    timeout: 10
                }
            };

            resolve(campaignFields);

        }).catch(err => reject(err));

    });
}

function listCampaignFraud() {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.listCampaign(auth.token).then(result => {
                resolve(_.filter(result, o => o.archived === 0));
            }, err => reject(err));
        }, err => reject(err));
    });
}

function getCampaignFraud(campaign_id) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.getCampaign(auth.token, campaign_id).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function createCampaignFraud(payload) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.createCampaign(auth.token, payload).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateCampaignFraud(payload) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.updateCampaign(auth.token, payload).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function archiveCampaignFraud(campaign_id) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.archiveCampaign(auth.token, campaign_id).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function statsCampaignsFraud(start_date, end_date) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.statsCampaigns(auth.token, start_date, end_date).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function statsCampaignFraud(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.statsCampaign(auth.token, campaign_id, start_date, end_date).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function trakingCampaignFraud(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        fraudAPI.auth().then(auth => {
            fraudAPI.trackingCampaign(auth.token, campaign_id, start_date, end_date).then(result => {
                resolve(result);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function fraudTrackingList(variable, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findOne({ id: campaign_id }, (err, camp) => {
            if (err) reject(err);
            if (!camp.id_fraud || _.isUndefined(camp.id_fraud))
                resolve([]);

            fraudAPI.auth().then(auth => {
                fraudAPI.trackingCampaign(auth.token, camp.id_fraud, start_date, end_date).then(result => {
                    let data = [];
                    if (!_.isEmpty(result) && !_.isEmpty(result.data)) {
                        data = _.filter(result.data, ['name', variable]);
                        data = _.map(data[0].vals, o => _.mapKeys(
                            _.pick(o, schemaFraud.fraudStatsColumns), (val, key) => key + '_PT')
                        );
                    }
                    resolve(data);
                }, err => reject(err));
            }, err => reject(err));
        });
    });
}

exports.campaignFraudInit = campaignFraudInit;
exports.listCampaignFraud = listCampaignFraud;
exports.getCampaignFraud = getCampaignFraud;
exports.createCampaignFraud = createCampaignFraud;
exports.updateCampaignFraud = updateCampaignFraud;
exports.archiveCampaignFraud = archiveCampaignFraud;
exports.statsCampaignsFraud = statsCampaignsFraud;
exports.statsCampaignFraud = statsCampaignFraud;
exports.trakingCampaignFraud = trakingCampaignFraud;
exports.fraudTrackingList = fraudTrackingList;