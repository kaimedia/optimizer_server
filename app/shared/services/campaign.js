'use strict';

const _ = require('lodash');
const __ = require('../../helpers/api');
const config = require('config');
const logger = require('../../lib/logger');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const helperService = require('../helpers/service');
const Traffic = require('../../models/traffic');
const Tracker = require('../../models/tracker');
const Campaign = require('../../models/campaign');
const CampaignMetrics = require('../../models/campaign.metrics');
const taboolaCampaignService = require('./taboola/campaign');
const outbrainCampaignService = require('./outbrain/campaign');

// Begin raw campaign metrics data
function updateStats(campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.findById(campaign_id, (err, campaign) => {
            if (err) return reject(err);

            if (!campaign || !campaign.id || !campaign.type || !campaign.trafficSourceAccount || !campaign.trackerAccount)
                return reject('Invalid campaign');

            multipleUpdateStats(campaign, start_date, end_date).then(res => {
                resolve(res);
            }).catch(err => reject(err));
        });
    });
}

function multipleUpdateStats(campaign, start_date, end_date, timeout = 0) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            const promises = [
                new Promise((resolve, reject) => {
                    Traffic.findById(campaign.trafficSourceAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                }),
                new Promise((resolve, reject) => {
                    Tracker.findById(campaign.trackerAccount, (err, res) => {
                        if (err || !res) return;
                        if (res) resolve(res);
                    });
                })
            ];
            Promise.all(promises).then(accessAccounts => {
                let timeout = 0, daysOfYear = [];

                for (let day = moment(moment(start_date)); day <= moment(end_date); day.add(1, 'days')) {
                    const thisDate = day.format(config.get('dateFormat'));
                    daysOfYear.push(updateStatsByDates(accessAccounts, campaign, thisDate, thisDate, timeout));
                    timeout = timeout + 2000;
                }

                Promise.all(daysOfYear).then(res => {
                    resolve(res);
                }).catch(err => logger.error(err));
            }).catch(err => logger.error(err));
        }, timeout);
    });
}

function updateStatsByDates(accounts, campaign, start_date, end_date, timeout) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            let accessTraffic;
            let accessTracker = _.pick(accounts[1], ['access_id', 'access_key']);
            switch (campaign.type) {
                case 'Taboola':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'client_id', 'client_secret']);
                    taboolaCampaignService.finalCampaignSummary(accessTraffic, accessTracker, start_date, end_date).then(results => {
                        results.campaign = _.find(results.campaigns, ['id', campaign.id]);
                        if (!results.campaign || _.size(results.campaign) === 0)
                            return;
                        saveCampaignDatas(campaign, results, results.summary).then(res => {
                            resolve(res);
                        }, err => logger.error(err));
                    }, err => logger.error(err));
                    break;
                case 'Outbrain':
                    accessTraffic = _.pick(accounts[0], ['type', 'account_id', 'email', 'password']);
                    outbrainCampaignService.finalCampaignSummary(accessTraffic, accessTracker, start_date, end_date, 0).then(results => {
                        results.campaign = _.find(results.campaigns, ['id', campaign.id]);
                        if (!results.campaign || _.size(results.campaign) === 0)
                            return;
                        saveCampaignDatas(campaign, results, results.summary).then(res => {
                            resolve(res);
                        }, err => logger.error(err));
                    }, err => logger.error(err));
            }
        }, timeout);
    });
}

function saveCampaignDatas(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        let promises = [
            saveCampaignMetrics(campaign, result, totals),
            saveCampaignMetadata(campaign, result)
        ];
        Promise.all(promises).then(res => {
            resolve(res);
        }).catch(err => logger.error(err));
    });
}

function saveCampaignMetrics(campaign, result, totals) {
    return new Promise(function (resolve, reject) {
        CampaignMetrics.findOne({
            start_date: result.start_date,
            end_date: result.end_date,
            campaign_id: campaign._id,
            trafficSource: campaign.type,
        }, (err, res) => {
            if (err) throw err;
            const campaignStats = _.omit(result.campaign, ['id', 'name', 'cpc', 'spending_limit', 'daily_cap', 'daily_ad_delivery_model', 'budget', 'amountSpent', 'remarketing_id', 'status']);
            if (!res) {
                //add
                const metrics = {
                    last_rawdata_update_time: result.timestamp,
                    last_rawdata_update_timestamp: moment().unix(),
                    start_date: result.start_date,
                    end_date: result.end_date,
                    campaign_id: campaign._id,
                    campaign_name: campaign.name,
                    trafficSource: campaign.type,
                    metrics: campaignStats,
                };
                CampaignMetrics(metrics).save((err) => {
                    if (err) throw err;
                    resolve('Campaign Metrics created!');
                });
            } else {
                //update
                res.updated_at = Date.now();
                res.last_rawdata_update_time = result.timestamp;
                res.last_rawdata_update_timestamp = moment().unix();
                res.metrics = campaignStats;
                res.save((err) => {
                    if (err) throw err;
                    resolve('Campaign Metrics updated!');
                });
            }
        });
    });
}

function saveCampaignMetadata(campaign, result) {
    return new Promise(function (resolve, reject) {
        Campaign.findByIdAndUpdate(campaign._id, {
            cpc: result.campaign.cpc,
            spending_limit: result.campaign.spending_limit,
            daily_cap: result.campaign.daily_cap ? result.campaign.daily_cap : 0,
            daily_ad_delivery_model: result.campaign.daily_ad_delivery_model,
            budget: result.campaign.budget ? result.campaign.budget : {},
            amountSpent: result.campaign.amountSpent ? result.campaign.amountSpent : 0,
            start_date: result.campaign.budget ? result.campaign.budget.startDate : "",
            status: result.campaign.status,
            updated_at: Date.now(),
        }, (err, res) => {
            if (err) throw err;
            resolve('Campaign Metadata updated!');
        });
    });
}
// End raw campaign metrics data

// Begin get campaigns metrics
function getCampaignsMetrics(trafficSource, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        Campaign.find({ type: trafficSource, archived: 0 }, (err, campaigns) => {
            if (err) reject(err);
            let promises = [];
            if (campaigns) {
                _.forEach(campaigns, campaign => {
                    promises.push(campaignMetrics(campaign, start_date, end_date));
                });
            }
            Promise.all(promises).then(campaigns => {
                let data = {};
                helperService.convertSummary(campaigns).then(summary => {
                    data.start_date = start_date;
                    data.end_date = end_date;
                    data.summary = summary[0];
                    data.campaigns = campaigns;
                    data.totalRows = _.size(campaigns);
                    resolve(data);
                }, err => reject(err));
            }).catch(err => reject(err));
        });
    });
}

function campaignMetrics(campaign, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        CampaignMetrics.find({
            trafficSource: campaign.type,
            campaign_id: campaign._id,
            start_date: { $gte: start_date },
            end_date: { $lte: end_date },
        }, (err, response) => {
            if (err) reject(err);
            helperService.convertSummary(response, 'metrics').then(results => {
                results[0]._id = campaign._id;
                results[0].id = campaign.id ? campaign.id : null;
                results[0].name = campaign.name;
                results[0].cpc = campaign.cpc;
                results[0].spending_limit = campaign.spending_limit;
                results[0].daily_ad_delivery_model = campaign.daily_ad_delivery_model;
                results[0].budget = campaign.budget ? campaign.budget : {};
                results[0].amountSpent = campaign.amountSpent ? campaign.amountSpent : 0;
                results[0].status = campaign.status;
                resolve(results[0]);
            }, err => reject(err));
        });
    });
}
// End get campaigns metrics

exports.updateStats = updateStats;
exports.multipleUpdateStats = multipleUpdateStats;
exports.getCampaignsMetrics = getCampaignsMetrics;