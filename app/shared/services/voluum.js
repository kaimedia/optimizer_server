'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const logger = require('../../lib/logger');
const voluumAPI = require('./api-connector/trackers/voluum');

function trafficSourcesVoluum(accessKeys) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.trafficSources(auth.token).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function affiliateNetworksVoluum(accessKeys) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.affiliateNetworks(auth.token).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function landersVoluum(accessKeys) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.getLanders(auth.token).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function createLanderVoluum(accessKeys, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.createLander(auth.token, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function getLanderVoluum(accessKeys, landerId) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.getLander(auth.token, landerId).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateLanderVoluum(accessKeys, landerId, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.updateLander(auth.token, landerId, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function offersVoluum(accessKeys) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.getOffers(auth.token).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function getOfferVoluum(accessKeys, offerId) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.getOffer(auth.token, offerId).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function createOfferVoluum(accessKeys, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.createOffer(auth.token, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateOfferVoluum(accessKeys, offerId, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.updateOffer(auth.token, offerId, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function trackerSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            const promises = [
                voluumAPI.specificCampaign(auth.token, campaign_id),
                voluumAPI.landersBreakdown(auth.token, campaign_id, start_date, end_date),
                voluumAPI.offersBreakdown(auth.token, campaign_id, start_date, end_date),
                voluumAPI.getLanders(auth.token),
                voluumAPI.getOffers(auth.token)
            ];
            Promise.all(promises).then(data => {
                const campaignDetail = data[0];
                const landerStats = data[1].rows;
                const offerStats = data[2].rows;
                const landerList = data[3].landers;
                const offerList = data[4].offers;

                let defaultPaths = campaignDetail.redirectTarget.inlineFlow.defaultPaths;
                defaultPaths = _.map(defaultPaths, path => {
                    _.map(path.landers, lander => _.assign(_.assign(lander.lander, _.find(landerStats, ['landerId', lander.lander.id])), _.find(landerList, ['id', lander.lander.id])));
                    _.map(path.offers, offer => _.assign(_.assign(offer.offer, _.find(offerStats, ['offerId', offer.offer.id])), _.find(offerList, ['id', offer.offer.id])));
                    return path;
                });

                resolve(defaultPaths);
            }).catch(err => reject(err));
        }, err => reject(err));
    });
}

function voluumCampaignDetail(accessKeys, campaign_id) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.specificCampaign(auth.token, campaign_id).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateCampaignVoluum(accessKeys, campaign_id, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.updateCampaign(auth.token, campaign_id, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function createCampaignVoluum(accessKeys, payload) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            voluumAPI.createCampaign(auth.token, payload).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

exports.trafficSourcesVoluum = trafficSourcesVoluum;
exports.affiliateNetworksVoluum = affiliateNetworksVoluum;
exports.landersVoluum = landersVoluum;
exports.getLanderVoluum = getLanderVoluum;
exports.createLanderVoluum = createLanderVoluum;
exports.updateLanderVoluum = updateLanderVoluum;
exports.offersVoluum = offersVoluum;
exports.getOfferVoluum = getOfferVoluum;
exports.createOfferVoluum = createOfferVoluum;
exports.updateOfferVoluum = updateOfferVoluum;
exports.trackerSummary = trackerSummary;
exports.voluumCampaignDetail = voluumCampaignDetail;
exports.createCampaignVoluum = createCampaignVoluum;
exports.updateCampaignVoluum = updateCampaignVoluum;
