'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('config');
const variables = config.get('OUTBRAIN').variables;
const voluum = require('./voluum');

/**
 * Get campaign summary
 * 
 * @param {*} token 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignBreakdown(token, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get campaign summary by id
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignItemBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=1&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get sections sunmmary
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function sectionBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id},${variables.section_id}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get contents summary
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function contentBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id},${variables.ad_id}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

exports.campaignBreakdown = campaignBreakdown;
exports.campaignItemBreakdown = campaignItemBreakdown;
exports.sectionBreakdown = sectionBreakdown;
exports.contentBreakdown = contentBreakdown;