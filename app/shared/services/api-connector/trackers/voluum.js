'use strict';

const _ = require('lodash');
const request = require('request');
const config = require('config').get('VOLUUM');
const Promise = require('bluebird');
const hash = require('object-hash');
const logger = require('../../../../lib/logger');
const cache = require('../../../../lib/redis');
const client = cache._createClient();
const dateFormat = require('config').get('dateFormat');
const moment = require('moment');

function headers(token) {
    let headers = { 'Content-Type': 'application/json' };

    if (token)
        headers[config.params.auth_header_name] = token;

    return headers;
}

function post(path, data, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.params.host}${path}`;
        logger.info("[POST] %s", url);

        request({ url: url, method: 'POST', headers: headers, body: JSON.stringify(data) }
            , (error, response, body) => {
                if (error) return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function put(path, data, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.params.host}${path}`;
        logger.info("[PUT] %s", url);

        request({ url: url, method: 'PUT', headers: headers, body: JSON.stringify(data) }
            , (error, response, body) => {
                if (error) return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function get(path, params, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.params.host}${path}`;
        logger.info("[GET] %s", url);

        request({ url: url, method: 'GET', headers: headers, qs: params }
            , (error, response, body) => {
                if (error) return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function auth(accessKeys) {
    return new Promise(function (resolve, reject) {
        const token_key = hash(accessKeys);
        client.get(token_key, (err, data) => {
            if (err) return reject(err);

            if (data) {
                try {
                    resolve({ token: JSON.parse(data) });
                } catch (e) {
                    resolve({ token: data });
                }

            } else {
                post(config.params.auth_session_path, {
                    accessId: accessKeys.access_id,
                    accessKey: accessKeys.access_key,
                }, headers()).then(dataToken => {
                    if (dataToken.error || dataToken.status === "BAD_REQUEST")
                        return reject(dataToken);

                    cache._set(client, token_key, dataToken.token, 10800);
                    resolve(dataToken);
                }, err => reject(err));
            }
        });
    });
}

/**
 * Get campaign list
 * 
 * @param {*} token 
 */
function campaignList(token) {
    return get('/campaign', {}, headers(token));
}

/**
 * Get a specific campaign
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 */
function specificCampaign(token, campaign_id) {
    return get(`/campaign/${campaign_id}`, {}, headers(token));
}

/**
 * Update campaign detail
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} payload 
 */
function updateCampaign(token, campaign_id, payload) {
    return put(`/campaign/${campaign_id}`, payload, headers(token));
}

/**
 * Create a campaign
 * 
 * @param {*} token 
 * @param {*} payload 
 */
function createCampaign(token, payload) {
    return post('/campaign', payload, headers(token));
}

/**
 * Get traffic sources
 * 
 * @param {*} token 
 */
function trafficSources(token) {
    return get('/traffic-source', {}, headers(token));
}

/**
 * Get affiliate networks
 * 
 * @param {*} token 
 */
function affiliateNetworks(token) {
    return get('/affiliate-network', {}, headers(token));
}

/**
 * Get landers
 * 
 * @param {*} token 
 */
function getLanders(token) {
    return get('/lander', {}, headers(token));
}

/**
 * Create lander
 * 
 * @param {*} token 
 * @param {*} payload 
 */
function createLander(token, payload) {
    return post('/lander', payload, headers(token));
}

/**
 * Get lander by id
 * 
 * @param {*} token 
 * @param {*} lander_id 
 */
function getLander(token, lander_id) {
    return get(`/lander/${lander_id}`, {}, headers(token));
}

/**
 * Update lander by id
 * 
 * @param {*} token 
 * @param {*} lander_id 
 * @param {*} payload 
 */
function updateLander(token, lander_id, payload) {
    return put(`/lander/${lander_id}`, payload, headers(token));
}

/**
 * Get offers
 * 
 * @param {*} token 
 */
function getOffers(token) {
    return get('/offer', {}, headers(token));
}

/**
 * Get offer by id
 * 
 * @param {*} token 
 * @param {*} offer_id 
 */
function getOffer(token, offer_id) {
    return get(`/offer/${offer_id}`, {}, headers(token));
}

/**
 * Create offer
 * 
 * @param {*} token 
 * @param {*} payload 
 */
function createOffer(token, payload) {
    return post('/offer', payload, headers(token));
}

/**
 * Update offer by id
 * 
 * @param {*} token 
 * @param {*} offer_id 
 * @param {*} payload 
 */
function updateOffer(token, offer_id, payload) {
    return put(`/offer/${offer_id}`, payload, headers(token));
}

/**
 * Re target date range
 * 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function synsDate(start_date, end_date) {
    end_date = moment(end_date).add(1, 'days').format(dateFormat);

    return end_date;
}


/**
 * Get landers breakdown by campaign id
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function landersBreakdown(token, campaign_id, start_date, end_date) {
    end_date = synsDate(start_date, end_date);
    return get(`/report?groupBy=campaign,lander&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, headers(token));
}

/**
 * Get offers breakdown by campaign id
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function offersBreakdown(token, campaign_id, start_date, end_date) {
    end_date = synsDate(start_date, end_date);
    return get(`/report?groupBy=campaign,offer&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, headers(token));
}

exports.headers = headers;
exports.post = post;
exports.get = get;
exports.put = put;
exports.auth = auth;
exports.campaignList = campaignList;
exports.specificCampaign = specificCampaign;
exports.createCampaign = createCampaign;
exports.updateCampaign = updateCampaign;
exports.trafficSources = trafficSources;
exports.affiliateNetworks = affiliateNetworks;
exports.getLanders = getLanders;
exports.getLander = getLander;
exports.createLander = createLander;
exports.updateLander = updateLander;
exports.getOffers = getOffers;
exports.getOffer = getOffer;
exports.createOffer = createOffer;
exports.updateOffer = updateOffer;
exports.synsDate = synsDate;
exports.landersBreakdown = landersBreakdown;
exports.offersBreakdown = offersBreakdown;
