'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('config');
const variables = config.get('TABOOLA').variables;
const voluum = require('./voluum');

/**
 * Get campaign summary
 * 
 * @param {*} token 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignBreakdown(token, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get campaign summary by id
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignItemBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=1&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get site's summary
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function siteBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id},${variables.site}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get campaign detail + summary
 * 
 * @param {*} token 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignDetailBreakdown(token, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id},campaign,offer&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

/**
 * Get content summary by campaign id
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function contentBreakdown(token, campaign_id, start_date, end_date) {
    end_date = voluum.synsDate(start_date, end_date);
    return voluum.get(`/report?groupBy=${variables.campaign_id},${variables.campaign_item_id}&filter=${campaign_id}&from=${start_date}&to=${end_date}&tz=America/New_York&limit=99999&include=ACTIVE&offset=0&conversionTimeMode=VISIT`, {}, voluum.headers(token));
}

exports.campaignBreakdown = campaignBreakdown;
exports.campaignItemBreakdown = campaignItemBreakdown;
exports.siteBreakdown = siteBreakdown;
exports.campaignDetailBreakdown = campaignDetailBreakdown;
exports.contentBreakdown = contentBreakdown;
