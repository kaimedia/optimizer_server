'use strict';

const _ = require('lodash');
const request = require('request');
const Promise = require('bluebird');
const config = require('config').get('ADMEDIA');
const logger = require('../../../../lib/logger');

function headers() {
    let headers = { 'Content-Type': 'application/json' };
    headers['X-API-KEY'] = config.token;
    return headers;
}

function post(path, data, headers) {
    return new Promise(function (resolve, reject) {
        let url = `${config.path.host}${path}`;
        logger.info("[POST] %s", url);
        request({ url: url, method: 'POST', headers: headers, body: JSON.stringify(data) }
            , (error, response, body) => {
                if (error)
                    return reject(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function get(path, params, headers) {
    return new Promise(function (resolve, reject) {
        let url = `${config.path.host}${path}`;
        logger.info("[GET] %s", url);
        request({ url: url, method: 'GET', headers: headers, qs: params }
            , (error, response, body) => {
                if (error)
                    return reject(error);
                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function campaignBreakdown(start_date, end_date) {
    return get(`/reports/campaigns`, {
        from: start_date,
        to: end_date
    }, headers());
}

function campaignItemBreakdown(campaign_id, start_date, end_date) {
    return get(`/reports/campaigns/${campaign_id}`, {
        from: start_date,
        to: end_date
    }, headers());
}

exports.campaignBreakdown = campaignBreakdown;
exports.campaignItemBreakdown = campaignItemBreakdown;
