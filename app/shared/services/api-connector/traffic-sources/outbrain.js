'use strict';

const _ = require('lodash');
const request = require('request');
const Promise = require('bluebird');
const hash = require('object-hash');
const config = require('config').get('OUTBRAIN');
const logger = require('../../../../lib/logger');
const cache = require('../../../../lib/redis');
const client = cache._createClient();

function headers(token, isLogin = false) {
    const headers = { 'Content-Type': 'application/json' };

    if (isLogin) {
        const tokenGenerated = new Buffer(`${token.email}:${token.password}`).toString('base64');
        headers['Authorization'] = `Basic ${tokenGenerated}`;
    }
    else {
        headers['OB-TOKEN-V1'] = token['OB-TOKEN-V1'];
    }

    return headers;
}

function post(path, data, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.endpoint}${path}`;
        logger.info("[POST] %s", url);

        const options = { url: url, method: 'POST', headers: headers, body: data, json: true };

        request(options, (error, response, body) => {
            if (error) return reject(error);

            if (response.statusCode && response.statusCode >= 400)
                return reject({ message: body.errorMessage });

            try {
                resolve(JSON.parse(body));
            } catch (e) {
                resolve(body);
            }
        });
    });
}

function get(path, params, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.endpoint}${path}`;
        logger.info("[GET] %s", url);

        request({ url: url, method: 'GET', headers: headers, qs: params }
            , (error, response, body) => {
                if (error) return reject(error);

                if (body.http_status && body.http_status >= 400)
                    return reject(body);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function put(path, data, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.endpoint}${path}`;
        logger.info("[PUT] %s", url);

        request({ url: url, method: 'PUT', headers: headers, body: JSON.stringify(data) }
            , (error, response, body) => {
                if (error) return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function auth(accessKeys) {
    return new Promise(function (resolve, reject) {
        const token_key = hash(accessKeys);
        client.get(token_key, (err, data) => {
            if (err) return reject(err);
            if (data) {
                try {
                    resolve(JSON.parse(data));
                } catch (e) {
                    resolve(data);
                }
            } else {
                get('login', {}, headers(accessKeys, true)).then(dataToken => {
                    cache._set(client, token_key, dataToken, 2505600); // 29 days
                    resolve(dataToken);
                }, err => reject(err));
            }
        });
    });
}

/**
 * Get single marketer
 * 
 * @param {*} token 
 * @param {*} account_id 
 */
function getMarketer(token, account_id) {
    return get(`marketers/${account_id}`, {}, headers(token));
}

/**
 * Update single marketer
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} payload 
 */
function updateMarketer(token, account_id, payload) {
    return put(`marketers/${account_id}`, payload, headers(token));
}

/**
 * Get campaign list
 * 
 * @param {*} token 
 * @param {*} account_id 
 */
function campaignList(token, account_id) {
    return get(`marketers/${account_id}/campaigns`, {
        includeArchived: false,
        fetch: 'all',
        extraFields: 'Locations,BlockedSites,BidBySections'
    }, headers(token));
}

/**
 * Get a specific campaign
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 */
function specificCampaign(token, campaign_id) {
    return get(`campaigns/${campaign_id}`, {
        extraFields: 'Locations,BlockedSites,BidBySections'
    }, headers(token));
}

/**
 * Update a campaign
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 * @param {*} payload 
 */
function updateCampaign(token, campaign_id, payload) {
    return put(`campaigns/${campaign_id}?extraFields=Locations,BlockedSites,BidBySections`, payload, headers(token));
}

/**
 * Update a budget
 * 
 * @param {*} token 
 * @param {*} budget_id 
 * @param {*} payload 
 */
function updateBudget(token, budget_id, payload) {
    return put(`budgets/${budget_id}`, payload, headers(token));
}

/**
 * Get content list
 * 
 * @param {*} token 
 * @param {*} campaign_id 
 */
function contentList(token, campaign_id, query = null) {
    if (!query) {
        query = {
            limit: 500,
            offset: 0,
        };
    }

    return get(`campaigns/${campaign_id}/promotedLinks`, {
        statuses: 'APPROVED,PENDING,REJECTED',
        limit: query.limit,
        offset: query.offset,
    }, headers(token));
}

/**
 * Update content
 * 
 * @param {*} token 
 * @param {*} content_id 
 * @param {*} payload 
 */
function updateContent(token, content_id, payload) {
    return put(`promotedLinks/${content_id}`, payload, headers(token));
}

/**
 * Get campaigns summary
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignBreakdown(token, account_id, start_date, end_date, query = null) {
    if (!query) {
        query = {
            limit: 500,
            offset: 0,
        };
    }

    return get(`reports/marketers/${account_id}/campaigns`, {
        from: start_date,
        to: end_date,
        limit: query.limit,
        offset: query.offset,
        includeArchivedCampaigns: false,
        includeConversionDetails: false
    }, headers(token));
}

/**
 * Get campaign item summary
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignItemBreakdown(token, account_id, campaign_id, start_date, end_date) {
    return get(`reports/marketers/${account_id}/campaigns`, {
        campaignId: campaign_id,
        from: start_date,
        to: end_date,
        includeConversionDetails: true
    }, headers(token));
}

/**
 * Get sections summary
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function sectionBreakdown(token, account_id, campaign_id, start_date, end_date, query = null) {
    if (!query) {
        query = {
            limit: 500,
            offset: 0,
        };
    }

    return get(`reports/marketers/${account_id}/sections`, {
        campaignId: campaign_id,
        from: start_date,
        to: end_date,
        limit: query.limit,
        offset: query.offset,
        sort: '-spend',
        filter: 'clicks gt 0',
        includeArchivedCampaigns: false,
        includeConversionDetails: false
    }, headers(token));
}

/**
 * Get contents summary
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function contentBreakdown(token, account_id, campaign_id, start_date, end_date, query = null) {
    if (!query) {
        query = {
            limit: 500,
            offset: 0,
        };
    }

    return get(`reports/marketers/${account_id}/content`, {
        campaignId: campaign_id,
        from: start_date,
        to: end_date,
        limit: query.limit,
        offset: query.offset,
        includeArchivedCampaigns: false,
        includeConversionDetails: false
    }, headers(token));
}

exports.auth = auth;
exports.getMarketer = getMarketer;
exports.updateMarketer = updateMarketer;
exports.campaignList = campaignList;
exports.specificCampaign = specificCampaign;
exports.campaignBreakdown = campaignBreakdown;
exports.campaignItemBreakdown = campaignItemBreakdown;
exports.updateCampaign = updateCampaign;
exports.updateBudget = updateBudget;
exports.contentList = contentList;
exports.updateContent = updateContent;
exports.sectionBreakdown = sectionBreakdown;
exports.contentBreakdown = contentBreakdown;