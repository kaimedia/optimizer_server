'use strict';

const _ = require('lodash');
const request = require('request');
const Promise = require('bluebird');
const hash = require('object-hash');
const config = require('config').get('TABOOLA');
const logger = require('../../../../lib/logger');
const cache = require('../../../../lib/redis');
const client = cache._createClient();

function headers(token, content_type = null) {
    let headers = { 'Content-Type': 'application/json' };

    if (token)
        headers['Authorization'] = 'Bearer ' + token;

    if (content_type)
        headers['Content-Type'] = content_type;

    return headers;
}

function post(path, data, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.params.host}${path}`;
        logger.info("[POST] %s", url);

        let options = { url: url, method: 'POST', headers: headers };

        if (headers['Content-Type'] == 'application/x-www-form-urlencoded')
            options = _.assign(options, { form: data })
        else
            options = _.assign(options, { body: data, json: true })

        request(options, (error, response, body) => {
            if (error) return reject(error);

            if (body.http_status && body.http_status >= 400)
                return reject(body);

            try {
                resolve(JSON.parse(body));
            } catch (e) {
                resolve(body);
            }
        });
    });
}

function get(path, params, headers) {
    return new Promise(function (resolve, reject) {
        const url = `${config.params.host}${path}`;
        logger.info("[GET] %s", url);

        request({ url: url, method: 'GET', headers: headers, qs: params }
            , (error, response, body) => {
                if (error) return reject(error);

                if (body.http_status && body.http_status >= 400)
                    return reject(body);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function remove(path, headers) {
    const url = `${config.params.host}${path}`;
    logger.info("[DELETE] %s", url);

    request({ url: url, method: 'DELETE', headers: headers }
        , (error, response, body) => {
            if (error) return reject(error);

            if (body.http_status && body.http_status >= 400)
                return reject(body);

            try {
                resolve(JSON.parse(body));
            } catch (e) {
                resolve(body);
            }
        });
}

/**
 * Get token
 *
 * @returns {*|Promise}
 */
function auth(accessKeys) {
    return new Promise(function (resolve, reject) {
        const token_key = hash(accessKeys);
        client.get(token_key, (err, data) => {
            if (err) return reject(err);

            if (data) {
                try {
                    resolve({ access_token: JSON.parse(data) });
                } catch (e) {
                    resolve({ access_token: data });
                }
            } else {
                post(config.params.auth_access_path, {
                    client_id: accessKeys.client_id,
                    client_secret: accessKeys.client_secret,
                    grant_type: 'client_credentials'
                }, headers(null, 'application/x-www-form-urlencoded')).then(dataToken => {
                    if (dataToken.error)
                        return reject(dataToken);

                    cache._set(client, token_key, dataToken.access_token, 7200);
                    resolve(dataToken);
                }, err => reject(err));
            }
        });
    });
}

/**
 * Get campaign list
 * 
 * @param {*} token 
 * @param {*} account_id 
 */
function campaignList(token, account_id) {
    return get(`${config.params.prefix}${account_id}/campaigns/`, {}, headers(token));
}

/**
 * Get a specific campaign
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 */
function specificCampaign(token, account_id, campaign_id) {
    return get(`${config.params.prefix}${account_id}/campaigns/${campaign_id}/`, {}, headers(token));
}

/**
 * Get campaign summary
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function campaignBreakdown(token, account_id, start_date, end_date) {
    return get(`${config.params.prefix}${account_id}/reports/campaign-summary/dimensions/campaign_breakdown?start_date=${start_date}&end_date=${end_date}`, {}, headers(token));
}

/**
 * Update a campaign
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} data 
 */
function updateCampaign(token, account_id, campaign_id, data) {
    return post(`${config.params.prefix}${account_id}/campaigns/${campaign_id}/`, data, headers(token));
}

/**
 * Get site summary
 *
 * @param account_id
 * @param campaign_id
 * @param start_date
 * @param end_date
 */
function siteBreakdown(token, account_id, campaign_id, start_date, end_date) {
    return get(`${config.params.prefix}${account_id}/reports/campaign-summary/dimensions/site_breakdown?campaign=${campaign_id}&start_date=${start_date}&end_date=${end_date}`, {}, headers(token));
}

/**
 * Get content list
 *
 * @param token
 * @param account_id
 * @param campaign_id
 * @returns {*|Promise}
 */
function contentList(token, account_id, campaign_id) {
    return get(`${config.params.prefix}${account_id}/campaigns/${campaign_id}/items/`, {}, headers(token));
}

/**
 * Get top contents by campaign id
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} start_date 
 * @param {*} end_date 
 */
function contentBreakdown(token, account_id, campaign_id, start_date, end_date) {
    return get(`${config.params.prefix}${account_id}/reports/top-campaign-content/dimensions/item_breakdown?campaign=${campaign_id}&start_date=${start_date}&end_date=${end_date}`, {}, headers(token));
}

/**
 * Update content
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} item_id 
 * @param {*} data 
 */
function updateContent(token, account_id, campaign_id, content_id, data) {
    return post(`${config.params.prefix}${account_id}/campaigns/${campaign_id}/items/${content_id}/`, data, headers(token));
}

/**
 * Delete content
 * 
 * @param {*} token 
 * @param {*} account_id 
 * @param {*} campaign_id 
 * @param {*} content_id 
 */
function deleteContent(token, account_id, campaign_id, content_id) {
    return remove(`${config.params.prefix}${account_id}/campaigns/${campaign_id}/items/${content_id}/`, headers(token));
}

exports.auth = auth;
exports.campaignList = campaignList;
exports.specificCampaign = specificCampaign;
exports.campaignBreakdown = campaignBreakdown;
exports.siteBreakdown = siteBreakdown;
exports.updateCampaign = updateCampaign;
exports.contentList = contentList;
exports.contentBreakdown = contentBreakdown;
exports.updateContent = updateContent;
exports.deleteContent = deleteContent;