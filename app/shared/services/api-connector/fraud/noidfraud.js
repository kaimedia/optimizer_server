'use strict';

const _ = require('lodash');
const request = require('request');
const Promise = require('bluebird');
const config = require('config').get('NOIPFRAUD');
const logger = require('../../../../lib/logger');
const cache = require('../../../../lib/redis');
const client = cache._createClient();

function headers(token) {
    let headers = { 'Content-Type': 'application/json' };

    if (token)
        headers['Authorization'] = 'Bearer ' + token;

    return headers;
}

function post(path, data, headers) {
    return new Promise(function (resolve, reject) {
        let url = `${config.path.host}${path}`;
        logger.info("[POST] %s", url);

        request({ url: url, method: 'POST', headers: headers, body: JSON.stringify(data) }
            , (error, response, body) => {
                if (error)
                    return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function get(path, params, headers) {
    return new Promise(function (resolve, reject) {
        let url = `${config.path.host}${path}`;
        logger.info("[GET] %s", url);

        request({ url: url, method: 'GET', headers: headers, qs: params }
            , (error, response, body) => {
                if (error)
                    return reject(error);

                try {
                    resolve(JSON.parse(body));
                } catch (e) {
                    resolve(body);
                }
            });
    });
}

function auth() {
    return new Promise(function (resolve, reject) {
        const tokenKey = 'fraud_access_token';
        client.get(tokenKey, (err, token) => {
            if (err) return reject(err);

            if (token) {
                try {
                    resolve({ token: JSON.parse(token) });
                } catch (e) {
                    resolve({ token: token });
                }
            } else {
                post(config.path.login, {
                    username: config.username,
                    password: config.password
                }, headers()).then(dataToken => {
                    cache._set(client, tokenKey, dataToken.token, 10800);
                    resolve(dataToken);
                }, err => reject(err));
            }
        });
    });
}

function listCampaign(token) {
    return get(`${config.path.campaigns}?a=list`, {}, headers(token));
}

function getCampaign(token, campaign_id) {
    return get(`${config.path.campaigns}?a=get&clid=${campaign_id}`, {}, headers(token));
}

function createCampaign(token, fields) {
    return post(`${config.path.campaigns}?a=create`, fields, headers(token));
}

function updateCampaign(token, fields) {
    return post(`${config.path.campaigns}?a=update`, fields, headers(token));
}

function archiveCampaign(token, campaign_id) {
    return get(`${config.path.campaigns}?a=archive&clid=${campaign_id}`, {}, headers(token));
}

function statsCampaigns(token, start_date, end_date) {
    return get(`${config.path.stats}?a=daily&clid=&from=${start_date}&to=${end_date}`, {}, headers(token));
}

function statsCampaign(token, campaign_id, start_date, end_date) {
    return get(`${config.path.stats}?a=daily&clid=${campaign_id}&from=${start_date}&to=${end_date}`, {}, headers(token));
}

function trackingCampaign(token, campaign_id, start_date, end_date) {
    return get(`${config.path.stats}?a=params&clid=${campaign_id}&from=${start_date}&to=${end_date}`, {}, headers(token));
}

exports.auth = auth;
exports.listCampaign = listCampaign;
exports.getCampaign = getCampaign;
exports.createCampaign = createCampaign;
exports.updateCampaign = updateCampaign;
exports.archiveCampaign = archiveCampaign;
exports.statsCampaigns = statsCampaigns;
exports.statsCampaign = statsCampaign;
exports.trackingCampaign = trackingCampaign;