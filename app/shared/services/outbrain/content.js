'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const api_helper = require('../../../helpers/api');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/outbrain');
const outbrainAPI = require('../api-connector/traffic-sources/outbrain');
const trackerService = require('../voluum');
const schemaContent = require('../../helpers/schemas/content');
const variables = config.get('OUTBRAIN').variables;
const _VL = config.get('VOLUUM').mapKey;
const redis = require('../../../lib/redis');
const client = redis._createClient();

function contentSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date, cache = 1) {
    return new Promise(function (resolve, reject) {
        const promises = [
            outbrainContentSummary(accessTraffic, campaign_id, start_date, end_date, cache),
            voluumContentSummary(accessTracker, campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const content_outbrain = data[0].results;
            const content_voluum = data[1].results;
            const contentList = filterContentResults(mergeContents(content_outbrain, content_voluum));
            const contentSummary = calcContentSummary(data[0].summary, data[1].summary);

            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(contentList),
                contents: _.reverse(contentList),
                summary: contentSummary
            });
        }).catch(err => reject(err));
    });
}

function outbrainContentSummary(accessKeys, campaign_id, start_date, end_date, cache) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.contentBreakdown(token, accessKeys.account_id, campaign_id, start_date, end_date).then(data => {
                const metrics = _.map(data.results, item => {
                    let row = {};

                    if (_.isUndefined(item.metrics) || _.isUndefined(item.metadata))
                        return row;

                    const cpm = api_helper.calcCpm(item.metrics.spend, item.metrics.impressions);
                    row = {
                        id: item.metadata.id,
                        campaign_id: item.metadata.campaignId,
                        title: item.metadata.text,
                        thumbnail_url: item.metadata.cachedImageUrl,
                        url: item.metadata.url,
                        is_active: item.metadata.enabled,
                        impressions: item.metrics.impressions,
                        clicks: item.metrics.clicks,
                        ctr: item.metrics.ctr,
                        spent: item.metrics.spend,
                        cpc: item.metrics.ecpc,
                        cpm: cpm,
                        status: item.metadata.status,
                    };

                    return row;
                });

                const response = { results: metrics, summary: data.summary };

                // if (data.totalResults > 0)
                //     redis._set(client, key, response, 600);

                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
        // const key = api_helper.setCache(client, 'content_summary', {
        //     accessKeys: accessKeys,
        //     campaign_id: campaign_id,
        //     start_date: start_date,
        //     end_date: end_date
        // });
        // if (cache == 0) redis._del(client, key);
        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function voluumContentSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.contentBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                const response = _.map(data.rows, o => _.mapKeys(
                    _.pick(o, schemaContent.voluumContentOutbrainColumns), (val, key) => key + _VL)
                );
                resolve({ results: response, summary: data.totals });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function calcContentSummary(outbrainSummary, voluumSummary) {
    const trafficSourceTotals = {
        clicks: outbrainSummary.clicks,
        spent: outbrainSummary.spend,
        ctr: outbrainSummary.ctr,
        impressions: outbrainSummary.impressions,
        cpc: outbrainSummary.ecpc,
        cpm: api_helper.calcCpm(outbrainSummary.spend, outbrainSummary.impressions)
    };

    const trackerTotals = _.mapKeys(_.pick(voluumSummary, schemaContent.voluumContentOutbrainColumns), (val, key) => key + _VL);
    const totals = filterContentResults(_.assign(trafficSourceTotals, trackerTotals));
    return totals;
}

function outbrainContentList(token, campaign_id) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.contentList(token, campaign_id).then(data => {
            resolve({ results: data.promotedLinks });
        }, err => reject(err));
    });
}

function changeStatusText(oldStatus) {
    let newStatus;
    switch (oldStatus) {
        case 'APPROVED':
            newStatus = 'RUNNING';
            break;

        default:
            newStatus = oldStatus;
            break;
    }
    return newStatus;
}

function filterContentResults(data) {
    const removeFields = [variables.ad_id + _VL];
    if (!_.isArray(data)) {
        data = _.omit(data, removeFields);
        return _.defaults(
            api_helper.addContentColumns(api_helper.changeKeys(data, schemaContent.keyMappingOutbrain)),
            schemaContent.defaultFields
        );
    } else {
        data = _.map(data, obj => {
            obj = _.omit(obj, removeFields);
            return api_helper.addContentColumns(api_helper.changeKeys(obj, schemaContent.keyMappingOutbrain));
        });
        return _.map(data, o => _.defaults(o, schemaContent.defaultFields));
    }
}

function mergeContents(contentTrafficSource, contentTracker) {
    return _.map(contentTrafficSource, source => _.assign(source, _.find(contentTracker, tracker => {
        if (tracker[variables.ad_id + _VL] === source.id)
            return true;
        return false;
    })));
}

function updateContent(accessKeys, content_id, payload) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.updateContent(token, content_id, payload).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

exports.contentSummary = contentSummary;
exports.outbrainContentSummary = outbrainContentSummary;
exports.voluumContentSummary = voluumContentSummary;
exports.outbrainContentList = outbrainContentList;
exports.updateContent = updateContent;