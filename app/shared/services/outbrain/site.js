'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const api_helper = require('../../../helpers/api');
const Campaign = require('../../../models/campaign');
const Site = require('../../../models/site');
const SiteMetadata = require('../../../models/site.metadata');
const SiteMetricsRule = require('../../../models/site.metrics.rules');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/outbrain');
const outbrainAPI = require('../api-connector/traffic-sources/outbrain');
const siteService = require('../site');
const campaignService = require('./campaign');
const fraudService = require('../fraud');
const schemaSite = require('../../helpers/schemas/site');
const variables = config.get('OUTBRAIN').variables;
const _VL = config.get('VOLUUM').mapKey;
const redis = require('../../../lib/redis');
const client = redis._createClient();

function sectionSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date, cache) {
    return new Promise(function (resolve, reject) {
        const promises = [
            outbrainSectionList(accessTraffic, campaign_id, start_date, end_date, cache),
            voluumSectionSummary(accessTracker, campaign_id, start_date, end_date),
            fraudService.fraudTrackingList('section_id', campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(data => {
            const siteList = filterSiteResults(mergeSites(data[0].results, data[1].results, data[2]));
            const siteSummary = campaignService.calcCampaignItemTotals(data[0].summary, data[1].totals);
            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(siteList),
                sites: siteList,
                summary: siteSummary
            });
        }).catch(err => reject(err));
    });
}

function outbrainSectionList(accessKeys, campaign_id, start_date, end_date, cache) {
    return new Promise(function (resolve, reject) {
        const promises = [
            new Promise((resolve, reject) => {
                Campaign.findOne({ id: campaign_id }, (err, res) => {
                    if (err) return reject(err);
                    resolve(res);
                });
            }),
            new Promise((resolve, reject) => {
                SiteMetadata.find({}, (err, res) => {
                    if (err) return reject(err);
                    resolve(res);
                });
            }),
            campaignService.outbrainCampaignDetail(accessKeys, campaign_id),
            outbrainSectionSummary(accessKeys, campaign_id, start_date, end_date, cache)
        ];

        Promise.all(promises).then(data => {
            const campaignDatabase = data[0];
            const siteMetadata = data[1];
            const campaignDetail = data[2];
            let siteResults = data[3].results;
            let siteSummary = data[3].summary;

            if (!siteResults || _.isEmpty(siteResults))
                resolve({ results: [], summary: siteSummary });

            siteResults = _.map(siteResults, item => {
                let row = {};
                let auto_update_cpc = true;

                if (_.isUndefined(item.metrics) || _.isUndefined(item.metadata))
                    return row;

                if (siteMetadata) {
                    const site = _.find(siteMetadata, ['site_id', item.metadata.id]);
                    if (site && site.metadata) {
                        const meta = _.find(site.metadata, ['camp_id', campaign_id]);
                        if (meta && !meta.auto_update_cpc) {
                            auto_update_cpc = meta.auto_update_cpc;
                        }
                    }
                }

                const payout = campaignDatabase.payout ? campaignDatabase.payout : 0;
                const cpc_boost = item.metadata.cpcAdjustment ? item.metadata.cpcAdjustment : '';
                const cpc_boost_percent = _.round(cpc_boost * 100);
                const boosted_cpc = _.round((campaignDetail.cpc * cpc_boost) + campaignDetail.cpc, 3);
                const changeStatus = changeStatusText(item.metadata.blockStatus);
                const cpm = api_helper.calcCpm(item.metrics.spend, item.metrics.impressions);

                row = {
                    camp_id: campaignDetail.id,
                    camp_name: campaignDetail.name,
                    site_id: item.metadata.id,
                    site_name: item.metadata.name,
                    site_url: item.metadata.url,
                    publisher_id: item.metadata.publisherId,
                    publisher_name: item.metadata.publisherName,
                    cpc: item.metrics.ecpc,
                    impressions: item.metrics.impressions,
                    clicks: item.metrics.clicks,
                    spent: item.metrics.spend,
                    ctr: item.metrics.ctr,
                    cpm: cpm,
                    status: changeStatus,
                    configured_cpc: campaignDetail.cpc,
                    cpc_boost: cpc_boost,
                    cpc_boost_percent: cpc_boost_percent,
                    boosted_cpc: boosted_cpc,
                    ap_VL: payout,
                    auto_update_cpc: auto_update_cpc
                };

                return row;
            });

            resolve({ results: siteResults, summary: siteSummary });
        }).catch(err => reject(err));
    });
}

function outbrainSectionSummary(accessKeys, campaign_id, start_date, end_date, cache) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            const account_id = accessKeys.account_id;
            let dataTotal = [];
            let promises = [];
            let query = { limit: 500, offset: 0 };

            outbrainAPI.sectionBreakdown(token, account_id, campaign_id, start_date, end_date, query).then(data => {
                if (data.totalFilteredResults <= query.limit) {
                    // if (data.results && _.size(data.results) > 0) {
                    //     redis._set(client, key, data, 600);
                    // }
                    resolve(data);
                }

                const totalPages = _.ceil(_.divide(data.totalFilteredResults, query.limit));
                if (totalPages > 1) {
                    dataTotal = data;
                    for (let i = 0; i < totalPages; i++) {
                        query.offset = query.limit * i + query.limit;
                        promises.push(outbrainAPI.sectionBreakdown(token, account_id, campaign_id, start_date, end_date, query));
                    }
                    Promise.all(promises).then(response => {
                        _.forEach(response, e => {
                            if (e.results) {
                                dataTotal.results = _.concat(dataTotal.results, e.results);
                            }
                        });
                        // if (dataTotal.results && _.size(dataTotal.results) > 0) {
                        //     redis._set(client, key, dataTotal, 600);
                        // }
                        resolve(dataTotal);
                    }).catch(err => reject(err));
                }
            }, err => reject(err));
        }, err => reject(err));
        // const key = api_helper.setCache(client, 'section_summary', { accessKeys: accessKeys, campaign_id: campaign_id, start_date: start_date, end_date: end_date });
        // if (cache === 'false' || cache === false) redis._del(client, key);
        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function voluumSectionSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.sectionBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                let sites = _.map(data['rows'], o => _.mapKeys(
                    _.pick(o, schemaSite.voluumSectionOutbrainColumns), (val, key) => key + _VL)
                );
                resolve({
                    campaign_id: campaign_id,
                    recordCount: data.totalRows,
                    totals: data.totals,
                    results: sites
                });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function filterSites(siteList, status) {
    switch (status) {
        case 'active':
            siteList = _.filter(siteList, o => o.status === 'ACTIVE');
            break;

        case 'blocked':
            siteList = _.filter(siteList, o => o.status === 'BLOCKED');
            break;

        case 'pause':
        case 'blacklist':
            siteList = [];
            break;
    }
    return siteList;
}

function changeStatusText(currentStatus) {
    let status;
    switch (currentStatus) {
        case 'Unblocked':
            status = 'ACTIVE';
            break;

        case 'Blocked':
        case 'PartialBlock':
            status = 'BLOCKED';
            break;
    }
    return status;
}

function filterSiteResults(data) {
    const removeFields = [variables.section_id + _VL, variables.campaign_id + _VL, 'name_PT'];
    data = _.map(data, obj => {
        obj = _.omit(obj, removeFields);
        return _.defaults(api_helper.addSiteColumns(api_helper.changeKeys(obj, schemaSite.keyMappingOutbrain)), schemaSite.defaultFields);
    })
    return _.map(data, o => _.defaults(o, schemaSite.defaultFields));
}

function mergeSites(siteOutbrain, siteVoluum, sitesFraud) {
    let traffic_tracker = _.map(siteOutbrain, o => _.defaults(o, _.find(siteVoluum, [variables.section_id + _VL, o.site_id])));

    if (!_.isEmpty(sitesFraud))
        traffic_tracker = _.map(traffic_tracker, o => _.defaults(o, _.find(sitesFraud, ['name_PT', o.site_id])));

    return traffic_tracker;
}

function changeSiteStatus(accessKeys, siteList, status, userId = null) {
    return new Promise(function (resolve, reject) {
        let promises = [];
        _.forEach(siteList, item => {
            promises.push(siteService.saveSiteStatus(item, status, accessKeys.type, userId));
            if (status === 'active' || status === 'blocked') {
                promises.push(siteService.saveSiteMetadataStatus(item, status, accessKeys.type));
            }
        });
        Promise.all(promises).then(res => {
            if (status === 'blacklist') {
                campaignService.blacklistSitesCampaign(accessKeys, siteList).then(res => {
                    resolve(res);
                }, err => reject(err));
            } else {
                resolve({ success: true, message: 'Success.' });
            }
        }).catch(err => reject(err));
    });
}

exports.sectionSummary = sectionSummary;
exports.outbrainSectionSummary = outbrainSectionSummary;
exports.voluumSectionSummary = voluumSectionSummary;
exports.changeSiteStatus = changeSiteStatus;
