'use strict';

const _ = require('lodash');
const config = require('config');
const Promise = require('bluebird');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const logger = require('../../../lib/logger');
const api_helper = require('../../../helpers/api');
const Campaign = require('../../../models/campaign');
const voluumAPI = require('../api-connector/trackers/voluum');
const trackerAPI = require('../api-connector/trackers/outbrain');
const outbrainAPI = require('../api-connector/traffic-sources/outbrain');
const outbrainSiteService = require('../outbrain/site');
const siteService = require('../site');
const admediaService = require('../admedia');
const schema = require('../../helpers/schemas/campaign');
const variables = config.get('OUTBRAIN').variables;
const _VL = config.get('VOLUUM').mapKey;
const redis = require('../../../lib/redis');
const client = redis._createClient();

function finalCampaignSummary(accessTraffic, accessTracker, start_date, end_date, cache = 1) {
    return new Promise(function (resolve, reject) {
        const promises = [
            outbrainCampaignSummary(accessTraffic, start_date, end_date, cache),
            voluumCampaignSummary(accessTracker, start_date, end_date),
            admediaService.admediaCampaignSummary(start_date, end_date),
        ];
        Promise.all(promises).then(data => {
            const campaigns_outbrain = data[0].results;
            const campaigns_voluum = data[1].results;
            const campaigns_admedia = data[2].results;
            const campaignsFiltered = filterCampaignResults(mergeCampaigns(campaigns_outbrain, campaigns_voluum, campaigns_admedia));
            const campaignSummary = calcCampaignTotals(campaignsFiltered);
            resolve({
                start_date: start_date,
                end_date: end_date,
                timestamp: moment().format(),
                totalRows: _.size(campaignsFiltered),
                campaigns: campaignsFiltered,
                summary: campaignSummary,
            });
        }).catch(err => reject(err));
    });
}

function outbrainCampaignSummary(accessKeys, start_date, end_date, cache) {
    return new Promise(function (resolve, reject) {
        let promises = [
            new Promise((reply, cancel) => {
                Campaign.find({}, (err, res) => {
                    if (err) cancel(err);
                    if (res) reply(res);
                });
            }),
            new Promise((reply, cancel) => {
                outbrainCampaignList(accessKeys).then(res => {
                    reply(res);
                }, err => cancel(err));
            }),
            new Promise((reply, cancel) => {
                outbrainAPI.auth(accessKeys).then(token => {
                    outbrainAPI.campaignBreakdown(token, accessKeys.account_id, start_date, end_date).then(res => {
                        reply(res);
                    }, err => cancel(err));
                }, err => cancel(err));
            }),
        ];
        Promise.all(promises).then(data => {
            let campaignDatabaseList = data[0];
            let campaignOutbrainList = data[1];
            let campaignStats = data[2];

            campaignStats = _.map(campaignStats.results, item => {
                let row = {}, remarketing_id, amountSpent;

                if (_.isUndefined(item.metrics) || _.isUndefined(item.metadata))
                    return row;

                const findCampaignOutbrain = _.find(campaignOutbrainList.results, ['id', item.metadata.id]);
                if (findCampaignOutbrain) {
                    amountSpent = findCampaignOutbrain.liveStatus ? findCampaignOutbrain.liveStatus.amountSpent : 0;
                }

                const findCampaignDatabase = _.find(campaignDatabaseList, ['id', item.metadata.id]);
                if (findCampaignDatabase) {
                    remarketing_id = findCampaignDatabase.remarketing_id ? findCampaignDatabase.remarketing_id : null;
                }

                const cpm = api_helper.calcCpm(item.metrics.spend, item.metrics.impressions);
                const status = changeStatusText(item.metadata.onAirReason);
                row = {
                    id: item.metadata.id,
                    name: item.metadata.name,
                    amountSpent: amountSpent,
                    cpc: item.metadata.cpc,
                    spending_limit: item.metadata.budget.amount,
                    daily_ad_delivery_model: item.metadata.budget.type,
                    budget: item.metadata.budget,
                    clicks: item.metrics.clicks,
                    spent: item.metrics.spend,
                    ctr: item.metrics.ctr,
                    impressions: item.metrics.impressions,
                    cpm: cpm,
                    status: status,
                    remarketing_id: remarketing_id,
                };

                return row;
            });

            resolve({ results: campaignStats });
        }).catch(err => reject(err));

        // const key = api_helper.setCache(client, 'campaign_summary', { accessKeys: accessKeys, start_date: start_date, end_date: end_date });
        // if (cache == 0) redis._del(client, key);
        // client.get(key, (err, hasCache) => {
        //     if (err) throw err;
        //     if (hasCache) {
        //         resolve(JSON.parse(hasCache));
        //     } else {

        //     }
        // });
    });
}

function voluumCampaignSummary(accessKeys, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.campaignBreakdown(auth.token, start_date, end_date).then(data => {
                let response = _.map(data.rows, o => _.mapKeys(
                    _.pick(o, schema.voluumCampaignOutbrainColumns), (val, key) => key + _VL)
                );
                resolve({ results: response });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function outbrainCampaignList(accessKeys, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = schema.outbrainDetailColumns;

        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.campaignList(token, accessKeys.account_id).then(data => {
                const results = _.map(data.campaigns, item => {
                    const status = item.liveStatus.onAirReason;
                    item = _.pick(item, columns);
                    item['status'] = changeStatusText(status);
                    return item;
                });
                resolve({ results: results });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function outbrainCampaignDetail(accessKeys, campaign_id, columns = null) {
    return new Promise(function (resolve, reject) {
        if (!columns)
            columns = schema.outbrainDetailColumns;

        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.specificCampaign(token, campaign_id).then(item => {
                const status = item.liveStatus.onAirReason;
                item = _.pick(item, columns);
                item['status'] = changeStatusText(status);
                resolve(item);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateCampaignOutbrain(accessKeys, campaign_id, payload) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.updateCampaign(token, campaign_id, payload).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateBudget(accessKeys, budget_id, payload) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.updateBudget(token, budget_id, payload).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function campaignItemTotals(accessTraffic, accessTracker, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        const promises = [
            outbrainCampaignItemSummary(accessTraffic, campaign_id, start_date, end_date),
            voluumCampaignItemSummary(accessTracker, campaign_id, start_date, end_date)
        ];
        Promise.all(promises).then(results => {
            const totals = calcCampaignItemTotals(results[0].summary, results[1].totals);
            resolve(totals);
        }).catch(err => reject(err));
    });
}

function outbrainCampaignItemSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.campaignItemBreakdown(token, accessKeys.account_id, campaign_id, start_date, end_date).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function voluumCampaignItemSummary(accessKeys, campaign_id, start_date, end_date) {
    return new Promise(function (resolve, reject) {
        voluumAPI.auth(accessKeys).then(auth => {
            trackerAPI.campaignItemBreakdown(auth.token, campaign_id, start_date, end_date).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function calcCampaignItemTotals(outbrainTotals, voluumTotals) {
    const trafficSourceTotals = {
        clicks: outbrainTotals.clicks,
        spent: outbrainTotals.spend,
        ctr: outbrainTotals.ctr,
        impressions: outbrainTotals.impressions,
        cpc: outbrainTotals.ecpc,
        cpm: api_helper.calcCpm(outbrainTotals.spend, outbrainTotals.impressions),
    };

    const trackerTotals = _.mapKeys(_.pick(voluumTotals, schema.voluumCampaignOutbrainColumns), (val, key) => key + _VL);
    const totals = filterCampaignResults(_.assign(trafficSourceTotals, trackerTotals));
    return totals;
}

function filterCampaignResults(data) {
    const removeFields = [variables.campaign_id + _VL, 'remarketing_id', 'id_AD'];
    if (!_.isArray(data)) {
        data = _.omit(data, removeFields);
        return _.defaults(
            api_helper.addCampaignColumns(api_helper.changeKeys(data, schema.keyMappingOutbrain)),
            schema.defaultFields
        );
    } else {
        data = _.map(data, obj => {
            obj = _.omit(obj, removeFields);
            return api_helper.addCampaignColumns(api_helper.changeKeys(obj, schema.keyMappingOutbrain));
        });
        return _.map(data, o => _.defaults(o, schema.defaultFields));
    }
}

function mergeCampaigns(outbrainCampaigns, voluumCampaigns, admediaCampaigns) {
    let combinedCampaigns = _.map(outbrainCampaigns, e => {
        e = _.assign(e, _.find(voluumCampaigns, [variables.campaign_id + _VL, e.id]));
        e = _.assign(e, _.find(admediaCampaigns, ['id_AD', e.remarketing_id]));
        return e;
    });
    return combinedCampaigns;
}

function calcCampaignTotals(campaignData) {
    const sumResults = api_helper.calculatorSumFields(campaignData, ['cost', 'revenue']);
    const net = api_helper.round(_.subtract(sumResults['revenue'], sumResults['cost']), 2);
    const roi = api_helper.round(api_helper.divide(net, sumResults['cost']) * 100, 2);
    return _.assign(sumResults, { net, roi });
}

function changeStatusText(oldStatus) {
    let newStatus;
    switch (oldStatus) {
        case 'CAMPAIGN_ON_AIR':
            newStatus = 'RUNNING';
            break;

        case 'CAMPAIGN_DISABLED':
            newStatus = 'PAUSED';
            break;

        case 'CAMPAIGN_FLY_DATES_PAST':
            newStatus = 'ENDED';
            break;

        case 'CAMPAIGN_FLY_DATES_FUTURE':
            newStatus = 'SCHEDULED';
            break;

        case 'CAMPAIGN_DAILY_CAPPED':
            newStatus = 'DAILY CAPPED';
            break;

        case 'CAMPAIGN_DAILY_CAPPED_MONTHLY':
            newStatus = 'DAILY CAPPED MONTHLY';
            break;

        case 'CAMPAIGN_DAILY_CAPPED_CAMPAIGN':
            newStatus = 'DAILY CAPPED CAMPAIGN';
            break;

        case 'CAMPAIGN_BUDGET_DEPLETED':
            newStatus = 'BUDGET DEPLETED';
            break;

        case 'CAMPAIGN_ALL_PROMOTED_LINKS_DISABLED':
            newStatus = 'DISABLED';
            break;

        case 'CAMPAIGN_ALL_PROMOTED_LINKS_REJECTED':
            newStatus = 'REJECTED';
            break;

        case 'ALL_PROMOTED_LINKS_EXPIRED':
            newStatus = 'EXPIRED';
            break;

        case 'ALL_PROMOTED_LINKS_PENDING':
            newStatus = 'PENDING';
            break;

        case 'WAITING_FOR_START_HOUR':
            newStatus = 'WATTING';
            break;

        case 'NO_RUNNING_PROMOTED_LINKS':
            newStatus = 'NO RUNNING';
            break;

        case 'MISSING_BILLING_INFO':
            newStatus = 'NO CREDIT CARD';
            break;

        case 'EXPIRED_BILLING_INFO':
            newStatus = 'CREDIT CARD EXPIRED';
            break;

        case 'FAILED_BILLING_INFO':
            newStatus = 'CREDIT CARD FAILED';
            break;

        case 'BAD_CONTENT':
            newStatus = 'BAD CONTENT';
            break;

        case 'OUTSTANDING_UNPAID_BALANCE':
            newStatus = 'UNPAID BALANCE';
            break;

        case 'TOS_VIOLATION':
            newStatus = 'TOS VIOLATION';
            break;

        case 'NEW_CUSTOMER_PENDING':
            newStatus = 'UNDER REVIEW';
            break;

        case 'PAUSED':
            newStatus = 'ACCOUNT PAUSED';
            break;

        case 'MARKED_DELETED':
            newStatus = 'MARKED DELETED';
            break;

        case 'LEGACY_UNKNOWN':
            newStatus = 'LEGACY UNKNOWN';
            break;

        case 'FAILED_THRESHOLD_BILLING':
            newStatus = 'FAILED THRESHOLD BILLING';
            break;

        default:
            newStatus = oldStatus;
            break;
    }
    return newStatus;
}

function blockSitesCampaign(accessKeys, campaign_id, sites, userId = null) {
    return new Promise(function (resolve, reject) {
        outbrainCampaignDetail(accessKeys, campaign_id).then(campaign => {
            blockSitesService(accessKeys, campaign, sites, userId).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function blockSitesService(accessKeys, campaign, sites, userId = null) {
    return new Promise(function (resolve, reject) {
        const siteItems = _.map(sites, o => {
            return { id: o.site_id };
        });

        let blocked_sites = campaign.blockedSites && campaign.blockedSites.blockedSections
            ? campaign.blockedSites.blockedSections
            : [];

        blocked_sites = _.unionBy(blocked_sites, siteItems, 'id');
        blocked_sites = _.map(blocked_sites, o => _.pick(o, 'id'));

        const payload = {
            blockedSites: {
                blockedSections: blocked_sites
            }
        };

        updateCampaignOutbrain(accessKeys, campaign.id, payload).then(res => {
            outbrainSiteService.changeSiteStatus(accessKeys, sites, 'blocked', userId).then(res => {
                resolve({ success: true, message: 'Success.' });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function unBlockSitesCampaign(accessKeys, campaign_id, sites) {
    return new Promise(function (resolve, reject) {
        outbrainCampaignDetail(accessKeys, campaign_id).then(campaign => {
            unBlockSitesService(accessKeys, campaign, sites).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function unBlockSitesService(accessKeys, campaign, sites) {
    return new Promise(function (resolve, reject) {
        const siteIds = _.map(sites, o => o.site_id);

        let payload = {
            blockedSites: {
                blockedSections: []
            }
        };

        let blockedSites = campaign.blockedSites && campaign.blockedSites.blockedSections
            ? campaign.blockedSites.blockedSections
            : [];

        _.remove(blockedSites, e => siteIds.indexOf(e.id) != -1);

        if (!_.isEmpty(blockedSites)) {
            blockedSites = _.map(blockedSites, o => _.pick(o, 'id'));
            payload.blockedSites.blockedSections = blockedSites;
        }

        updateCampaignOutbrain(accessKeys, campaign.id, payload).then(response => {
            siteService.removeBlockedSites(sites, accessKeys.type, campaign.id).then(result => {
                resolve({ success: true, message: 'Success.' });
            }, err => reject(err));
        }, err => reject(err));
    });
}

function blacklistSitesCampaign(accessKeys, sites) {
    return new Promise(function (resolve, reject) {
        marketerDetail(accessKeys).then(marketer => {
            blacklistSitesService(accessKeys, marketer, sites).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function blacklistSitesService(accessKeys, marketer, sites) {
    return new Promise(function (resolve, reject) {

        const siteItems = _.map(sites, o => {
            return { id: o.site_id };
        });

        let blacklisted_sites = marketer.blockedSites && marketer.blockedSites.blockedSections
            ? marketer.blockedSites.blockedSections
            : [];

        blacklisted_sites = _.unionBy(siteItems, blacklisted_sites, 'id');

        blacklisted_sites = _.map(blacklisted_sites, o => _.pick(o, 'id'));

        const payload = {
            blockedSites: {
                blockedSections: blacklisted_sites
            }
        };

        updateMarketer(accessKeys, payload).then(response => {
            resolve({ success: true, message: 'Success.' });
        }, err => reject(err));
    });
}

function marketerDetail(accessKeys) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.getMarketer(token, accessKeys.account_id).then(data => {
                resolve(data);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function updateMarketer(accessKeys, payload) {
    return new Promise(function (resolve, reject) {
        outbrainAPI.auth(accessKeys).then(token => {
            outbrainAPI.updateMarketer(token, accessKeys.account_id, payload).then(response => {
                resolve(response);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function changeSiteCpc(accessKeys, site) {
    return new Promise(function (resolve, reject) {
        if (site.cpc_boost_percent >= 1000)
            return reject({ success: false, message: 'CPC adjustment must be lower than 1000.' });

        if (site.boosted_cpc <= 0)
            return reject({ success: false, message: 'Fixed CPC must be higher than zero.' });

        outbrainCampaignDetail(accessKeys, site.camp_id).then(campaign => {
            if (site.boosted_cpc > 0 && site.boosted_cpc < campaign.minimumCpc) {
                site.boosted_cpc = campaign.minimumCpc;
                site.cpc_boost = _.round((site.boosted_cpc - site.configured_cpc) / site.configured_cpc, 2);
            }

            let modified_sites = campaign.bids ? campaign.bids.bySection : [];
            if (site.cpc_boost_percent === 0) {
                if (isSiteExists(site, modified_sites)) {
                    _.remove(modified_sites, e => site.site_id.indexOf(e.sectionId) != -1);
                } else {
                    return resolve({ success: false, message: 'Success.' });
                }
            } else {
                if (!isSiteExists(site, modified_sites)) {
                    modified_sites.push({
                        sectionId: site.site_id,
                        cpcAdjustment: site.cpc_boost
                    });
                } else {
                    modified_sites = _.unionBy([{
                        sectionId: site.site_id,
                        cpcAdjustment: site.cpc_boost
                    }], modified_sites, 'sectionId');
                }
            }

            const payload = {
                bids: {
                    bySection: _.map(modified_sites, o => _.pick(o, ['sectionId', 'cpcAdjustment']))
                }
            };

            updateCampaignOutbrain(accessKeys, site.camp_id, payload).then(res => {
                siteService.saveSiteMetadataCpc(site, accessKeys.type).then(res => {
                    resolve({ success: true, message: 'Success.' });
                }, err => reject(err));
            }, err => reject(err));
        }, err => reject(err));
    });
}

function isSiteExists(site, siteList) {
    const sites = _.filter(siteList, o => o.sectionId === site.site_id);
    if (_.size(sites) > 0)
        return true;
    return false;
}

exports.finalCampaignSummary = finalCampaignSummary;
exports.outbrainCampaignSummary = outbrainCampaignSummary;
exports.outbrainCampaignItemSummary = outbrainCampaignItemSummary;
exports.voluumCampaignSummary = voluumCampaignSummary;
exports.voluumCampaignItemSummary = voluumCampaignItemSummary;
exports.outbrainCampaignList = outbrainCampaignList;
exports.outbrainCampaignDetail = outbrainCampaignDetail;
exports.updateCampaignOutbrain = updateCampaignOutbrain;
exports.updateBudget = updateBudget;
exports.campaignItemTotals = campaignItemTotals;
exports.calcCampaignItemTotals = calcCampaignItemTotals;
exports.filterCampaignResults = filterCampaignResults;
exports.mergeCampaigns = mergeCampaigns;
exports.blockSitesCampaign = blockSitesCampaign;
exports.unBlockSitesCampaign = unBlockSitesCampaign;
exports.blacklistSitesCampaign = blacklistSitesCampaign;
exports.changeSiteCpc = changeSiteCpc;
exports.changeStatusText = changeStatusText;