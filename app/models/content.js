const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var contentMetadataSchema = new Schema({
    id: String,
    name: String,
    thumbnail_url: String,
    url: String,
    campaign_id: String,
    trafficSource: String,
    is_active: Boolean,
    status: String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

contentMetadataSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const contentMetadata = mongoose.model('content', contentMetadataSchema);
module.exports = contentMetadata;