const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var siteMetricsSchema = new Schema({
    last_rawdata_update_time: String,
    last_rawdata_update_timestamp: Number,
    start_date: String,
    end_date: String,
    campaign_id: String,
    campaign_name: String,
    trafficSource: String,
    totalRows: Number,
    totals: Object,
    rows: Array,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});


siteMetricsSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const siteMetrics = mongoose.model('sites.metrics', siteMetricsSchema);
module.exports = siteMetrics;