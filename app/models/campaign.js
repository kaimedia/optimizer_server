const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var campaignSchema = new Schema({
    id: String,
    name: String,
    cpc: Number,
    minimumCpc: Number,
    spending_limit: Number,
    daily_cap: Number,
    daily_ad_delivery_model: String,
    budget: Object,
    amountSpent: Number,
    publisher_targeting: Object,
    publisher_bid_modifier: Object,
    start_date: String,
    type: String,
    countryCode: String,
    trafficSourceAccount: String,
    trackerAccount: String,
    tracker_id: String,
    id_fraud: String,
    remarketing_id: Number,
    campaignUrl: String,
    deployUrl: String,
    payout: Number,
    blockSites: Array,
    is_active: Boolean,
    status: String,
    archived: { type: Number, default: 0 },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

campaignSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const Campaign = mongoose.model('Campaign', campaignSchema);
module.exports = Campaign;