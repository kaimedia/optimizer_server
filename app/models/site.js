const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var siteSchema = new Schema({
    site_id: String,
    site_name: String,
    trafficSource: String,
    metrics: Array,
    status: String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

siteSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const Site = mongoose.model('Site', siteSchema);
module.exports = Site;