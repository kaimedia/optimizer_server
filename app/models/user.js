const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    firstName: String,
    lastName: String,
    avatar: String,
    address: String,
    country: String,
    token: String,
    role: { type: String, default: 'GUEST' },
    active: { type: Boolean, default: true },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

userSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const User = mongoose.model('User', userSchema);
module.exports = User;