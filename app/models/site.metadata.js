const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var siteMetadataSchema = new Schema({
    site_id: String,
    site_name: String,
    site_url: String,
    publisher_id: String,
    publisher_name: String,
    trafficSource: String,
    metadata: Array,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

siteMetadataSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const siteMetadata = mongoose.model('sites.metadata', siteMetadataSchema);
module.exports = siteMetadata;