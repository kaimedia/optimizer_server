const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var campaignMetricsSchema = new Schema({
    last_rawdata_update_time: String,
    last_rawdata_update_timestamp: Number,
    start_date: String,
    end_date: String,
    campaign_id: String,
    campaign_name: String,
    trafficSource: String,
    metrics: Object,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

campaignMetricsSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const campaignMetrics = mongoose.model('campaigns.metrics', campaignMetricsSchema);
module.exports = campaignMetrics;