const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var trackerSchema = new Schema({
    user_id: String,
    name: { type: String, required: true },
    type: { type: String, required: true },
    access_id: { type: String, required: true },
    access_key: { type: String, required: true },
    status: Boolean,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

trackerSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const Tracker = mongoose.model('Tracker', trackerSchema);
module.exports = Tracker;