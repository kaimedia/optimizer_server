const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var trafficSourceSchema = new Schema({
    user_id: String,
    name: { type: String, required: true },
    type: { type: String, required: true },
    account_id: String,
    client_id: String,
    client_secret: String,
    username: String,
    email: String,
    password: String,
    trackers: { type: Array, required: true },
    status: Boolean,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

trafficSourceSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const trafficSource = mongoose.model('trafficSource', trafficSourceSchema);
module.exports = trafficSource;