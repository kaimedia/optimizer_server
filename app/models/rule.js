const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ruleSchema = new Schema({
    name: { type: String, required: true },
    action: Number,
    interval: String,
    rotation: String,
    level: Number,
    campaigns: { type: Array, required: true },
    criterias: { type: Array, required: true },
    increase_cpc: Array,
    decrease_cpc: Array,
    status: Boolean,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

ruleSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const Rule = mongoose.model('Rule', ruleSchema);
module.exports = Rule;