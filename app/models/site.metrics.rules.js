const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var siteMetricsRuleSchema = new Schema({
    start_date: String,
    end_date: String,
    timestamp: String,
    trafficSource: String,
    campaign: Object,
    results: Array,
    totalResults: Number,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

siteMetricsRuleSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const siteMetricsRule = mongoose.model('sites.metrics.rule', siteMetricsRuleSchema);
module.exports = siteMetricsRule;