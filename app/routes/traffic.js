'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/traffic');
const trafficController = require('../controllers/traffic');

router.use(auth.required);

router.post('/',
    validate(paramValidation.create),
    trafficController.create
);

router.get('/',
    trafficController.all
);

router.get('/:id',
    validate(paramValidation.get),
    trafficController.get
);

router.patch('/:id',
    validate(paramValidation.update),
    trafficController.update
);

router.delete('/:id',
    validate(paramValidation.remove),
    trafficController.remove
);

module.exports = router;