'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const accessAPI = require('../middlewares/account');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/site');
const siteController = require('../controllers/site');

router.use(auth.required);

router.get('/list',
    accessAPI.required,
    validate(paramValidation.list),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/site`).list(req, res, next);
    }
);

router.patch('/blockSites',
    accessAPI.required,
    validate(paramValidation.blockSites),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/site`).blockSites(req, res, next);
    }
);

router.patch('/unBlockSites',
    accessAPI.required,
    validate(paramValidation.unBlockSites),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/site`).unBlockSites(req, res, next);
    }
);

router.patch('/changeStatus',
    accessAPI.required,
    validate(paramValidation.changeStatus),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/site`).changeStatus(req, res, next);
    }
);

router.get('/getStatus',
    accessAPI.required,
    validate(paramValidation.getStatus),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/site`).getStatus(req, res, next);
    }
);

router.post('/changeAutoCpc',
    accessAPI.required,
    validate(paramValidation.changeAutoCpc),
    siteController.changeAutoCpc
);

router.get('/blockedSites',
    accessAPI.required,
    siteController.blockedSites
);

router.get('/updateStats',
    validate(paramValidation.updateStats),
    siteController.updateStats
);

router.get('/siteMetrics',
    validate(paramValidation.siteMetrics),
    siteController.siteMetrics
);

module.exports = router;