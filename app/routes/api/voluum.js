'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const accessAPI = require('../../middlewares/account');
const authentication = require('../../middlewares/authentication');
const paramValidation = require('../../middlewares/validation/api/voluum');
const voluumControllers = require('../../controllers/api/voluum');

router.use(authentication.required);

router.get('/traffic-sources',
    accessAPI.required,
    voluumControllers.getTrafficSources
);

router.get('/affiliate-network',
    accessAPI.required,
    voluumControllers.getAffiliateNetworks
);

router.get('/campaign',
    accessAPI.required,
    validate(paramValidation.getCampaign),
    voluumControllers.getCampaignDetail
);

router.put('/campaign',
    accessAPI.required,
    validate(paramValidation.updateCampaign),
    voluumControllers.updateCampaign
);

router.get('/landers',
    accessAPI.required,
    voluumControllers.getLanders
);

router.get('/lander',
    accessAPI.required,
    validate(paramValidation.getLander),
    voluumControllers.getLander
);

router.post('/lander',
    accessAPI.required,
    voluumControllers.createLander
);

router.put('/lander',
    accessAPI.required,
    validate(paramValidation.updateLander),
    voluumControllers.updateLander
);

router.get('/offers',
    accessAPI.required,
    voluumControllers.getOffers
);

router.get('/offer',
    accessAPI.required,
    validate(paramValidation.getOffer),
    voluumControllers.getOffer
);

router.post('/offer',
    accessAPI.required,
    voluumControllers.createOffer
);

router.put('/offer',
    accessAPI.required,
    validate(paramValidation.updateOffer),
    voluumControllers.updateOffer
);

router.get('/tracker-view',
    accessAPI.required,
    validate(paramValidation.trackerView),
    voluumControllers.trackerView
);

module.exports = router;