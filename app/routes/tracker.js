'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/tracker');
const trackerController = require('../controllers/tracker');

router.use(auth.required);

router.post('/',
    validate(paramValidation.createTracker),
    trackerController.create
);

router.get('/',
    trackerController.all
);

router.get('/:id',
    validate(paramValidation.getTracker),
    trackerController.get
);

router.patch('/:id',
    validate(paramValidation.updateTracker),
    trackerController.update
);

router.delete('/:id',
    validate(paramValidation.removeTracker),
    trackerController.remove
);

module.exports = router;