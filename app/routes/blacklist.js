'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/blacklist');
const blacklistController = require('../controllers/blacklist');

router.use(auth.required);

router.get('/list',
    validate(paramValidation.list),
    blacklistController.list
);

router.patch('/unBlock',
    blacklistController.unBlock
);

module.exports = router;