'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const accessAPI = require('../middlewares/account');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/campaign');
const campaignController = require('../controllers/campaign');

router.use(auth.required);

router.get('/all',
    campaignController.all
);

router.get('/detail',
    campaignController.detail
);

router.post('/create',
    campaignController.create
);

router.post('/update',
    campaignController.update
);

router.get('/archiveList',
    campaignController.archiveList
);

router.post('/archive',
    validate(paramValidation.archive),
    campaignController.archive
);

router.post('/unArchive',
    validate(paramValidation.unArchive),
    campaignController.unArchive
);

router.get('/allByStatus',
    validate(paramValidation.allByStatus),
    campaignController.allByStatus
);

router.get('/list',
    accessAPI.required,
    validate(paramValidation.list),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).list(req, res, next);
    }
);

router.get('/get',
    accessAPI.required,
    validate(paramValidation.get),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).get(req, res, next);
    }
);

router.post('/campaignApiUpdate',
    accessAPI.required,
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).update(req, res, next);
    }
);

router.patch('/changeStatus',
    accessAPI.required,
    validate(paramValidation.changeStatus),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).changeStatus(req, res, next);
    }
);

router.get('/getTotalsDetail',
    accessAPI.required,
    validate(paramValidation.getTotalsDetail),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).getTotalsDetail(req, res, next);
    }
);

router.patch('/changeBid',
    accessAPI.required,
    validate(paramValidation.changeBid),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).changeBid(req, res, next);
    }
);

router.patch('/changeSiteCpc',
    accessAPI.required,
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).changeSiteCpc(req, res, next);
    }
);

router.get('/fraudStatsDaily',
    accessAPI.required,
    validate(paramValidation.fraudStatsDaily),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/campaign`).fraudStatsDaily(req, res, next);
    }
);

router.get('/updateStats',
    validate(paramValidation.updateStats),
    campaignController.updateStats
);

router.get('/campaignMetrics',
    validate(paramValidation.campaignMetrics),
    campaignController.campaignMetrics
);

module.exports = router;