'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const accessAPI = require('../middlewares/account');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/fraud');
const fraudController = require('../controllers/fraud');

router.use(auth.required);

router.get('/campaign/list',
    fraudController.list
);

router.get('/campaign?campaign_id=:campaign_id',
    validate(paramValidation.getCampaign),
    fraudController.get
);

router.post('/campaign/create',
    validate(paramValidation.createCampaign),
    fraudController.create
);

router.get('/campaign/archive?campaign_id=:campaign_id',
    validate(paramValidation.archiveCampaign),
    fraudController.archive
);

router.get('/statsTotals',
    validate(paramValidation.statsCampaigns),
    fraudController.statsCampaigns
);

router.get('/test',
    fraudController.test
);

module.exports = router;