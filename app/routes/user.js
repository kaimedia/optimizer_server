'use strict';


const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/user');
const userController = require('../controllers/user');

router.use(auth.required);

router.post('/',
    validate(paramValidation.create),
    userController.create
);

router.get('/',
    userController.all
);

router.get('/:id',
    validate(paramValidation.get),
    userController.get
);

router.delete('/:id',
    validate(paramValidation.remove),
    userController.remove
);

router.patch('/',
    validate(paramValidation.update),
    userController.update
);

router.patch('/password',
    validate(paramValidation.changePassword),
    userController.password
);

module.exports = router;