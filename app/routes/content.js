'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/content');
const accessAPI = require('../middlewares/account');
const contentController = require('../controllers/content');

router.use(auth.required);

router.get('/list',
    accessAPI.required,
    validate(paramValidation.list),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/content`).list(req, res, next);
    }
);

router.post('/update',
    accessAPI.required,
    validate(paramValidation.update),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/content`).update(req, res, next);
    }
);

router.delete('/',
    accessAPI.required,
    validate(paramValidation.remove),
    (req, res, next) => {
        require(`../controllers/api/${_.toLower(req.accessAPI.traffic.type)}/content`).remove(req, res, next);
    }
);

router.get('/updateStats',
    validate(paramValidation.updateStats),
    contentController.updateStats
);

router.get('/contentMetrics',
    validate(paramValidation.contentMetrics),
    contentController.contentMetrics
);

module.exports = router;