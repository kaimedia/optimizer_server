'use strict';


const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/auth');
const authController = require('../controllers/auth');

router.post('/register',
    validate(paramValidation.registerAuth),
    authController.register
);

router.post('/login',
    authController.login
);

router.get('/logout',
    auth.required,
    authController.logout
);

router.get('/me',
    auth.required,
    authController.me
);

module.exports = router;