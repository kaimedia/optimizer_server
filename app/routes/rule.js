'use strict';

const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const paramValidation = require('../middlewares/validation/rule');
const ruleController = require('../controllers/rule');

router.use(auth.required);

router.post('/',
    validate(paramValidation.createRule),
    ruleController.create
);

router.get('/',
    ruleController.all
);

router.get('/:id',
    validate(paramValidation.getRule),
    ruleController.get
);

router.patch('/:id',
    validate(paramValidation.updateRule),
    ruleController.update
);

router.delete('/:id',
    validate(paramValidation.deleteRule),
    ruleController.remove
);

module.exports = router;