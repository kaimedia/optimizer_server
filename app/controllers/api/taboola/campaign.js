'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const apiHelper = require('../../../helpers/api');
const Campaign = require('../../../models/campaign');
const campaignService = require('../../../shared/services/taboola/campaign');
const siteService = require('../../../shared/services/site');
const fraudService = require('../../../shared/services/fraud');

function list(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const accessVoluum = req.accessAPI.tracker;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');

    campaignService.finalCampaignSummary(accessTaboola, accessVoluum, start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function get(req, res, next) {
    const campaign_id = req.query.id;
    Campaign.findOne({ id: campaign_id }, (err, campaign) => {
        if (err) return next(err);
        if (!_.isEmpty(campaign)) {
            res.status(200).json(campaign);
        } else {
            campaignService.taboolaCampaignDetail(req.accessAPI.traffic, campaign_id).then(result => {
                res.status(200).json(result);
            }, err => next(err));
        }
    });
}

function changeStatus(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const campaign_id = req.body.id;
    const data = {
        is_active: req.body.is_active
    };

    campaignService.updateCampaignTaboola(accessTaboola, campaign_id, data).then(result => {
        const status = data.is_active ? 'RUNNING' : 'PAUSED';
        Campaign.findOneAndUpdate({ id: campaign_id }, {
            is_active: data.is_active,
            status: status
        }, (err, response) => {
            if (err) throw err;
            res.status(200).send({ message: 'Success.' });
        });
    }, err => next(err));
}

function update(req, res, next) {
    Campaign.findByIdAndUpdate(req.body._id, apiHelper.removeKey(req.body), { new: true }, (err, result) => {
        if (err) return next(err);
        res.status(200).json([]);
    });
}

function getTotalsDetail(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const accessVoluum = req.accessAPI.tracker;
    const campaign_id = req.query.id;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');

    campaignService.campaignItemTotals(accessTaboola, accessVoluum, campaign_id, start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function changeBid(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const campaign_id = req.body.id;
    const data = {
        cpc: req.body.cpc,
        spending_limit: req.body.spending_limit,
    };

    campaignService.updateCampaignTaboola(accessTaboola, campaign_id, data).then(response => {
        Campaign.findOneAndUpdate({ id: campaign_id }, data, (err, response) => {
            if (err) throw err;
            siteService.updateSitesConfiguredCpc(campaign_id, cpc).then(response => {
                res.status(200).send({ success: true, message: 'Success.' });
            }, err => next(err));
        });
    }, err => next(err));
}

function changeSiteCpc(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const campaign_id = req.query.campaign_id;
    const site = req.body.site;

    campaignService.changeSiteCpc(accessTaboola, site).then(response => {
        res.status(200).json(response);
    }, err => next(err));
}

function fraudStatsDaily(req, res, next) {
    let campaign_id = req.query.id;
    let start_date = dateFormat(req.query.start_date, 'isoDate');
    let end_date = dateFormat(req.query.end_date, 'isoDate');

    Campaign.findOne({ id: campaign_id }, (err, camp) => {
        if (err) return next(err);
        if (!_.isUndefined(camp.id_fraud)) {
            fraudService.statsCampaignFraud(camp.id_fraud, start_date, end_date).then(result => {
                res.status(200).json(result);
            }, err => next(err));
        } else {
            res.status(200).json([]);
        }
    });
}

function fraudTrackingParams(req, res, next) {
    let campaign_id = req.query.id;
    let start_date = dateFormat(req.query.start_date, 'isoDate');
    let end_date = dateFormat(req.query.end_date, 'isoDate');

    Campaign.findOne({ id: campaign_id }, (err, camp) => {
        if (err) return next(err);
        if (!_.isUndefined(camp.id_fraud)) {
            fraudService.trakingCampaignFraud(camp.id_fraud, start_date, end_date).then(result => {
                res.status(200).json(result);
            }, err => next(err));
        } else {
            res.status(200).json([]);
        }
    });
}

exports.get = get;
exports.list = list;
exports.update = update;
exports.changeStatus = changeStatus;
exports.getTotalsDetail = getTotalsDetail;
exports.changeBid = changeBid;
exports.changeSiteCpc = changeSiteCpc;
exports.fraudStatsDaily = fraudStatsDaily;
exports.fraudTrackingParams = fraudTrackingParams;