'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const Content = require('../../../models/content');
const contentService = require('../../../shared/services/taboola/content');

function list(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const accessVoluum = req.accessAPI.tracker;
    const campaign_id = req.query.campaign_id;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    contentService.contentSummary(accessTaboola, accessVoluum, campaign_id, start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function update(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    const data = {
        is_active: req.body.is_active
    };
    contentService.updateContent(accessTaboola, req.query.campaign_id, req.query.item_id, data).then(result => {
        Content.findOneAndUpdate({ id: req.query.item_id, campaign_id: req.query.campaign_id }, { is_active: req.body.is_active }, (err, response) => {
            if (err) return next(err);
            res.status(200).json({ success: true, message: 'Success! Please wait for update.' });
        });
    }, err => next(err));
}

function remove(req, res, next) {
    const accessTaboola = req.accessAPI.traffic;
    contentService.removeContent(accessTaboola, req.query.campaign_id, req.query.item_id).then(result => {
        Content.findOneAndRemove({ id: req.query.item_id, campaign_id: req.query.campaign_id }, (err, response) => {
            if (err) return next(err);
            res.status(200).json({ success: true, message: 'Success! Please wait for update.' });
        });
    }, err => next(err));
}

exports.list = list;
exports.update = update;
exports.remove = remove;