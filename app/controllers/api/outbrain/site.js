'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const Campaign = require('../../../models/campaign');
const campaignService = require('../../../shared/services/outbrain/campaign');
const siteService = require('../../../shared/services/outbrain/site');

function list(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const accessTracker = req.accessAPI.tracker;
    const campaign_id = req.query.campaign_id;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    const status = req.query.status;
    const cache = req.query.cache;

    siteService.sectionSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date, cache).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function blockSites(req, res, next) {
    campaignService.blockSitesCampaign(req.accessAPI.traffic, req.query.campaign_id, req.body.sites, req.userId).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function unBlockSites(req, res, next) {
    campaignService.unBlockSitesCampaign(req.accessAPI.traffic, req.query.campaign_id, req.body.sites).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function changeStatus(req, res, next) {
    siteService.changeSiteStatus(req.accessAPI.traffic, req.body.sites, req.query.status, req.userId).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

exports.list = list;
exports.blockSites = blockSites;
exports.unBlockSites = unBlockSites;
exports.changeStatus = changeStatus;