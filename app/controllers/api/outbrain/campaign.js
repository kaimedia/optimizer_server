'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const apiHelper = require('../../../helpers/api');
const Campaign = require('../../../models/campaign');
const campaignService = require('../../../shared/services/outbrain/campaign');
const siteService = require('../../../shared/services/site');
const fraudService = require('../../../shared/services/fraud');

function list(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const accessTracker = req.accessAPI.tracker;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    const cache = req.query.cache;

    campaignService.finalCampaignSummary(accessTraffic, accessTracker, start_date, end_date, cache).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function get(req, res, next) {
    const campaign_id = req.query.id;
    Campaign.findOne({ id: campaign_id }, (err, campaign) => {
        if (err) return next(err);
        if (!_.isEmpty(campaign)) {
            res.status(200).json(campaign);
        } else {
            campaignService.outbrainCampaignDetail(req.accessAPI.traffic, campaign_id).then(result => {
                res.status(200).json(result);
            }, err => next(err));
        }
    });
}

function changeStatus(req, res, next) {
    const campaign_id = req.body.id;
    const data = {
        enabled: req.body.is_active
    };

    campaignService.updateCampaignOutbrain(req.accessAPI.traffic, campaign_id, data).then(result => {
        res.status(200).send({ success: true, message: 'Success.' });
    }, err => next(err));
}

function changeBid(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const campaign_id = req.body.id;
    const cpc = req.body.cpc;
    const amount = req.body.spending_limit;

    if (cpc < 0.03)
        return res.status(400).send({ success: false, message: 'Please enter a CPC of at least $0.03' });

    campaignService.updateCampaignOutbrain(accessTraffic, campaign_id, { cpc: cpc }).then(response => {
        if (amount < 20)
            return res.status(400).send({ success: false, message: 'Please enter a Spending Limit of at least $20 per day.' });

        campaignService.updateBudget(accessTraffic, req.body.budget.id, { amount: amount }).then(response => {

            siteService.updateSitesConfiguredCpc(campaign_id, cpc).then(response => {
                res.status(200).send({ success: true, message: 'Success.' });
            }, err => next(err));

        }, err => next(err));
    }, err => next(err));
}

function update(req, res, next) {
    Campaign.findByIdAndUpdate(req.body._id, apiHelper.removeKey(req.body), { new: true }, (err, result) => {
        if (err) return next(err);
        res.status(200).json({ success: true });
    });
}

function getTotalsDetail(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const accessTracker = req.accessAPI.tracker;
    const campaign_id = req.query.id;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');

    campaignService.campaignItemTotals(accessTraffic, accessTracker, campaign_id, start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function fraudStatsDaily(req, res, next) {
    let campaign_id = req.query.id;
    let start_date = dateFormat(req.query.start_date, 'isoDate');
    let end_date = dateFormat(req.query.end_date, 'isoDate');

    Campaign.findOne({ id: campaign_id }, (err, camp) => {
        if (err) return next(err);
        if (!_.isUndefined(camp.id_fraud)) {
            fraudService.statsCampaignFraud(camp.id_fraud, start_date, end_date).then(result => {
                res.status(200).json(result);
            }, err => next(err));
        } else {
            res.status(200).json([]);
        }
    });
}

function changeSiteCpc(req, res, next) {
    campaignService.changeSiteCpc(req.accessAPI.traffic, req.body.site).then(response => {
        res.status(200).json(response);
    }, err => next(err));
}

exports.get = get;
exports.list = list;
exports.update = update;
exports.changeStatus = changeStatus;
exports.changeBid = changeBid;
exports.getTotalsDetail = getTotalsDetail;
exports.fraudStatsDaily = fraudStatsDaily;
exports.changeSiteCpc = changeSiteCpc;