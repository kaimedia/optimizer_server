'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const Content = require('../../../models/content');
const contentService = require('../../../shared/services/outbrain/content');

function list(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const accessTracker = req.accessAPI.tracker;
    const campaign_id = req.query.campaign_id;
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    const cache = req.query.cache;
    contentService.contentSummary(accessTraffic, accessTracker, campaign_id, start_date, end_date, cache).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function update(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const data = {
        enabled: req.body.is_active
    };
    contentService.updateContent(accessTraffic, req.query.item_id, data).then(result => {
        Content.findOneAndUpdate({ id: req.query.item_id, campaign_id: req.query.campaign_id }, { is_active: req.body.is_active }, (err, response) => {
            if (err) return next(err);
            res.status(200).json({ success: true, message: 'Success.' });
        });
    }, err => next(err));
}

function remove(req, res, next) {
    res.status(400).json({ success: false, message: 'Access denied to delete content.' });
}

exports.list = list;
exports.update = update;
exports.remove = remove;