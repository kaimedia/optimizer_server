'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const Tracker = require('../../models/tracker');
const apiHelper = require('../../helpers/api');
const fraudService = require('../../shared/services/fraud');
const trackerService = require('../../shared/services/voluum');

function getTrafficSources(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.trafficSourcesVoluum(accessKeys).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getAffiliateNetworks(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.affiliateNetworksVoluum(accessKeys).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getCampaignDetail(req, res, next) {
    const campaign_id = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.voluumCampaignDetail(accessKeys, campaign_id).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function updateCampaign(req, res, next) {
    const campaign_id = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        const payload = {
            namePostfix: req.body.namePostfix,
            trafficSource: req.body.trafficSource,
            country: req.body.country,
            costModel: req.body.costModel,
            redirectTarget: req.body.redirectTarget,
        };
        trackerService.updateCampaignVoluum(accessKeys, campaign_id, payload).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getLanders(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.landersVoluum(accessKeys).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function createLander(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.createLanderVoluum(accessKeys, req.body).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getLander(req, res, next) {
    const landerId = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.getLanderVoluum(accessKeys, landerId).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function updateLander(req, res, next) {
    const landerId = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.updateLanderVoluum(accessKeys, landerId, req.body).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getOffers(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.offersVoluum(accessKeys).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function getOffer(req, res, next) {
    const offerId = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.getOfferVoluum(accessKeys, offerId).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function createOffer(req, res, next) {
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.createOfferVoluum(accessKeys, req.body).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function updateOffer(req, res, next) {
    const offerId = req.query.id;
    apiHelper.getTrackerKeys(req).then(accessKeys => {
        trackerService.updateOfferVoluum(accessKeys, offerId, req.body).then(result => {
            if (result.error)
                return next(result.error);
            res.status(200).json(result);
        }, err => next(err));
    }, err => next(err));
}

function trackerView(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    trackerService.trackerSummary(req.accessAPI.tracker, req.query.tracker, start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

exports.getTrafficSources = getTrafficSources;
exports.getAffiliateNetworks = getAffiliateNetworks;
exports.getCampaignDetail = getCampaignDetail;
exports.updateCampaign = updateCampaign;
exports.getLanders = getLanders;
exports.getLander = getLander;
exports.createLander = createLander;
exports.updateLander = updateLander;
exports.getOffers = getOffers;
exports.getOffer = getOffer;
exports.createOffer = createOffer;
exports.updateOffer = updateOffer;
exports.trackerView = trackerView;