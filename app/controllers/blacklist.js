'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const Traffic = require('../models/traffic');
const Site = require('../models/site');
const siteService = require('../shared/services/site');
const taboolaCampaignService = require('../shared/services/taboola/campaign');
const outbrainCampaignService = require('../shared/services/outbrain/campaign');

function list(req, res, next) {
    siteService.getList(req.query.traffic_source, req.query.status).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function unBlock(req, res, next) {
    const sites = req.body.sites;
    const trafficType = sites[0].trafficSource;
    const campaign_id = sites[0].camp_id;

    Traffic.findOne({ type: trafficType }, (err, traffic) => {
        if (err) return next(err);
        let accessKeys;
        switch (trafficType) {
            case 'Taboola':
                accessKeys = _.pick(traffic, ['type', 'account_id', 'client_id', 'client_secret']);
                taboolaCampaignService.unBlockSitesCampaign(accessKeys, campaign_id, sites).then(response => {
                    res.status(200).send(response);
                }, err => next(err));
                break;

            case 'Outbrain':
                accessKeys = _.pick(traffic, ['type', 'account_id', 'email', 'password']);
                outbrainCampaignService.unBlockSitesCampaign(accessKeys, campaign_id, sites).then(response => {
                    res.status(200).send(response);
                }, err => next(err));
                break;
        };
    });
}

exports.list = list;
exports.unBlock = unBlock;