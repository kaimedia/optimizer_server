'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const siteService = require('../shared/services/site');

function changeAutoCpc(req, res, next) {
    const accessTraffic = req.accessAPI.traffic;
    const site = req.body;
    const status = req.query.status;
    siteService.updateAutoChangeCpcStatus(accessTraffic.type, site, status).then(response => {
        res.status(200).send({ success: true });
    }, err => next(err));
}

function blockedSites(req, res, next) {
    siteService.blockedSites(req.query.traffic_source).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function updateStats(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    siteService.updateStats(req.query.campaign_id, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function siteMetrics(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    siteService.getSiteMetrics(req.query.campaign_id, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

exports.changeAutoCpc = changeAutoCpc;
exports.blockedSites = blockedSites;
exports.updateStats = updateStats;
exports.siteMetrics = siteMetrics;