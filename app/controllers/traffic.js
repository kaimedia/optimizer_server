'use strict';

const logger = require('../lib/logger');
const Traffic = require('../models/traffic');

function create(req, res) {
    const traffic_source = Traffic(req.body);
    traffic_source.save(err => {
        if (err)
            return res.status(500).send({ message: 'Error create traffic source.' });

        res.status(200).json(traffic_source);
    });
}

function get(req, res) {
    Traffic.findById(req.params.id, (err, traffic_source) => {
        if (err)
            return res.status(500).send({ message: 'Error get the traffic source.' });

        res.status(200).json(new Buffer(JSON.stringify(traffic_source)).toString('base64'));
    });
}

function all(req, res) {
    Traffic.find({}, (err, traffic_sources) => {
        if (err)
            return res.status(500).send({ message: 'Error get all traffic source.' });

        res.status(200).json(new Buffer(JSON.stringify(traffic_sources)).toString('base64'));
    });
}

function update(req, res) {
    const id = req.params.id;
    Traffic.findByIdAndUpdate(id, req.body, (err, results) => {
        if (err)
            return res.status(500).send({ message: 'Error find traffic source and update.' });

        let traffic_source = Traffic.findById(id, (err, traffic_source) => {
            if (err)
                return res.status(500).send({ message: 'Error find traffic source by ID.' });

            res.status(200).json(traffic_source);
        });
    });
}

function remove(req, res) {
    Traffic.findByIdAndRemove(req.params.id, (err, traffic_source) => {
        if (err)
            return res.status(500).send({ message: 'Error find traffic source by Id and remove.' });

        Traffic.find({}, (err, traffic_sources) => {
            if (err)
                return res.status(500).send({ message: 'Error find all traffic source.' });

            res.status(200).json(traffic_sources);
        });
    });
}

exports.create = create;
exports.get = get;
exports.all = all;
exports.update = update;
exports.remove = remove;

