'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const Tracker = require('../models/tracker');
const Campaign = require('../models/campaign');
const voluumService = require('../shared/services/voluum');
const campaignService = require('../shared/services/campaign');
const fraudService = require('../shared/services/fraud');

function all(req, res, next) {
    Campaign.find({}, (err, campaigns) => {
        if (err) return next(err);
        res.status(200).json(_.filter(campaigns, o => o.archived === 0));
    });
}

function detail(req, res, next) {
    Campaign.findById(req.query.campaign_id, (err, campaign) => {
        if (err) return next(err);
        res.status(200).json(campaign);
    })
}

function archiveList(req, res, next) {
    Campaign.find({}, (err, campaigns) => {
        if (err) return next(err);
        res.status(200).json(_.filter(campaigns, o => o.archived === 1));
    });
}

function allByStatus(req, res, next) {
    Campaign.find({}, (err, campaigns) => {
        if (err) return next(err);
        res.status(200).json(_.filter(campaigns, o => o.status === req.query.status));
    });
}

function create(req, res, next) {
    Tracker.findById(req.body.tracker, (err, tracker) => {
        if (err) return next(err);
        const trackerKeys = {
            access_id: tracker.access_id,
            access_key: tracker.access_key
        };
        voluumService.createCampaignVoluum(trackerKeys, {
            namePostfix: req.body.namePostfix,
            trafficSource: req.body.trafficSource,
            country: req.body.country,
            costModel: req.body.costModel,
            redirectTarget: req.body.redirectTarget,
        }).then(campaignTracker => {
            fraudService.campaignFraudInit(req.body.type, trackerKeys, campaignTracker).then(fields => {
                fraudService.createCampaignFraud(fields).then(campaignFraud => {
                    Campaign.create({
                        name: campaignTracker.name,
                        campaignUrl: campaignTracker.url,
                        tracker_id: campaignTracker.id,
                        id_fraud: campaignFraud.clid,
                        type: req.body.type,
                        countryCode: req.body.country.code,
                        trafficSourceAccount: req.body.traffic,
                        trackerAccount: req.body.tracker,
                        payout: req.body.payout,
                        blockSites: req.body.blockSites,
                        status: "UNDER REVIEW"
                    }, (err, campaign) => {
                        if (err) return next(err);
                        res.status(200).json(campaign);
                    });
                }, err => next(err));
            }, err => next(err));
        }, err => next(err));
    });
}

function update(req, res, next) {
    Tracker.findById(req.body.tracker, (err, tracker) => {
        if (err) return next(err);
        const trackerKeys = {
            access_id: tracker.access_id,
            access_key: tracker.access_key
        };
        voluumService.updateCampaignVoluum(trackerKeys, req.body.id, {
            namePostfix: req.body.namePostfix,
            trafficSource: req.body.trafficSource,
            country: req.body.country,
            costModel: req.body.costModel,
            redirectTarget: req.body.redirectTarget,
        }).then(campaignTracker => {
            Campaign.findByIdAndUpdate(req.body._id, {
                name: campaignTracker.name,
                campaignUrl: campaignTracker.url,
                type: req.body.type,
                countryCode: req.body.country.code,
                trafficSourceAccount: req.body.traffic,
                trackerAccount: req.body.tracker,
                payout: req.body.payout,
                blockSites: req.body.blockSites
            }, (err, campaign) => {
                if (err) return next(err);
                res.status(200).json(campaign);
            }, err => next(err));
        }, err => next(err));
    });
}

function archive(req, res, next) {
    Campaign.findByIdAndUpdate(req.query.id, { archived: 1 }, (err, response) => {
        res.status(200).json({ success: true, message: 'Success.' });
    });
}

function unArchive(req, res, next) {
    Campaign.findByIdAndUpdate(req.query.id, { archived: 0 }, (err, response) => {
        res.status(200).json({ success: true, message: 'Success.' });
    });
}

function updateStats(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    campaignService.updateStats(req.query.campaign_id, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function campaignMetrics(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    campaignService.getCampaignsMetrics(req.query.type, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

exports.all = all;
exports.detail = detail;
exports.archiveList = archiveList;
exports.allByStatus = allByStatus;
exports.create = create;
exports.update = update;
exports.archive = archive;
exports.unArchive = unArchive;
exports.updateStats = updateStats;
exports.campaignMetrics = campaignMetrics;
