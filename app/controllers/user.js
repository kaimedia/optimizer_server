'use strict';

const logger = require('../lib/logger');
const bcrypt = require('bcryptjs');
const User = require('../models/user');

function create(req, res) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    User.create({
        email: req.body.email,
        password: hashedPassword,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        country: req.body.country,
        role: req.body.role
    }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error create user.' });

        res.status(200).send(user);
    });
}

function all(req, res) {
    User.find({ active: true }, (err, users) => {
        if (err)
            return res.status(500).send({ message: 'Error get all users.' });

        res.status(200).send(users);
    });
}

function get(req, res) {
    User.findOne({ _id: req.params.id, active: true }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error find user by Id.' });

        if (!user)
            return res.status(404).send({ message: "No user found." });

        res.status(200).send(user);
    });
}

function remove(req, res) {
    User.findByIdAndRemove(req.params.id, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error find user by Id and remove.' });

        res.status(200).send({ message: "User: " + user.email + " was deleted." });
    });
}

function update(req, res) {
    User.findByIdAndUpdate(req.body._id, req.body, { new: true }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error find user by Id and update.' });

        res.status(200).send({ message: "Updated user successfully" });
    });
}

function password(req, res) {
    if (req.body.newPassword !== req.body.passwordConfirmation)
        return res.status(500).send({ message: "New password not equal password confirmation" });

    const newPassword = bcrypt.hashSync(req.body.newPassword, 8);

    User.findByIdAndUpdate(req.body._id, { password: newPassword }, { new: true }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error find user by Id and update.' });

        res.status(200).send({ message: "Change password successfully" });
    });
}

exports.create = create;
exports.all = all;
exports.get = get;
exports.remove = remove;
exports.update = update;
exports.password = password;