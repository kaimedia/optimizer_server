'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const contentService = require('../shared/services/content');

function updateStats(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    contentService.updateStats(req.query.campaign_id, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

function contentMetrics(req, res, next) {
    const start_date = dateFormat(req.query.start_date, 'isoDate');
    const end_date = dateFormat(req.query.end_date, 'isoDate');
    contentService.getContentMetrics(req.query.campaign_id, start_date, end_date).then(response => {
        res.status(200).send(response);
    }, err => next(err));
}

exports.updateStats = updateStats;
exports.contentMetrics = contentMetrics;