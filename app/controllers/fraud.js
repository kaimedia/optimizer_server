'use strict';

const _ = require('lodash');
const dateFormat = require('dateformat');
const logger = require('../lib/logger');
const moment = require('moment');
const config = require('config');
const Campaign = require('../models/campaign');
const Rule = require('../models/rule');
const fraudService = require('../shared/services/fraud');

const outbrainSiteService = require('../shared/services/outbrain/site');
const ruleHelper = require('../rules/index');

function list(req, res, next) {
    fraudService.listCampaignFraud().then(results => {
        res.status(200).json(results);
    }, err => next(err));
}

function get(req, res, next) {
    let campaign_id = req.query.campaign_id;
    fraudService.getCampaignFraud(campaign_id).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function create(req, res, next) {
    fraudService.createCampaignFraud(req.body).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function archive(req, res, next) {
    let campaign_id = req.query.campaign_id;

    fraudService.archiveCampaignFraud(campaign_id).then(result => {
        res.status(200).end();
    }, err => next(err));
}

function statsCampaigns(req, res, next) {
    let start_date = dateFormat(req.query.start_date, 'isoDate');
    let end_date = dateFormat(req.query.end_date, 'isoDate');
    let min_date = dateFormat(moment(end_date).subtract(6, 'days'), 'isoDate');
    let inRangeDate = moment(start_date).isBetween(min_date, end_date, 'days', true);

    if (inRangeDate)
        start_date = min_date;

    fraudService.statsCampaignsFraud(start_date, end_date).then(result => {
        res.status(200).json(result);
    }, err => next(err));
}

function test(req, res, next) {
    Rule.find({ status: true }, (err, rules) => {
        if (err) return next(err);
        if (_.size(rules) === 0)
            return logger.warn('Rules empty.');

        _.forEach(rules, rule => {
            ruleHelper.ruleAction(rule);
        });
    });
}

exports.test = test;
exports.list = list;
exports.get = get;
exports.create = create;
exports.archive = archive;
exports.statsCampaigns = statsCampaigns;