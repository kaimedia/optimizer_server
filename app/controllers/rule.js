'use strict';

const _ = require('lodash');
const logger = require('../lib/logger');
const Rule = require('../models/rule');
const Job = require('../schedules/rule');

function create(req, res, next) {
    const rule = Rule(req.body);
    rule.save(err => {
        if (err) return next(err);

        res.status(200).json(rule);
        Job.startRule(rule);
    });
}

function get(req, res, next) {
    Rule.findById(req.params.id, (err, rule) => {
        if (err) return next(err);

        res.status(200).json(rule);
    });
}

function all(req, res, next) {
    Rule.find({}, (err, rules) => {
        if (err) return next(err);

        res.status(200).json(rules);
    });
}

function update(req, res, next) {
    const id = req.params.id;
    Rule.findByIdAndUpdate(id, req.body, (err, results) => {
        if (err) return next(err);

        let rule = Rule.findById(id, (err, rule) => {
            if (err) return next(err);

            res.status(200).json(rule);
            Job.startRule(rule);
        });
    });
}

function remove(req, res, next) {
    Rule.findByIdAndRemove(req.params.id, (err, rule) => {
        if (err) return next(err);

        Job.startRule(rule, false);
        Rule.find({}, (err, rules) => {
            if (err) return next(err);

            res.status(200).json(rules);
        });
    });
}

exports.create = create;
exports.get = get;
exports.all = all;
exports.update = update;
exports.remove = remove;