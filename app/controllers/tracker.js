'use strict';

const logger = require('../lib/logger');
const Tracker = require('../models/tracker');

function create(req, res) {
    const tracker = Tracker(req.body);
    tracker.save(err => {
        if (err)
            return res.status(500).send({ message: 'Error create tracker.' });

        res.status(200).json(tracker);
    });
}

function get(req, res) {
    Tracker.findById(req.params.id, (err, tracker) => {
        if (err)
            return res.status(500).send({ message: 'Error get the tracker.' });

        res.status(200).json(new Buffer(JSON.stringify(tracker)).toString('base64'));
    });
}

function all(req, res) {
    Tracker.find({}, (err, trackers) => {
        if (err)
            return res.status(500).send({ message: 'Error get all trackers.' });

        res.status(200).json(new Buffer(JSON.stringify(trackers)).toString('base64'));
    });
}

function update(req, res) {
    const id = req.params.id;
    Tracker.findByIdAndUpdate(id, req.body, (err, results) => {
        if (err)
            return res.status(500).send({ message: 'Error find tracker and update.' });

        let tracker = Tracker.findById(id, (err, tracker) => {
            if (err)
                return res.status(500).send({ message: 'Error find tracker by ID.' });

            res.status(200).json(tracker);
        });
    });
}

function remove(req, res) {
    Tracker.findByIdAndRemove(req.params.id, (err, tracker) => {
        if (err)
            return res.status(500).send({ message: 'Error find tracker by Id and remove.' });

        Tracker.find({}, (err, trackers) => {
            if (err)
                return res.status(500).send({ message: 'Error find all trackers.' });

            res.status(200).json(trackers);
        });
    });
}

exports.create = create;
exports.get = get;
exports.all = all;
exports.update = update;
exports.remove = remove;

