'use strict';

const _ = require('lodash');
const jwt = require('jsonwebtoken');
const secret = require('../../config/index').secret;

function required(req, res, next) {

    var token = _.split(req.headers.authorization, " ", 2)[1];
    if (!token)
        return res.status(401).send({ success: false, message: 'Authentication is required.' });

    jwt.verify(token, secret, function (err, decoded) {
        if (err)
            return res.status(401).send({ success: false, message: 'Unauthorized' });

        req.userId = decoded.id;
        next();
    });

}

exports.required = required;