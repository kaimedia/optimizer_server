'use strict';

const _ = require('lodash');
const logger = require('../lib/logger');
const Traffic = require('../models/traffic');
const Tracker = require('../models/tracker');

function required(req, res, next) {
    const trafficSourceId = req.headers.traffic;
    const trackerId = req.headers.tracker;

    let accessAPI = {
        traffic: {},
        tracker: {},
    };

    Traffic.findById(trafficSourceId, (err, traffic_source) => {
        if (err) return res.status(500).send({ success: false, message: err.message });

        if (_.isEmpty(traffic_source))
            return res.status(500).send({ success: false, message: `Traffic source ID not found.` });

        switch (traffic_source.type) {
            case 'Taboola':
                accessAPI.traffic = {
                    type: traffic_source.type,
                    account_id: traffic_source.account_id,
                    client_id: traffic_source.client_id,
                    client_secret: traffic_source.client_secret
                };
                break;

            case 'Outbrain':
                accessAPI.traffic = {
                    type: traffic_source.type,
                    account_id: traffic_source.account_id,
                    email: traffic_source.email,
                    password: traffic_source.password
                };
                break;
        };

        Tracker.findById(trackerId, (err, tracker) => {
            if (err) return res.status(500).send({ success: false, message: err.message });

            if (_.isEmpty(tracker))
                return res.status(500).send({ success: false, message: `Tracker ID not found.` });

            accessAPI.tracker = {
                access_id: tracker.access_id,
                access_key: tracker.access_key
            };

            req.body.accessAPI = JSON.stringify(accessAPI);
            req.accessAPI = accessAPI;
            next();
        });

    });
}

exports.required = required;