'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const list = {
    query: {
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const allByStatus = {
    query: {
        status: Joi.string().required(),
    }
};

const get = {
    query: {
        id: Joi.string().required(),
    }
};

const archive = {
    query: {
        id: Joi.string().required(),
    }
};

const unArchive = {
    query: {
        id: Joi.string().required(),
    }
};

const changeStatus = {
    body: {
        id: Joi.string().required(),
        is_active: Joi.boolean().required(),
    }
};

const getTotals = {
    query: {
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const getTotalsDetail = {
    query: {
        id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const changeBid = {
    body: {
        id: Joi.string().required(),
        cpc: Joi.number().required(),
        spending_limit: Joi.number().required(),
    }
};

const publisherBidModifier = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const fraudStatsDaily = {
    query: {
        id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const fraudTrackingParams = {
    query: {
        id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const updateStats = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const campaignMetrics = {
    query: {
        type: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

exports.get = get;
exports.list = list;
exports.archive = archive;
exports.unArchive = unArchive;
exports.allByStatus = allByStatus;
exports.changeStatus = changeStatus;
exports.getTotals = getTotals;
exports.getTotalsDetail = getTotalsDetail;
exports.changeBid = changeBid;
exports.publisherBidModifier = publisherBidModifier;
exports.fraudStatsDaily = fraudStatsDaily;
exports.fraudTrackingParams = fraudTrackingParams;
exports.updateStats = updateStats;
exports.campaignMetrics = campaignMetrics;
