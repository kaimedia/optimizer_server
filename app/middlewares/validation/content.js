'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const list = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const update = {
    query: {
        campaign_id: Joi.string().required(),
        item_id: Joi.string().required(),
    }
};

const remove = {
    query: {
        campaign_id: Joi.string().required(),
        item_id: Joi.string().required(),
    }
};

const updateStats = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const contentMetrics = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

exports.list = list;
exports.update = update;
exports.remove = remove;
exports.updateStats = updateStats;
exports.contentMetrics = contentMetrics;