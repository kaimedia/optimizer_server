'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const registerAuth = {
    body: {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        email: Joi.string().required().regex(Regex.email, 'Email'),
        password: Joi.string().required().regex(Regex.password, 'Password'),
    }
};

module.exports = {
    registerAuth,
};