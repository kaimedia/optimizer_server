'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const createRule = {
    body: {
        name: Joi.string().required(),
        action: Joi.number().positive().required(),
        interval: Joi.string().required(),
        rotation: Joi.string().required(),
        level: Joi.number().positive().required(),
        campaigns: Joi.array().required(),
        criterias: Joi.array().required(),
        status: Joi.boolean().required(),
        created_at: Joi.string().required(),
    }
};

const getRule = {
    params: {
        id: Joi.string().required(),
    }
};

const updateRule = {
    params: {
        id: Joi.string().required(),
    }
};

const deleteRule = {
    params: {
        id: Joi.string().required(),
    }
};

module.exports = {
    createRule,
    getRule,
    updateRule,
    deleteRule,
};