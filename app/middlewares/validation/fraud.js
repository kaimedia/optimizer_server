'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');


const getCampaign = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const createCampaign = {
    body: {
        info: Joi.string().required(),
    }
};

const archiveCampaign = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const statsCampaigns = {
    query: {
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

exports.createCampaign = createCampaign;
exports.getCampaign = getCampaign;
exports.archiveCampaign = archiveCampaign;
exports.statsCampaigns = statsCampaigns;