'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const list = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const blockSites = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const unBlockSites = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const changeStatus = {
    query: {
        status: Joi.string().required(),
    }
};

const getStatus = {
    query: {
        status: Joi.string().required(),
    }
};

const changeAutoCpc = {
    query: {
        status: Joi.string().required(),
    }
};

const updateStats = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const siteMetrics = {
    query: {
        campaign_id: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

exports.list = list;
exports.blockSites = blockSites;
exports.unBlockSites = unBlockSites;
exports.changeStatus = changeStatus;
exports.getStatus = getStatus;
exports.changeAutoCpc = changeAutoCpc;
exports.updateStats = updateStats;
exports.siteMetrics = siteMetrics;