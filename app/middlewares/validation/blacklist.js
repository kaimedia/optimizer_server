'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const list = {
    query: {
        traffic_source: Joi.string().required(),
        status: Joi.string().required(),
    }
};

exports.list = list;