'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const create = {
    body: {
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        email: Joi.string().required().regex(Regex.email, 'Email'),
        password: Joi.string().required().regex(Regex.password, 'Password'),
    }
};

const get = {
    params: {
        id: Joi.string().required(),
    }
};

const remove = {
    params: {
        id: Joi.string().required(),
    }
};

const update = {
    body: {
        _id: Joi.string().required(),
    }
};

const changePassword = {
    body: {
        _id: Joi.string().required(),
        newPassword: Joi.string().required().regex(Regex.password, 'Password'),
        passwordConfirmation: Joi.string().required().regex(Regex.password, 'Password'),
    }
};

module.exports = {
    create,
    get,
    remove,
    update,
    changePassword,
};