'use strict';

const Joi = require('joi');
const Regex = require('../../../helpers/regex');

const getCampaign = {
    query: {
        id: Joi.string().required(),
    }
};

const createCampaign = {
    query: {
        campaign_id: Joi.string().required(),
    }
};

const updateCampaign = {
    query: {
        id: Joi.string().required(),
    }
};

const getLander = {
    query: {
        id: Joi.string().required(),
    }
};

const updateLander = {
    query: {
        id: Joi.string().required(),
    }
};

const getOffer = {
    query: {
        id: Joi.string().required(),
    }
};

const updateOffer = {
    query: {
        id: Joi.string().required(),
    }
};

const trackerView = {
    query: {
        tracker: Joi.string().required(),
        start_date: Joi.date().iso().required(),
        end_date: Joi.date().iso().min(Joi.ref('start_date')).required(),
    }
};

const trafficSources = {
    query: {
        tracker_id: Joi.string().required(),
    }
};

const landersByTracker = {
    query: {
        tracker_id: Joi.string().required(),
    }
};

const offersByTracker = {
    query: {
        tracker_id: Joi.string().required(),
    }
};

exports.getCampaign = getCampaign;
exports.createCampaign = createCampaign;
exports.updateCampaign = updateCampaign;
exports.getLander = getLander;
exports.updateLander = updateLander;
exports.getOffer = getOffer;
exports.updateOffer = updateOffer;
exports.trackerView = trackerView;