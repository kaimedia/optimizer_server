'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const createTracker = {
    body: {
        name: Joi.string().required(),
        type: Joi.number().integer().required(),
        access_id: Joi.string().required(),
        access_key: Joi.string().required(),
        status: Joi.boolean().required(),
        created_at: Joi.string().required(),
    }
}

const getTracker = {
    params: {
        id: Joi.string().required(),
    }
};

const updateTracker = {
    params: {
        id: Joi.string().required(),
    }
};

const removeTracker = {
    params: {
        id: Joi.string().required(),
    }
};

module.exports = {
    createTracker,
    getTracker,
    updateTracker,
    removeTracker,
};