'use strict';

const Joi = require('joi');
const Regex = require('../../helpers/regex');

const create = {
    body: {
        name: Joi.string().required(),
        type: Joi.string().required(),
        trackers: Joi.array().required(),
        status: Joi.boolean().required(),
        created_at: Joi.string().required(),
    }
};

const get = {
    params: {
        id: Joi.string().required(),
    }
};

const update = {
    params: {
        id: Joi.string().required(),
    }
};

const remove = {
    params: {
        id: Joi.string().required(),
    }
};

module.exports = {
    create,
    get,
    update,
    remove,
};