'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const logger = require('../../lib/logger');
const ruleHelper = require('../index');
const Traffic = require('../../models/traffic');
const Tracker = require('../../models/tracker');
const Campaign = require('../../models/campaign');
const SiteMetrics = require('../../models/site.metrics');
const SiteMetricsRule = require('../../models/site.metrics.rules');
const siteService = require('../../shared/services/site');
const taboolaCampaignService = require('../../shared/services/taboola/campaign');
const outbrainCampaignService = require('../../shared/services/outbrain/campaign');
const taboolaSiteService = require('../../shared/services/taboola/site');
const outbrainSiteService = require('../../shared/services/outbrain/site');

const metricFields = ['site_id', 'site_name', 'boosted_cpc', 'configured_cpc', 'cpc_boost', 'cost', 'ctr', 'cpm', 'cpc', 'auto_update_cpc'];

/**
 * Block Sites Rule
 */
function blockOnAllCampaigns(rule) {
    return new Promise(function (resolve, reject) {
        Campaign.find({ archived: 0 }, (err, camps) => {
            if (err || _.isEmpty(camps))
                return;

            const campaignList = _.filter(camps, o => o.status === 'RUNNING' && o.id && o.trafficSourceAccount && o.trackerAccount && o.archived == 0);
            if (_.isEmpty(campaignList))
                return;

            let timeout = 0;
            let promises = [];
            _.forEach(campaignList, camp => {
                promises.push(blockOnCampaign(rule, camp._id, timeout));
                timeout = timeout + 5000;
            });
            Promise.all(promises).then(results => {
                resolve(results);
            }).catch(err => reject(err));
        });
    });
}

function blockOnCampaign(rule, campaign_id, timeout) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            Campaign.findById(campaign_id, (err, camp) => {
                if (err || camp.status !== 'RUNNING' || !camp.id || !camp.trafficSourceAccount || !camp.trackerAccount || camp.archived !== 0)
                    return;

                accountDetails(camp).then(results => {
                    siteMetricsByCampaign(camp.id, rule.interval).then(siteList => {
                        let accessTraffic;
                        switch (camp.type) {
                            case 'Taboola':
                                accessTraffic = _.pick(results[0], ['type', 'account_id', 'client_id', 'client_secret']);
                                taboolaBlockSites(accessTraffic, siteList, camp.id, rule).then(res => {
                                    resolve(res);
                                }, err => logger.error(err));
                                break;

                            case 'Outbrain':
                                accessTraffic = _.pick(results[0], ['type', 'account_id', 'email', 'password']);
                                outbrainBlockSites(accessTraffic, siteList, camp.id, rule).then(res => {
                                    resolve(res);
                                }, err => logger.error(err));
                                break;
                        }
                    }, err => logger.error(err));
                });
            });
        }, timeout);
    });
}

function taboolaBlockSites(accessKeys, siteList, campaign_id, rule) {
    return new Promise(function (resolve, reject) {
        const validatedSites = ruleHelper.validateCriteriasData(siteList.sites, rule.criterias);
        if (_.isEmpty(validatedSites)) return logger.warn('No Taboola validated site in rule: [%s]', rule.name);

        const sites = addActionType(validatedSites);
        taboolaCampaignService.blockSitesCampaign(accessKeys, campaign_id, sites).then(res => {
            const siteNames = _.join(getSiteFields(sites, 'site_name'), ",");
            const alertMsg = `Taboola Campaign [${campaign_id}] blocked sites: <code>${siteNames}</code>`;
            if (rule.action === 3) {
                taboolaSiteService.changeSiteStatus(accessKeys, sites, 'blacklist').then(res => {
                    resolve({ alertMsg: alertMsg });
                }, err => logger.error(err));
            } else {
                resolve({ alertMsg: alertMsg });
            }
        }, err => logger.error(err));
    });
}

function outbrainBlockSites(accessKeys, siteList, campaign_id, rule) {
    return new Promise(function (resolve, reject) {
        const validatedSites = ruleHelper.validateCriteriasData(siteList.sites, rule.criterias);
        if (_.isEmpty(validatedSites)) return logger.warn('No Outbrain validated site in rule: [%s]', rule.name);

        const sites = addActionType(validatedSites);
        outbrainCampaignService.blockSitesCampaign(accessKeys, campaign_id, sites).then(res => {
            const siteNames = _.join(getSiteFields(sites, 'site_name'), ",");
            const alertMsg = `Outbrain Campaign [${campaign_id}] blocked sites: <code>${siteNames}</code>`;
            if (rule.action === 3) {
                outbrainSiteService.changeSiteStatus(accessKeys, sites, 'blacklist').then(res => {
                    resolve({ alertMsg: alertMsg });
                }, err => logger.error(err));
            } else {
                resolve({ alertMsg: alertMsg });
            }
        }, err => logger.error(err));
    });
}

function getSiteFields(siteList, field) {
    siteList = _.map(siteList, o => o[field]);
    return siteList;
}

function addActionType(sites) {
    if (_.size(sites) > 0) {
        sites = _.map(sites, item => {
            item.action = 'AUTO';
            return item;
        });
    }
    return sites;
}

/**
 *  Change Site CPC Bid Rule
 */
function changeSitesCpcOnAllCampaigns(rule) {
    return new Promise(function (resolve, reject) {
        Campaign.find({ archived: 0 }, (err, camps) => {
            if (err || _.isEmpty(camps)) return;

            const campaignList = _.filter(camps, o => o.status === 'RUNNING' && o.id && o.trafficSourceAccount && o.trackerAccount && o.archived === 0);
            if (_.isEmpty(campaignList)) return;

            let timeout = 0;
            let promises = [];
            _.forEach(campaignList, camp => {
                promises.push(changeSitesCpcOnCampaign(rule, camp, timeout));
                timeout = timeout + 10000;
            });
            Promise.all(promises).then(res => {
                resolve(res);
            }).catch(err => reject(err));
        });
    });
}

function changeSitesCpcOnCampaign(rule, campaign, timeout) {
    return new Promise(function (resolve, reject) {
        let wait = setTimeout(() => {
            clearTimeout(wait);
            if (_.isObject(campaign)) {
                getAccessDetailsByCampaign(campaign, rule).then(res => {
                    resolve(res);
                }, err => logger.error(err));
            } else {
                Campaign.findById(campaign, (err, camp) => {
                    if (err) return;
                    getAccessDetailsByCampaign(camp, rule).then(res => {
                        resolve(res);
                    }, err => logger.error(err));
                });
            }
        }, timeout);
    });
}

function getAccessDetailsByCampaign(campaign, rule) {
    return new Promise(function (resolve, reject) {
        if (campaign.status !== 'RUNNING' || !campaign.id || !campaign.trafficSourceAccount || !campaign.trackerAccount || campaign.archived != 0)
            return;

        accountDetails(campaign).then(results => {
            let accessTraffic;
            if (campaign.type === 'Taboola') {
                accessTraffic = _.pick(results[0], ['type', 'account_id', 'client_id', 'client_secret']);
            }
            else if (campaign.type === 'Outbrain') {
                accessTraffic = _.pick(results[0], ['type', 'account_id', 'email', 'password']);
            }

            siteMetricsByCampaign(campaign.id, rule.interval).then(new_stats => {
                checkSiteMetricsRule(accessTraffic, campaign, new_stats, rule).then(res => {
                    resolve(res);
                }, err => logger.error(err));
            }, err => logger.error(err));
        }, err => logger.error(err));
    });
}

function checkSiteMetricsRule(accessKeys, campaign, new_stats, rule) {
    return new Promise(function (resolve, reject) {
        checkSiteMetrics(accessKeys.type, campaign).then(old_stats => {
            if (!old_stats || _.isEmpty(old_stats)) {
                saveSiteMetricsToDatabase(accessKeys.type, campaign, new_stats).then(res => {
                    resolve(res);
                }, err => logger.error(err));
            } else {
                compareSiteMetrics(accessKeys, campaign, old_stats, new_stats, rule).then(res => {
                    resolve(res);
                }, err => logger.error(err));
            }
        }, err => logger.error(err));
    });
}

function compareSiteMetrics(accessKeys, campaign, oldData, newData, rule) {
    return new Promise(function (resolve, reject) {
        let siteList = [];

        let newResults = _.map(newData.sites, newSite => {
            const oldSite = _.find(oldData.results, ['site_id', newSite.site_id]);

            if (!oldSite || _.isEmpty(oldSite))
                return newSite;

            if (newSite.auto_update_cpc && newSite.auto_update_cpc === false) {
                return newSite;
            }

            if (checkCpcCondition(rule.decrease_cpc[0], oldSite, newSite, true) && checkCpcCondition(rule.decrease_cpc[1], oldSite, newSite)) {
                newSite.boosted_cpc = _.round((1 * newSite.cpc) / newSite.cpm, 3);
                newSite.cpc_boost = _.round((newSite.boosted_cpc - newSite.configured_cpc) / newSite.configured_cpc, 2);
                siteList.push(newSite);
                return newSite;
            } else {
                return oldSite;
            }
        });

        let promises = [
            updateCurrentMetrics(oldData._id, newData, newResults)
        ];

        if (!_.isEmpty(siteList)) {
            promises.push(updateCpcBid(accessKeys, campaign, siteList));
        }

        Promise.all(promises).then(response => {
            resolve(response);
        }).catch(err => logger.error(err));
    });
}

function checkCpcCondition(criteria, oldObj, newObj, range = false) {
    let isTrue = 0;
    let columnValue = newObj[criteria.column];

    if (range) {
        columnValue = newObj[criteria.column] - oldObj[criteria.column];
    }

    switch (criteria.operator) {
        case 'greater':
            if (columnValue >= criteria.value) {
                isTrue = 1;
            }
            break;

        case 'less':
            if (columnValue < criteria.value) {
                isTrue = 1;
            }
            break;
    }
    return (isTrue === 1) ? true : false;
}

function updateCpcBid(accessKeys, campaign, siteList) {
    return new Promise(function (resolve, reject) {
        if (_.isEmpty(siteList)) return;

        switch (accessKeys.type) {
            case 'Taboola':
                updateTaboolaSiteBid(accessKeys, campaign, siteList).then(res => {
                    resolve(res);
                }, err => logger.error(err));
                break;

            case 'Outbrain':
                updateOutbrainSiteBid(accessKeys, campaign, siteList).then(res => {
                    resolve(res);
                }, err => logger.error(err));
                break;
        }
    });
}

function updateTaboolaSiteBid(accessKeys, campaign, siteList) {
    return new Promise(function (resolve, reject) {
        siteList = _.filter(siteList, o => {
            const cpc_boost = _.round(1 + o.cpc_boost, 2);
            return cpc_boost >= 0.5 && cpc_boost <= 1.5;
        });

        if (_.isEmpty(siteList)) return;

        taboolaCampaignService.taboolaCampaignDetail(accessKeys, campaign.id, ['publisher_bid_modifier']).then(camp => {
            let modified_sites = camp.publisher_bid_modifier !== null ? camp.publisher_bid_modifier.values : [];

            siteList = _.map(siteList, o => {
                return {
                    target: o.site_id,
                    cpc_modification: _.round(1 + o.cpc_boost, 2),
                };
            });

            const payload = {
                publisher_bid_modifier: {
                    values: _.unionBy(siteList, modified_sites, 'target')
                }
            };

            taboolaCampaignService.updateCampaignTaboola(accessKeys, campaign.id, payload).then(response => {
                Campaign.findOneAndUpdate({ id: campaign.id }, payload, (err, result) => {
                    if (err) return logger.error(err);
                    resolve({
                        success: true,
                        message: 'Success.',
                    });
                });
            }, err => logger.error(err));

        }, err => logger.error(err));
    });
}

function updateOutbrainSiteBid(accessKeys, campaign, siteList) {
    return new Promise(function (resolve, reject) {
        outbrainCampaignService.outbrainCampaignDetail(accessKeys, campaign.id).then(camp => {
            let modified_sites = camp.bids ? camp.bids.bySection : [];

            siteList = _.map(siteList, e => {
                if (e.boosted_cpc < camp.minimumCpc) {
                    e.boosted_cpc = camp.minimumCpc;
                    e.cpc_boost = _.round((e.boosted_cpc - e.configured_cpc) / e.configured_cpc, 2);
                }
                return e;
            });
            if (_.isEmpty(siteList)) return;

            siteList = _.map(siteList, e => {
                return {
                    sectionId: e.site_id,
                    cpcAdjustment: e.cpc_boost
                };
            });

            modified_sites = _.unionBy(siteList, modified_sites, 'sectionId');

            const payload = {
                bids: {
                    bySection: _.map(modified_sites, o => _.pick(o, ['sectionId', 'cpcAdjustment']))
                }
            };

            outbrainCampaignService.updateCampaignOutbrain(accessKeys, campaign.id, payload).then(reponse => {
                resolve({
                    success: true,
                    message: 'Success.',
                });
            }, err => logger.error(err));

        }, err => logger.error(err));
    });
}

function updateCurrentMetrics(id, newData, newResults) {
    return new Promise(function (resolve, reject) {
        newResults = _.map(newResults, o => _.pick(o, metricFields));

        SiteMetricsRule.findByIdAndUpdate(id, {
            start_date: newData.start_date,
            end_date: newData.end_date,
            totalResults: _.size(newResults),
            updated_at: new Date(),
            results: newResults,
        }, (err, res) => {
            if (err) return logger.error(err);
            resolve({ success: true, message: 'Rule CPC updated metrics.' });
        });
    });
}

function saveSiteMetricsToDatabase(trafficSource, campaign, data) {
    return new Promise(function (resolve, reject) {
        data.sites = _.map(data.sites, o => _.pick(o, metricFields));

        SiteMetricsRule.create({
            start_date: data.start_date,
            end_date: data.end_date,
            timestamp: data.timestamp,
            trafficSource: trafficSource,
            campaign: {
                id: campaign.id,
                name: campaign.name,
            },
            totalResults: _.size(data.sites),
            results: data.sites,
        }, (err, response) => {
            if (err) return logger.error(err);
            resolve({ success: true, message: 'Rule CPC saved metrics.' });
        });
    });
}

function checkSiteMetrics(trafficSource, campaign) {
    return new Promise(function (resolve, reject) {
        SiteMetricsRule.findOne({ trafficSource: trafficSource, 'campaign.id': campaign.id }, {}, (err, site) => {
            if (err) return logger.error(err);
            resolve(site);
        });
    });
}

function siteMetricsByCampaign(campaign_id, interval) {
    return new Promise(function (resolve, reject) {
        const dateRange = ruleHelper.getDateRange(interval);
        siteService.getSiteMetrics(campaign_id, dateRange.start_date, dateRange.end_date).then(res => {
            if (res.totalRows === 0) return;
            res.sites = _.filter(res.sites, ['status', 'ACTIVE']);
            resolve(res);
        }, err => logger.error(err));
    });
}

function accountDetails(campaign) {
    return new Promise(function (resolve, reject) {
        Promise.all([
            new Promise((resolve, reject) => {
                Traffic.findById(campaign.trafficSourceAccount, (err, res) => {
                    if (err || !res) return;
                    resolve(res);
                });
            }),
            new Promise((resolve, reject) => {
                Tracker.findById(campaign.trackerAccount, (err, res) => {
                    if (err || !res) return;
                    resolve(res);
                });
            })
        ]).then(data => {
            resolve(data);
        }).catch(err => logger.error(err));
    });
}

exports.blockOnCampaign = blockOnCampaign;
exports.blockOnAllCampaigns = blockOnAllCampaigns;
exports.changeSitesCpcOnCampaign = changeSitesCpcOnCampaign;
exports.changeSitesCpcOnAllCampaigns = changeSitesCpcOnAllCampaigns;