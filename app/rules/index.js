'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('config');
const logger = require('../lib/logger');
const mailer = require('../lib/mailer');
const dateFormat = config.get('dateFormat');
const moment = require('moment-timezone');
moment.tz.setDefault(config.get('timezone'));
const Traffic = require('../models/traffic');
const Tracker = require('../models/tracker');
const campaignRule = require('./services/campaign');
const widgetRule = require('./services/widget');
const contentRule = require('./services/content');

function blockWidget(rule) {
    let promises = [];

    if (!isObjectExists(rule.campaigns) || !isObjectExists(rule.criterias))
        return;

    if (_.includes(rule.campaigns, 'all')) {
        promises.push(widgetRule.blockOnAllCampaigns(rule));
    } else {
        let timeout = 0;
        _.forEach(rule.campaigns, campaign_id => {
            promises.push(widgetRule.blockOnCampaign(rule, campaign_id, timeout));
            timeout = timeout + 60000;
        });
    }

    resolvesPromise(promises, rule, 'Block Sites Alert!');
}

function changeSiteCpc(rule) {
    let promises = [];

    if (!isObjectExists(rule.campaigns))
        return;

    if (_.includes(rule.campaigns, 'all')) {
        promises.push(widgetRule.changeSitesCpcOnAllCampaigns(rule));
    } else {
        let timeout = 0;
        _.forEach(rule.campaigns, campaign_id => {
            promises.push(widgetRule.changeSitesCpcOnCampaign(rule, campaign_id, timeout));
            timeout = timeout + 60000;
        });
    }

    Promise.all(promises).then(response => {
        logger.info(JSON.stringify(response));
    }).catch(err => logger.error(err));
}

function resolvesPromise(promises, rule, subject = null) {
    Promise.all(promises).then(responses => {
        if (rule.action !== 1)
            return;

        const alertMsg = _.map(responses, o => o.alertMsg ? o.alertMsg : '');

        const messageText = _.join(alertMsg, " <br/> ");
        emailAlert(null, subject + ' - ' + new Date(), messageText);
    }).catch(err => logger.error(err));
}

function filterRules(rules) {
    rules = validateRules(rules);

    if (!isObjectExists(rules))
        return logger.warn('Rules not available!');

    return rules;
}

function pipeRules(ruleArray) {
    _.forEach(ruleArray, rule => {
        ruleAction(rule);
    });
}

function isObjectExists(obj) {
    if (!obj || _.isUndefined(obj) || _.isEmpty(obj))
        return false;

    return true;
}

function validateRules(rules) {
    return _.filter(rules, rule => {
        return rule.status && isObjectExists(rule.campaigns) && isObjectExists(rule.criterias);
    });
}

function ruleAction(rule) {
    switch (rule.action) {
        case 1:
        case 2:
        case 3:
            ruleBlock(rule);
            break;

        case 4:
            break;

        case 5:
            ruleChangeCpc(rule);
            break;
    }
}

function ruleBlock(rule) {
    switch (rule.level) {
        case 1:
            break;

        case 2:
            break;

        case 3:
            blockWidget(rule);
            break;
    };
}

function ruleChangeCpc(rule) {
    switch (rule.level) {
        case 1:
            break;

        case 2:
            break;

        case 3:
            changeSiteCpc(rule);
            break;
    };
}

function validateCriteriasData(data, criterias) {
    let filterData = data;
    _.forEach(criterias, criteria => {
        if ('' === criteria.value)
            return;

        switch (criteria.operator) {
            case 'greater':
                filterData = _.filter(filterData, obj => {
                    let criteriaValue = criteria.value;
                    const columnValue = columnValueDefault(obj, criteria.column);

                    if (criteria.onCalc && criteria.onCalc === true) {
                        if (obj[criteria.xcolumn] === 0 || _.isUndefined(obj[criteria.xcolumn]))
                            return false;

                        criteriaValue = calcCriteriaColumn(obj, criteria);
                        if (_.isNull(criteriaValue))
                            return false;
                    }

                    return columnValue > criteriaValue;
                });
                break;

            case 'equal':
                filterData = _.filter(filterData, obj => {
                    let criteriaValue = criteria.value;
                    const columnValue = columnValueDefault(obj, criteria.column);

                    if (criteria.onCalc && criteria.onCalc === true) {
                        if (obj[criteria.xcolumn] === 0 || _.isUndefined(obj[criteria.xcolumn]))
                            return false;

                        criteriaValue = calcCriteriaColumn(obj, criteria);
                        if (_.isNull(criteriaValue))
                            return false;
                    }

                    return columnValue == criteriaValue;
                });
                break;

            case 'less':
                filterData = _.filter(filterData, obj => {
                    let criteriaValue = criteria.value;
                    const columnValue = columnValueDefault(obj, criteria.column);

                    if (criteria.onCalc && criteria.onCalc === true) {
                        if (obj[criteria.xcolumn] === 0 || _.isUndefined(obj[criteria.xcolumn]))
                            return false;

                        criteriaValue = calcCriteriaColumn(obj, criteria);
                        if (_.isNull(criteriaValue))
                            return false;
                    }

                    return columnValue < criteriaValue;
                });
                break;

            default:
                filterData = [];
                break;
        }
    });

    return filterData;
}

function columnValueDefault(obj, column) {
    return !_.isUndefined(obj[column]) ? obj[column] : 0;
}

function calcCriteriaColumn(obj, criteria) {
    let totalValue;

    switch (criteria.calc) {
        case 'add':
            totalValue = _.add(criteria.value, obj[criteria.xcolumn]);
            break;

        case 'subtract':
            totalValue = _.subtract(criteria.value, obj[criteria.xcolumn]);
            break;

        case 'multiply':
            totalValue = _.multiply(criteria.value, obj[criteria.xcolumn]);
            break;

        case 'divide':
            totalValue = _.divide(criteria.value, obj[criteria.xcolumn]);
            break;

        default:
            totalValue = null;
            break;
    }

    return totalValue;
}

function getCronTime(rotation) {
    let cron_time;
    switch (rotation) {
        case '10min':
            cron_time = '*/10 * * * *';
            break;

        case '15min':
            cron_time = '*/15 * * * *';
            break;

        case '20min':
            cron_time = '*/20 * * * *';
            break;

        case '30min':
            cron_time = '*/30 * * * *';
            break;

        case '45min':
            cron_time = '*/45 * * * *';
            break;

        case '1hour':
            cron_time = '00 */1 * * *';
            break;

        case '2hour':
            cron_time = '00 */2 * * *';
            break;

        case '3hour':
            cron_time = '00 */3 * * *';
            break;

        case '6hour':
            cron_time = '00 */6 * * *';
            break;

        case '12hour':
            cron_time = '00 */12 * * *';
            break;

        case 'daily':
            cron_time = '00 00 * * 0-6';
            break;

        case 'weekly':
            cron_time = '00 00 * * 0';
            break;

        default:
            break;
    }

    return cron_time;
}

function getDateRange(interval) {
    let start_date = moment();
    let end_date = moment();

    switch (interval) {
        case 'today':
            start_date = end_date = moment();
            break;

        case 'yesterday':
            start_date = moment().subtract(1, 'days');
            end_date = moment().subtract(1, 'days');
            break;

        case '3days':
            start_date = moment().subtract(2, 'days');
            break;

        case '7days':
            start_date = moment().subtract(6, 'days');
            break;

        case '14days':
            start_date = moment().subtract(13, 'days');
            break;

        case '30days':
            start_date = moment().subtract(29, 'days');
            break;

        case 'lifetime':
            start_date = moment('2017-01-01');
            break;

        default:
            break;
    }

    return {
        start_date: start_date.format(dateFormat),
        end_date: end_date.format(dateFormat)
    };
}

function emailAlert(error, subject, body) {
    if (error) {
        logger.error('Send email error: %s', error);
        body = error;
    }

    mailer.send('dinhgiang008@gmail.com', subject, body);
}

exports.getDateRange = getDateRange;
exports.filterRules = filterRules;
exports.pipeRules = pipeRules;
exports.isObjectExists = isObjectExists;
exports.validateRules = validateRules;
exports.ruleAction = ruleAction;
exports.ruleBlock = ruleBlock;
exports.blockWidget = blockWidget;
exports.validateCriteriasData = validateCriteriasData;
exports.emailAlert = emailAlert;
exports.getCronTime = getCronTime;