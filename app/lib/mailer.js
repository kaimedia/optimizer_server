'use strict';

const conf = require('config').get('EMAIL');
const mailer = require('nodemailer');

function send(to, subject, body) {
    let transporter = mailer.createTransport({
        host: conf.SMTP.host,
        port: conf.SMTP.port,
        secure: conf.SMTP.secure,
        auth: {
            user: conf.SMTP.user,
            pass: conf.SMTP.pass,
        }
    });

    let mailOptions = {
        from: `"${conf.FROM.name}" <${conf.FROM.email}>`,
        to: to,
        subject: subject,
        html: body,
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error)
            return logger.error(error);

        logger.info('Email %s sent: %s', info.messageId, info.response);
    });
}

exports.send = send;