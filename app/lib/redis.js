'use strict';

const logger = require('./logger');
const config = require('config');
const redis = require("redis");

function _createClient() {
    const client = redis.createClient(config.get('REDIS.PORT'), config.get('REDIS.HOST'),
        {
            retry_strategy: (options) => {
                if (options.error && options.error.code === 'ECONNREFUSED') {
                    return new Error('The server refused the connection');
                }
                if (options.total_retry_time > 1000 * 60 * 60) {
                    return new Error('Retry time exhausted');
                }
                if (options.attempt > 10) {
                    return undefined;
                }
                return Math.min(options.attempt * 100, 3000);
            }
        });
    return client;
}

function _status(client) {
    client.on('connect', () => logger.info('Redis is connected! [%s:%s]', config.get('REDIS.HOST'), config.get('REDIS.PORT')));
    client.on('reconnecting', () => logger.warn('Redis reconnecting!'));
    client.on('end', () => logger.error('Redis is disconnected!'));
    client.on('error', (err) => logger.error(err));
}

function _set(client, key, value, expire) {
    client.set(key, JSON.stringify(value), 'EX', expire, (err) => {
        if (err) throw err;
    });
}

function _del(client, key) {
    client.del(key, (err, res) => {
        if (err) throw err;
        if (res) logger.warn('Deleted cache: %s', key);
    });
}

exports._createClient = _createClient;
exports._status = _status;
exports._set = _set;
exports._del = _del;