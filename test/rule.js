'use strict';

//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let RuleModel = require('../models/rule');
let server = require('../app');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('Rules', () => {

    describe('/GET rules', () => {
        it('it should GET all the rules', (done) => {
            chai.request(server)
                .get('/api/rule')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.have.json;
                    done();
                });
        });
    });

});