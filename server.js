'use strict';

const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
const config = require('config');
const database = require('./config/database');
const moment = require('moment-timezone');
const logger = require('./app/lib/logger');
// const schedule = require('./app/schedules/index');

const app = express();
const server = http.createServer(app);

app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
app.disable('etag');

app.use(morgan('dev', {
    skip: (req, res) => res.statusCode < 400
}));

const authRoutes = require('./app/routes/auth');
const userRoutes = require('./app/routes/user');
const trafficRoutes = require('./app/routes/traffic');
const trackerRoutes = require('./app/routes/tracker');
const voluumRoutes = require('./app/routes/api/voluum');
const campaignRoutes = require('./app/routes/campaign');
const contentRoutes = require('./app/routes/content');
const siteRoutes = require('./app/routes/site');
const fraudRoutes = require('./app/routes/fraud');
const ruleRoutes = require('./app/routes/rule');
const blacklistRoutes = require('./app/routes/blacklist');

app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/traffic', trafficRoutes);
app.use('/api/tracker', trackerRoutes);
app.use('/api/voluum', voluumRoutes);
app.use('/api/campaign', campaignRoutes);
app.use('/api/content', contentRoutes);
app.use('/api/site', siteRoutes);
app.use('/api/fraud', fraudRoutes);
app.use('/api/rule', ruleRoutes);
app.use('/api/blacklist', blacklistRoutes);

app.use((err, req, res, next) => {
    if (!err) return next();
    res.status(res.statusCode).send({
        message: err.message,
        error: err
    });
});

if (config.util.getEnv('NODE_ENV') === "production") {
    console.log = () => { };
}

server.listen(app.get('port'), () => {
    logger.info('[SERVER] %s', moment().tz(config.get('timezone')).format('YYYY-MM-DD HH:mm:ss Z'));
    logger.info('[SERVER] Listening on port: %s', app.get('port'));
});

server.timeout = 240000; //4 minutes

module.exports = server;