'use strict';

const config = require('config');
const mongoose = require('mongoose');
const logger = require('../app/lib/logger');

const options = {
    useMongoClient: true,
    server: {
        socketOptions: {
            connectTimeoutMS: 30000,
            keepAlive: 1
        }
    },
    replset: {
        socketOptions: {
            connectTimeoutMS: 30000,
            keepAlive: 1
        }
    },
};

mongoose.Promise = global.Promise;
mongoose.connect(config.get('database.uri'), options);
const db = mongoose.connection;

db.on('error', console.error.bind(console, '[MongoDB] connection error:'));
db.once('open', () => {
    logger.info('[MongoDB] is connected!');
});